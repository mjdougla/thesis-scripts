#!/usr/bin/python
# intron_parent_genes.py

import csv
import re

gene_database = {}
new_lines = []

# Read the GFF3 of WormBase gene models, parse the attributes colunm of each
# row encoding a gene, and create a dictionary of transcript aliases and their
# associated gene names.
with open('c_elegans.PRJNA13758.WS250.gene_models.gff3', 'r') as incsv:
    csvreader = csv.reader(incsv, delimiter='\t', quotechar='#')
    for row in csvreader:
        if len(row) > 1:
            if row[2].lower() == 'gene':
                gene_name = re.search('(?<=ID=Gene:)[A-Za-z0-9\.]+(?=(;|))', row[-1]).group(0)
                transcript_alias = re.search('(?<=Alias=)[a-zA-Z0-9\.\-\,]+(?=(;|))', row[-1]).group(0).split(',')
                for alias in transcript_alias:
                    gene_database[alias] = gene_name

with open('unique_introns.csv', 'r') as incsv:
    csvreader = csv.reader(incsv, delimiter='\t', quotechar='#')
    next(csvreader, None) #skip header
    for row in csvreader:
        #transcript = re.search('(?<=Parent=Transcript:)[A-Za-z0-9\.]+(?=(;|))', row[2]).group(0)
        transcript = row[2]
        # The following parsing of transcript names is specific to the WormBase
        # format, and may not work in all cases (eg. CKSAT2.2a.4 -> CKSAT2.2)
        parent = transcript
        if parent.count('.') > 1: parent = '.'.join(parent.split('.')[:2])
        parent = re.sub('[a-z]$', '', parent)
        # End parsing.
        new_lines.append([row[0], row[1], gene_database[parent]])

with open('unique_introns_gene.csv', 'w') as outcsv:
    csvwriter = csv.writer(outcsv, delimiter='\t', quotechar='#', quoting=csv.QUOTE_MINIMAL)
    csvwriter.writerow(['position', 'length', 'gene'])  #write header
    for row in new_lines:
        csvwriter.writerow(row)

print('done')
