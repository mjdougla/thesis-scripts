#!/usr/local/bin/python3

import statistics, sys
from collections import defaultdict

def mismatches(f, MM_dict):
    for line in f:
        if line[0] != '@':
            for i in line.split('\t')[::-1]:
                if 'NM:i:' in i:
                    score = int(i.split(':')[-1])
                    MM_dict[score] += 1
                    break

    return MM_dict


def output(MM_dict):
    print('Mismatches\tFreq\tFreq (%)')
    total = sum([i for i in MM_dict.values()])
    sum_p = 0
    for i in range(min(MM_dict), max(MM_dict)+1):
        try:
            freq = MM_dict[i]
        except KeyError:
            freq = 0
        freq_p = freq / total * 100
        sum_p += freq_p
        print(i, freq, '%.2f' % freq_p + '%', sep='\t')
    print('Total', total, '%.2f' % sum_p + '%', sep='\t')


def main(SAM_files):
    MM_dict = defaultdict(int)

    # get the frequency of alignment scores in each SAM file
    for i in SAM_files:
        with open(i, 'r') as f:
            MM_dict = mismatches(f, MM_dict)

    # print results
    output(MM_dict)


if __name__ == '__main__':
    main(sys.argv[1:])
