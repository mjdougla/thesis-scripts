#!/home2/mattdoug/bin/python3

import gzip, os, sys
from Bio import SeqIO

path1 = sys.argv[1]
path2 = sys.argv[2]
ext1 = path1.split('.')[-2]
ext2 = path2.split('.')[-2]
out1 = open('.'.join(('out_1', ext1)), 'w')
out2 = open('.'.join(('out_2', ext2)), 'w')
ftype1 = ''
ftype2 = ''
reads1 = set()
reads2 = set()
has_mate = 0
no_mate1 = 0
no_mate2 = 0

if ext1 in ('fasta', 'fa', 'cfa', 'pfa'):
    ftype1 = 'fasta'
elif ext1 in ('fastq', 'fq', 'cfq', 'pfq'):
    ftype1 = 'fastq'
else:
    print('[ERROR] Unrecognized file type "{}" for file "{}"'.format(path1, ext1))

if ext2 in ('fasta', 'fa', 'cfa', 'pfa'):
    ftype2 = 'fasta'
elif ext2 in ('fastq', 'fq', 'cfq', 'pfq'):
    ftype2 = 'fastq'
else:
    print('[ERROR] Unrecognized file type "{}" for file "{}"'.format(path2, ext2))

print('Reading file "{}" (format: {})...'.format(path1, ftype1))
with gzip.open(path1, "rt") as f:
    for record in SeqIO.parse(f, ftype1):
        rid = record.id.rsplit('/')[0]
        reads1.add(rid)

print('Checking for mates in file "{}" (format: {})...'.format(path2, ftype2))
with gzip.open(path2, "rt") as f:
    for record in SeqIO.parse(f, ftype2):
        rid = record.id.rsplit('/')[0]
        reads2.add(rid)
        if rid in reads1:
            SeqIO.write(record, out2, ftype2)
            has_mate += 1
        else:
            no_mate2 += 1

print('Checking for mates in file "{}" (format: {})...'.format(path1, ftype1))
with gzip.open(path1, "rt") as f:
    for record in SeqIO.parse(f, ftype1):
        rid = record.id.rsplit('/')[0]
        if rid in reads2:
            SeqIO.write(record, out1, ftype1)
        else:
            no_mate1 += 1

print('  {:,} reads pairs!'.format(has_mate))
print('  {:,} reads in "{}" have no mate!'.format(no_mate1, path1))
print('  {:,} reads in "{}" have no mate!'.format(no_mate2, path2))
