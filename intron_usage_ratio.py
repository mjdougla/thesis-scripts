#!/usr/local/bin/python3
# Last updated: 11/5/2018
# Author: Matt Douglas
#
# Purpose: Calulate the relative usage ratios for introns within a gene (1 base
# adjacent to the gene counts as "within the gene").
#
# Note: Based on the procedure in the paper by Tourasse et al. (2017), which
# did not include introns that do not overlap annoated genes. For introns that
# do not overlap genes, we assign a ratio of 0.
# 
# Example: II:4,991,332..4,993,331 - genes overlap

import sys
from collections import defaultdict
from itertools import chain

flatten = chain.from_iterable
gene_dict = defaultdict(set)
intron_dict = defaultdict(set)
assign_dict = defaultdict(list)
AS_counts = { 0:0, 0.01:0, 0.05:0, 0.10:0, 0.25:0, 0.33:0 }
i_at_ratio = { 0:set(), 0.01:set(), 0.05:set(), 0.10:set(), 0.25:set(), 0.33:set() }
i_per_gene = { 0:[], 0.01:[], 0.05:[], 0.10:[], 0.25:[], 0.33:[] }
gene_c = 0
intron_c = 0
have_introns = 0
window = 500000 # for performance only
gene_gff = sys.argv[1]
intron_gff = sys.argv[2]


class Intron(object):
    def __init__(self, chrom, start, end, count, strand):
        self.chrom = chrom
        self.start = start
        self.end = end
        self.count = count
        self.strand = strand
        self.usage = 0.0
        self.max_usage = 0.0


def eprint(*args, **kwargs):
    """Print to stderr."""
    print(*args, file=sys.stderr, **kwargs)


def prog(count, total):
    """Return a number formatted as a percentage, to 1 decimal place."""
    try:
        progress = count * 100 / total
        progress = '{0:.1f}%'.format(progress)
        return progress
    except ZeroDivisionError:
        return '0%'


def perc(num):
    """Return a number formatted as a percentage, to 1 decimal place."""
    return '{0:.1f}%'.format(num * 100)


def compare_expression(introns):
    """Calcuate the 'donor ratio' for all introns that share a splice donor as:
    No. support for that intron / Total support for introns sharing that donor
    The 'acceptor ratio' is calculated similarily. If both a donor and acceptor
    ratio can be computed for a given exon, the 'usage ratio' is the higher
    value. If an intron does not share a splice site with another intron, the
    'usage ratio' = 1.
    Also define a ratio based on the intron with the highest support within the
    gene as: No. support for that intron / No. support for highest supported
    intron. Note that introns assigned to more than one gene will have ratios
    specific to each gene.
    """
    l_site_dict = defaultdict(set)
    r_site_dict = defaultdict(set)
    i_as_objects = { 0:[], 0.01:[], 0.05:[], 0.10:[], 0.25:[], 0.33:[] }
    i_as_tuples = { 0:[], 0.01:[], 0.05:[], 0.10:[], 0.25:[], 0.33:[] }
    is_AS = { 0:False, 0.01:False, 0.05:False, 0.10:False, 0.25:False, 0.33:False }
    max_c = 0

    # determine which introns share donor and/or acceptor sites
    for i in introns:
        l = i.start
        r = i.end
        l_site_dict[l].add(i)
        r_site_dict[r].add(i)

    # calulate the usage ratio for introns sharing donor/acceptor sites
    for i in introns:
        l = i.start
        r = i.end
        c = i.count
        alt_l = l_site_dict[l]
        alt_r = r_site_dict[r]
        usage = 1 # usage ratio defaults to 1 if no splice sites are shared
        # keep track of the most used intron in the gene
        if c > max_c:
            max_c = c
        # if there are both alt acceptor and donor sites...
        if len(alt_l) > 1 and len(alt_r) > 1:
            l_sum = sum( [i.count for i in alt_l] )
            r_sum = sum( [i.count for i in alt_r] )
            usage = c / max( (l_sum, r_sum) )
        # else if only a donor site is shared...
        elif len(alt_l) > 1:
            l_sum = sum( [i.count for i in alt_l] )
            usage = c / l_sum
        # else if only a acceptor site is shared...
        elif len(alt_r) > 1:
            r_sum = sum( [i.count for i in alt_r] )
            usage = c / r_sum
        i.usage = usage

    # calulate the usage ratio relative to the most highly expressed intron
    for i in introns:
        c = i.count
        i.max_usage = c / max_c
        # seperate out introns by usage ratios
        for ratio, introns in i_as_objects.items():
            if i.max_usage >= ratio:
                introns.append(i)
        for ratio, introns in i_as_tuples.items():
            if i.max_usage >= ratio:
                intron = i.chrom, i.start, i.end, i.strand
                introns.append(intron)

    # check if the gene still shows alternative splicing at each usage ratio
    # threshold
    for ratio, introns in i_as_objects.items():
        site_dict = defaultdict(set)
        for i in introns:
            l = i.start
            r = i.end
            site_dict[l].add(i)
            site_dict[r].add(i)
        for i in site_dict.values():
            if len(i) > 1:
                is_AS[ratio] = True
                break

    return i_as_objects, i_as_tuples, is_AS


def sort_introns(introns):
    """Sort GFF3 formatted entries by chromosome, then start position, then end
    position. NOTE: C. elegans uses roman numerals for chromosomes names.
    """
    numerals = {'I':1, 'II':2, 'III':3, 'IV':4, 'V':5, 'X':10, 'MtDNA':11}
    try:
        return sorted(introns, key=lambda x: (numerals[x.chrom], x.start, x.end, x.strand))
    except KeyError:
        return sorted(introns, key=lambda x: (x.chrom, x.start, x.end, x.strand))


def print_as_gff3(introns):
    """Take a list of Intron() objects and print out in GFF3 format."""
    print('##gff-version 3')
    for n, i in enumerate(introns):
        chrom = i.chrom
        start = i.start
        end   = i.end
        count = i.count
        strnd = i.strand
        usage = '{0:.3f}'.format(i.usage)
        m_use = '{0:.3f}'.format(i.max_usage)
        line  = (chrom, '.', 'intron', start, end, m_use, strnd, '.',
                 'ID=Intron{};Support={};Usage={}'.format(n+1, count, usage))
        print(*line, sep='\t')


# build a dictionary of gene boundaries (on each strand)
with open(gene_gff, 'r') as f:
    for line in f:
        if line[0] != '#':
            i   = line.split('\t')
            kind = i[2]
            key = i[0], i[6]
            pos = int(i[3]) - 1, int(i[4]) + 1 # add 1 to ends to count introns that might extend gene
            if kind == 'gene':
                gene_dict[key].add(pos)
                gene_c += 1

if len(gene_dict) < 1:
    eprint('No genes found. Exiting.')
    sys.exit(0)

# sort gene boundaries by position
for key, pos_list in gene_dict.items():
    gene_dict[key] = sorted(pos_list, key=lambda x: (x[0], x[1]))

# identify all introns
with open(intron_gff, 'r') as f:
    for line in f:
        if line[0] != '#':
            i = line.split('\t')
            kind = i[2]
            count = [int(i[5]) if i[5] != '.' else 1.0][0]
            intron = Intron(i[0], int(i[3]), int(i[4]), count, i[6])
            key = intron.chrom, intron.strand
            intron_dict[key].add(intron)
            intron_c += 1

if len(intron_dict) < 1:
    eprint('No introns found. Exiting.')
    sys.exit(0)

# sort introns by position
for key, introns in intron_dict.items():
    intron_dict[key] = sorted(introns, key=lambda x: (x.start, x.end))

# assign introns to genes where one or more splice sites are located within
# the gene boudaries
for key, genes in gene_dict.items():
    introns = intron_dict[key]
    last = 0
    for i in genes:
        a, b = i
        for n, j in enumerate(introns):
            c, d = j.start, j.end
            key2 = key[0], a, b, key[1]
            if a <= c <= b: # intron start lies within gene
                assign_dict[key2].append(j)
            elif a <= d <= b: # intron end lies within gene
                assign_dict[key2].append(j)
            elif c < a - window: # for performance only
                last = n
            elif b < c: # next, if we gone past the end of the gene
                introns = introns[last:]
                break

for _, i in assign_dict.items():
    if len(i) > 0:
        have_introns += 1

# for each gene, compute junction usage ratios
for gene, introns in assign_dict.items():
    i_as_objects, i_as_tuples, is_AS = compare_expression(introns)
    assert len(i_as_objects.values()) == len(i_as_tuples.values())
    # track which introns show up, and how many introns per gene, at each
    # threshold
    for ratio, introns in i_as_tuples.items():
        i_per_gene[ratio].append(len(introns))
        for i in introns:
            i_at_ratio[ratio].add(i)
    # track if the gene still shows AS at each ratio threshold
    for ratio, b in is_AS.items():
        if b:
            AS_counts[ratio] += 1

# merge and sort the results (including introns not assigned to a gene)
introns = [i for j in intron_dict.values() for i in j]
introns = sort_introns(introns)

print_as_gff3(introns)

eprint('{:,} genes found'.format(gene_c))
eprint('{:,}/{:,} ({}) genes have introns assigned to them'.format(
       have_introns, gene_c, prog(have_introns, gene_c)))
eprint('{:,} introns found'.format(intron_c))

eprint('Number of introns at each usage ratio threshold:')
for ratio in sorted(i_at_ratio):
    introns = i_at_ratio[ratio]
    r = int(ratio * 100)
    s = len(introns)
    p = prog(s, intron_c)
    eprint('  {}% or more = {:,} ({})'.format(r, s, p))

eprint('Avg. no. introns per gene at each usage ratio threshold:')
for ratio in sorted(i_per_gene):
    counts = i_per_gene[ratio]
    s = sum(counts)
    t = float(len(counts))
    avg = s / t
    avg = '{0:.1f}'.format(avg)
    eprint('  {} or more = {}'.format(perc(ratio), avg))

eprint('Genes with detected AS at each usage ratio threshold:')
for ratio in sorted(AS_counts):
    count = AS_counts[ratio]
    eprint('  {} or more = {:,} ({})'.format(perc(ratio), count, prog(count, gene_c)))
