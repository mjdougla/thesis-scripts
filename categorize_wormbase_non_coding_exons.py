#!/usr/local/bin/python3
# Last updated: 2/3/2018
# Author: Matt Douglas
# USAGE: categorize_wormbase_non_coding_exons.py <reference.gff3> <output_prefix>

import re, sys
from collections import defaultdict

attr_name = re.compile('(?<=Name=)[^;]+(?=(;|))')
attr_seq_name = re.compile('(?<=sequence_name=)[^;]+(?=(;|))')

def eprint(*args, **kwargs):
    """Print to stderr."""
    print(*args, file=sys.stderr, **kwargs)


def parse_attr(attr, label):
    names = []

    for i in attr.split(';'):
        if i.startswith(label):
            for j in i.split(','):
                name = j.split(':')[1]
                names.append(name)

    return names


def parse_parent(name):
    parent = '.'.join(name.split('.')[:2]) #Y71H2AM.19a.1 <- ".1" indicates different UTRs
    if parent[-1].isalpha():
        parent = parent[:-1]

    return parent


def sort_exon_by_pos(lines):
    # remove non-exon lines
    lines = [i.split('\t') for i in lines if i.split('\t')[2] == 'exon']
    # sort by position
    sorted_lines = sorted(lines, key=lambda x: (int(x[3]), int(x[4])))
    # rejoin lines
    sorted_lines = ['\t'.join(i) for i in sorted_lines]

    return sorted_lines


if __name__ == '__main__':
    name_dict = {}
    line_dict = {}
    header    = []
    out1      = []
    out2      = []
    out3      = []
    out4      = []

    gff_file = sys.argv[1]
    prefix   = [sys.argv[2] if len(sys.argv) > 2 else 'non_coding'][0]

    # get the names of all genes
    with open(gff_file, 'r') as f:
        for line in f:
            line = line.strip()
            if line[0] != '#':
                i = line.split('\t')
                kind = i[2]
                if kind == 'gene':
                    name     = attr_name.search(i[8]).group(0)
                    seq_name = attr_seq_name.search(i[8]).group(0)
                    name_dict[seq_name] = name
                    line_dict[name] = [line]

    # loop through the GFF file and yield any non-gene entries
    with open(gff_file, 'r') as f:
        for line in f:
            line = line.strip()
            if line[0] == '#':
                header.append(line)
            else:
                i = line.split('\t')
                kind, attr = i[2], i[8]
                if kind != 'gene':
                    for name in parse_attr(attr, 'Parent='):
                        parent = parse_parent(name)
                        try:
                            line_dict[parent].append(line)
                        except KeyError:
                            parent = name_dict[parent]
                            line_dict[parent].append(line)

    # for each exon get the coordinate of each exon from a list of GFF format
    # lines, sort them by position, and categorize them
    for parent, lines in line_dict.items():
        exons = sort_exon_by_pos(lines)
        if len(exons) < 1: # some entires don't have "exon" features, so will be None
            continue
        elif len(exons) == 1: # single-exon gene
            out1 += exons
        else:
            strand = exons[0].split('\t')[6]
            out3 += exons[1:-1]
            if strand == '+':
                out2 += [exons[0]]
                out4 += [exons[-1]]
            elif strand == '-':
                out4 += [exons[0]]
                out2 += [exons[-1]]

    # output the results
    for out, suf in ( (out1, 'single'), (out2, 'five_term'), (out3, 'internal'), (out4, 'three_term')):
        with open(prefix+'.'+suf+'.gff3', 'w') as f:
            print('#gff-version 3', file=f)
            for line in out:
                print(line, file=f)
