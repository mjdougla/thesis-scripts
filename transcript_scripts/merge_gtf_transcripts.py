#!/usr/local/bin/python3
# Last updated: 5/15/2018
# Author: Matt Douglas
# NOTE: NOT USED IN MY THESIS. USE 'gffcompare -X' INSTEAD.
# Purpose: Naive merge transcripts in GTF/GFF3 format from multiple samples.
#          Transcripts will be merged if they contain the same chain of introns.
#          If terminal exons differ in length, the longest terminal exon(s) will
#          be kept.
# NOTE: This script does NOT retain information about expression levels.

import sys
from collections import defaultdict

class Transcript(object):
    def __init__(self, seqid, left, right, strand):
        self.seqid = seqid
        self.left = left
        self.right = right
        self.strand = strand
        self.source = set()
        self.cds = []
        self.exons = []
        self.introns = []

    def infer_introns(self):
        self.exons.sort()
        for n in range(len(self.exons)-1):
            self.introns.append((self.exons[n][1]+1, self.exons[n+1][0]-1))

    def is_single(self):
        return len(self.exons) < 2


def parse_attr(attr):
    # e.g. gene_id "CUFF.1"; transcript_id "CUFF.1.1"; exon_number "1";
    result = defaultdict(str)

    for i in attr.strip().split(';'):
        if len(i) > 0:
            k, v = i.strip().split(' ')
            result[k] = v

    return result['transcript_id'], result['gene_id']


def format_as_gtf(t, t_id):
    attr = 'transcript_id "{}"; sources "{}";'.format(t_id, len(t.source))
    line = t.seqid, '.', 'transcript', t.left, t.right, '.', t.strand, '.', attr
    yield '\t'.join(list(map(str, line)))
    for i in t.cds:
        line = t.seqid, '.', 'CDS', i[0], i[1], '.', t.strand, '.', attr
        yield '\t'.join(list(map(str, line)))
    for i in t.exons:
        line = t.seqid, '.', 'exon', i[0], i[1], '.', t.strand, '.', attr
        yield '\t'.join(list(map(str, line)))
    for i in t.introns:
        line = t.seqid, '.', 'intron', i[0], i[1], '.', t.strand, '.', attr
        yield '\t'.join(list(map(str, line)))


if __name__ == '__main__':
    fofn = sys.argv[1]
    input_list = []
    final = {}
    single = set()
    parent = defaultdict(set)
    source = defaultdict(set)
    notes = defaultdict(list)
    t_count = 0

    with open(fofn, 'r') as f:
        input_list = [line.strip() for line in f]

    for ic, i in enumerate(input_list, 1):
        print('Reading input #{:,}: {}'.format(ic, i), file=sys.stderr)
        print('#Input {}: {}'.format(ic, i))
        skip_c = 0
        temp = {}
        try:
            with open(i, 'r') as f:
                for line in f:
                    if line[0] == '#':
                        continue
                    l = line.split('\t')
                    seqid = l[0]
                    kind = l[2]
                    left = int(l[3])
                    right = int(l[4])
                    strand = l[6]
                    t_id, g_id = parse_attr(l[8])
                    if t_id not in temp:
                        parent[g_id].add(t_id)
                        temp[t_id] = Transcript(seqid, left, right, strand)
                    if kind == 'exon':
                        temp[t_id].exons.append((left, right))
                    elif kind == 'CDS':
                        temp[t_id].cds.append((left, right))
                # infer introns from the positions of exons
                for t in temp.values():
                    t.infer_introns()
                # check which transcripts use the same chain of introns
                for t in temp.values():
                    if t.is_single():
                        skip_c += 1
                        single.add(t)
                        continue
                    chain = tuple([t.seqid, t.strand] + t.introns)
                    # replace the terminal exons of the transcipt if they're longer
                    if chain not in final:
                        final[chain] = t
                    final[chain].source.add(i)
                    lterm = final[chain].exons[0][0]
                    rterm = final[chain].exons[-1][1]
                    if t.exons[0][0] < lterm:
                        final[chain].exons[0] = t.exons[0]
                        final[chain].left = t.exons[0][0]
                    if t.exons[-1][1] > rterm:
                        final[chain].exons[-1] = t.exons[-1]
                        final[chain].right = t.exons[-1][1]
                print('  {:,} transcripts total'.format(len(temp)), file=sys.stderr)
                print('  Ignoring {:,} with only a single exon'.format(skip_c), file=sys.stderr)
        except FileNotFoundError:
            print('  Could not open file! File not found.', file=sys.stderr)

    # print out the merged transcripts
    with open('transcripts_merged.gtf', 'w') as f:
        for n, chain in enumerate(sorted(final, key=lambda x: (x[0], x[2][0])), 1):
            t = final[chain]
            new_id = 'MRG.{}'.format(n)
            for line in format_as_gtf(t, new_id):
                print(line, file=f)
    print('Output {:,} transcripts'.format(n), file=sys.stderr)

    # output single exon transcripts
    with open('transcripts_single_exon.gtf', 'w') as f:
        for n, t in enumerate(single, 1):
            new_id = 'SNGL.{}'.format(n)
            for line in format_as_gtf(t, new_id):
                print(line, file=f)
    print('Output {:,} single-exon transcripts'.format(n), file=sys.stderr)
