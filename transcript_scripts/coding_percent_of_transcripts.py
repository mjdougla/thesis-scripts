#!/usr/local/bin/python3
# Last updated: 5/9/2018
# Author: Matt Douglas
# Purpose: Bin transcripts by the coding percentage of each (i.e. not a UTR).

import os, sys
import gffutils as gff
import numpy as np

path = sys.argv[1]
path_db_name = path + '.db'
ftype = 'mRNA'
bins = np.linspace(0, 1, 11, endpoint=True)
data = []

# build the gffutils database
if os.path.isfile(path_db_name):
    print('Query database found. Importing...', file=sys.stderr)
    qry_db = gff.FeatureDB(path_db_name)
else:
    print('Building query database...', file=sys.stderr)
    qry_db = gff.create_db(qry,
                             dbfn=path_db_name,
                             merge_strategy='create_unique',
                             disable_infer_genes=True,
                             disable_infer_transcripts=True,
                             verbose=True
                            )

# calulate how much of each mRNA is a UTR
print('Reading database...', file=sys.stderr)
for i in qry_db.features_of_type(ftype):
    m_size = i.end - i.start + 1
    c_size = m_size
    for u in ('five_prime_UTR', 'three_prime_UTR'):
        for j in qry_db.children(i, featuretype=u, order_by='start'):
            c_size -= j.end - j.start + 1
    p_size = c_size / m_size
    data.append(p_size)

# bin values and output
count, bin_edge = np.histogram(data, bins=bins)
print('Lower.Bound', 'Count', sep='\t')
for n, i in enumerate(bin_edge[:-1]):
    print('{:.1f}\t{}'.format(i, count[n]))
print('Total\t{}'.format(sum(count)))
