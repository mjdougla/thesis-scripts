#!/usr/local/bin/python3
# Last updated: 8/22/2018
# Author: Matt Douglas
# Purpose: Return transcript models lacking UTRs. Transcript boundaries will be
#          updated to start at the first and last coding bases.

import os, sys
import gffutils as gff
from collections import defaultdict

qry = sys.argv[1]
qry_db_name = qry + '.db'
gene_d = defaultdict(set)

if os.path.isfile(qry_db_name):
    print('Query database found. Importing...', file=sys.stderr)
    qry_db = gff.FeatureDB(qry_db_name)
else:
    print('Building query database...', file=sys.stderr)
    qry_db = gff.create_db(qry,
                             dbfn=qry_db_name,
                             merge_strategy='create_unique',
                             disable_infer_genes=True,
                             disable_infer_transcripts=True,
                             verbose=True
                            )

for g in qry_db.features_of_type('gene'):
    gene = g['ID'][0]
    cds_d = {}
    for n, t in enumerate(qry_db.children(g, featuretype='mRNA'), 1):
        cds = tuple(qry_db.children(t, featuretype='CDS', order_by='start'))
        cds_set = tuple([(x.start, x.end) for x in cds])
        cds_d[cds_set] = cds
    # new gene ends at the boundaries of the CDSs
    g.start = min([x[0][0] for x in cds_d])
    g.end = max([x[-1][1] for x in cds_d])
    print(g)
    # new transcripts
    for n, t in enumerate(cds_d, 1):
        t_start = t[0][0]
        t_end = t[-1][1]
        t_name = 'mRNA{}'.format(n)
        print(g.seqid, '.', 'mRNA', t_start, t_end, '.', g.strand, '.', 'ID={};Parent={}'.format(t_name, gene), sep='\t')
        for c in cds_d[t]:
            c['Parent'] = t_name
            print(c)
