#!/usr/local/bin/python3
# Purpose: Count the number of transcripts identified by each program from each
#          library, before and after checking which are supported by our
#          introns/exons.

import glob, sys
from collections import defaultdict

wdir_list = ['../1_raw/cufflinks/*.gtf',
             '../1_raw/stringtie/*.gtf',
             '../1_raw/transabyss/*.gff3',
             'cufflinks/*-final.gff3',
             'stringtie/*-final.gff3',
             'transabyss/*-final.gff3',
             'cufflinks/*-single.gff3',
             'stringtie/*-single.gff3',
             'transabyss/*-single.gff3'
            ]
names = ['Cufflinks',
         'Stringtie',
         'Trans-ABYSS',
         'Cufflinks (Validated)',
         'Stringtie (Validated)',
         'Trans-ABYSS (Validated)',
         'Cufflinks (Single)',
         'Stringtie (Single)',
         'Trans-ABYSS (Single)'
        ]
counts = defaultdict(dict)

def transcript_per_run(wdir):
    for path in glob.glob('{}'.format(wdir)):
        lib = path.split('/')[-1].split('.')[0].split('-')[0]
        print('{} (as {})\t'.format(path, lib), end='', file=sys.stderr)
        c = 0
        with open(path, 'r') as f:
            for line in f:
                if line[0] != '#':
                    ftype = line.split('\t')[2]
                    if ftype == 'transcript' or 'RNA' in ftype:
                        c += 1
        print('{:,} transcripts'.format(c), file=sys.stderr)
        counts[lib][wdir] = c


for i in wdir_list:
    transcript_per_run(i)

print('Library', *names, sep='\t')
for lib in sorted(counts):
    line = [lib]
    for i in wdir_list:
        line.append((counts[lib][i] if i in counts[lib] else '#N/A'))
    print(*line, sep='\t')
