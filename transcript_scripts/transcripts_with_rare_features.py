#!/home2/mattdoug/python3/bin/python3
# Last updated: 10/26/2018
# Author: Matt Douglas
# Purpose: Check how many transcripts have only non-rare introns and exons.

import os, sys
import gffutils as gff
from collections import defaultdict

# input files
in_i = '/home2/mattdoug/Thesis/intron_database/v2/intron_database.min5.ratios.gff3'
in_e = '/home2/mattdoug/Thesis/exon_database/v2/exons.final.ratios.gff3'
in_t = sys.argv[1]
in_t_db_name = in_t + '.db'

# output files
out_nr_global_name = in_t + '.nonrare_global'
out_nr_local_name = in_t + '.nonrare_local'
out_nr_either_name = in_t + '.nonrare_either'
out_nr_global = open(out_nr_global_name, 'w')
out_nr_local = open(out_nr_local_name, 'w')
out_nr_either = open(out_nr_either_name, 'w')

# declare variables
total = 0
ratio_min = 0.01
r_global_i_c = 0
r_local_i_c = 0
r_global_e_c = 0
r_local_e_c = 0
nr_global_t = 0
nr_local_t = 0
nr_either_t = 0


def find_or_build_database(in_file, db_name):
    if os.path.isfile(db_name):
        print('Query database found. Importing...', file=sys.stderr)
        db = gff.FeatureDB(db_name)
    else:
        print('Building query database...', file=sys.stderr)
        db = gff.create_db(in_file,
                           dbfn=db_name,
                           merge_strategy='create_unique',
                           disable_infer_genes=True,
                           disable_infer_transcripts=True,
                           verbose=True
                          )
    return db


def max_intron_score(attr):
    #e.g. Parent=Gene:WBGene00004418;ScorePerLibrary=SRX659949:2
    maxscore = -1

    for i in attr.strip().split(';'):
        k, v = i.split('=')
        if k == 'ScorePerLibrary':
            try:
                maxscore = max([float(x.split(':')[1]) for x in v.split(',')])
            except IndexError:
                pass

    return maxscore


def parse_gff(in_file, kind):
    d_global = defaultdict(set)
    d_local = defaultdict(set)
    d_global_l = defaultdict(set)
    d_global_r = defaultdict(set)
    d_local_l = defaultdict(set)
    d_local_r = defaultdict(set)
    c_global = 0
    c_local = 0
    count = 0

    with open(in_file, 'r') as f:
        for line in f:
            if len(line) > 0 and line[0] != '#':
                i = line.split('\t')
                if i[2] == kind:
                    count += 1
                    region = i[0], i[6]
                    end, start = int(i[3]), int(i[4])
                    pos = end, start
                    ratio_global = float(i[5])
                    ratio_local = max_intron_score(i[8])
                    if ratio_global < ratio_min:
                        d_global[region].add(pos)
                        d_global_l[region].add(start)
                        d_global_r[region].add(end)
                        c_global += 1
                    if ratio_local < ratio_min:
                        d_local[region].add(pos)
                        d_local_l[region].add(start)
                        d_local_r[region].add(end)
                        c_local += 1

    print('Read {:,} features matching "{}" in {}'.format(count, kind, in_file), file=sys.stderr)
    print('... {:,} are globally rare'.format(c_global), file=sys.stderr)
    print('... {:,} are locally rare'.format(c_local), file=sys.stderr)
    return d_global, d_local, d_global_l, d_global_r, d_local_l, d_local_r


if __name__ =='__main__':
    db = find_or_build_database(in_t, in_t_db_name)

    d_intron_global, d_intron_local, _, _, _, _ = parse_gff(in_i, 'intron')
    d_exon_global, d_exon_local, d_global_l, d_global_r, d_local_l, d_local_r = parse_gff(in_e, 'CDS')

    for kind in ('transcript', 'mRNA'):
        for i in db.features_of_type(kind):
            total += 1
            region = (i.seqid, i.strand)
            exons = [(x.start, x.end) for x in db.children(i, featuretype='exon', order_by='start')]
            exon_l = exons[0][1]
            exon_i = exons[1:-1]
            exon_r = exons[-1][0]
            introns = set([(exons[n][1]+1, exons[n+1][0]-1) for n in range(len(exons)-1)])
            r_global_i, r_local_i = False, False
            r_global_e, r_local_e = False, False

            for j in introns:
                if j in d_intron_global[region]:
                    r_global_i = True
                if j in d_intron_local[region]:
                    r_local_i = True

            if exon_l in d_global_r:
                r_global_e = True
            if exon_l in d_local_r:
                r_global_e = True

            if exon_r in d_global_l:
                l_global_e = True
            if exon_r in d_local_l:
                l_global_e = True

            for j in exon_i:
                if j in d_exon_global[region]:
                    r_global_e = True
                if j in d_exon_local[region]:
                    r_local_e = True

            if r_global_i:
                r_global_i_c += 1
            if r_local_i:
                r_local_i_c += 1
            if r_global_e:
                r_global_e_c += 1
            if r_local_e:
                r_local_e_c += 1

            if not any((r_global_i, r_global_e)):
                nr_global_t += 1
                print(i, file=out_nr_global)
            if not any((r_local_i, r_local_e)):
                nr_local_t += 1
                print(i, file=out_nr_local)
            if not any((r_global_i, r_global_e)) or not any((r_local_i, r_local_e)):
                nr_either_t += 1
                print(i, file=out_nr_either)

    print('Intron path\t{}'.format(in_i))
    print('Exon path\t{}'.format(in_e))
    print('Transcript path\t{}'.format(in_t))
    print('Output globally non-rare transcripts to\t{}'.format(out_nr_global_name))
    print('Output locally non-rare transcripts to\t{}'.format(out_nr_local_name))
    print('Output globally or locally non-rare transcripts to\t{}'.format(out_nr_either_name))
    print('#'*10)
    print('Transcripts total\t{:,}'.format(total))
    print('Transcripts non-rare globally\t{:,}'.format(nr_global_t))
    print('Transcripts non-rare locally\t{:,}'.format(nr_local_t))
    print('Transcripts non-rare either globally or locally\t{:,}'.format(nr_either_t))
    print('#'*10)
    print('Transcripts with globally rare intron\t{:,}'.format(r_global_i_c))
    print('Transcripts with locally rare intron\t{:,}'.format(r_local_i_c))
    print('Transcripts with globally rare exon\t{:,}'.format(r_global_e_c))
    print('Transcripts with locally rare exon\t{:,}'.format(r_local_e_c))

    out_nr_global.close()
    out_nr_local.close()
    out_nr_either.close()

    print('Done!', file=sys.stderr)
