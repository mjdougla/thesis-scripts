#!/usr/local/bin/python3
# Purpose: Used to compare the number of isoforms per gene between a reference
#          gene set and a query gene set.

import sys

a_path = sys.argv[1] # FILE A = Reference
b_path = sys.argv[2] # FILE B = Sample
counts = {}
results = {i:[] for i in range(7)}
a_count = 0
b_count = 0

def parse_gff3(path, ind):
    with open(path, 'r') as f:
        for line in f:
            if line[0] != '#':
                i = line.strip().split('\t')
                pos = i[0], i[3], i[4], i[6]
                cnt = int(i[9])
                if pos not in counts:
                    counts[pos] = [0, 0]
                counts[pos][ind] = cnt

def parse_csv(path, ind):
    # Region	Name	Introns	Max.Depth	Scores
    # I:4119-10230-	WBGene00022277	9	3	109302,137,53
    with open(path, 'r') as f:
        next(f)
        for line in f:
            i = line.strip().split('\t')
            pos = []
            seqid, t1 = i[0].split(':')        # I, 4119-10230-
            left, t2 = t1.split('-', 1)      # 4119, 10230-
            right, strand = t2[:-1], t2[-1] # 10230, -
            pos = seqid, left, right, strand
            cnt = int(i[3])
            if pos in counts:
                counts[pos][ind] = cnt

parse_gff3(a_path, 0)
parse_csv(b_path, 1)

for pos in counts:
    a = counts[pos][0]
    b = counts[pos][1]
    pos = '{}:{}-{}{}'.format(*pos)
    if a == 1 and b < 1: results[0].append(pos)
    elif a == 1 and b == 1: results[1].append(pos)
    elif a == 1 and b > 1: results[2].append(pos)
    elif a > 1 and b < 1: results[3].append(pos)
    elif a > 1 and b == 1: results[4].append(pos)
    elif a > 1 and b > 1: results[5].append(pos)
    elif a < 1 and b > 0: results[6].append(pos)

print('File.A', a_path, sep='\t')
print('File.B', b_path, sep='\t')
print('')
print('File.A', 'File.B', 'Genes', sep='\t')
print('1', '0', len(results[0]), sep='\t')
print('1', '1', len(results[1]), sep='\t')
print('1', '>1', len(results[2]), sep='\t')
print('>1', '0', len(results[3]), sep='\t')
print('>1', '1', len(results[4]), sep='\t')
print('>1', '>1', len(results[5]), sep='\t')
print('0', '>0', len(results[6]), sep='\t')

with open('compare_gff_to_csv.a1b0.gff3', 'w') as f:
    for i in results[0]:
        print(i, file=f)
with open('compare_gff_to_csv.a1b1.gff3', 'w') as f:
    for i in results[1]:
        print(i, file=f)
with open('compare_gff_to_csv.a1b2.gff3', 'w') as f:
    for i in results[2]:
        print(i, file=f)
with open('compare_gff_to_csv.a2b0.gff3', 'w') as f:
    for i in results[3]:
        print(i, file=f)
with open('compare_gff_to_csv.a2b1.gff3', 'w') as f:
    for i in results[4]:
        print(i, file=f)
with open('compare_gff_to_csv.a2b2.gff3', 'w') as f:
    for i in results[5]:
        print(i, file=f)
with open('compare_gff_to_csv.a0b2.gff3', 'w') as f:
    for i in results[6]:
        print(i, file=f)
