#!/usr/local/bin/python3
# Last updated: 7/9/2018
# Author: Matt Douglas
# Purpose: Count the number of alternative transcript start and end positions.

import os, sys
import gffutils as gff
from collections import defaultdict

path = sys.argv[1]
path_db_name = path + '.db'
result = {'gene':0, 'alt.start':0, 'alt.end':0}

if os.path.isfile(path_db_name):
    print('Query database found. Importing...', file=sys.stderr)
    qry_db = gff.FeatureDB(path_db_name)
else:
    print('Building query database...', file=sys.stderr)
    qry_db = gff.create_db(path,
                           dbfn=path_db_name,
                           merge_strategy='create_unique',
                           disable_infer_genes=True,
                           disable_infer_transcripts=True,
                           verbose=True
                          )

print('Reading database...', file=sys.stderr)
for i in qry_db.features_of_type('gene'):
    starts = set()
    ends = set()
    for n, j in enumerate(qry_db.children(i, level=1)):
        starts.add(j.start)
        ends.add(j.end)
    result['alt.start'] += len(starts)-1
    result['alt.end'] += len(ends)-1
    if len(starts) * len(ends) > 1:
        result['gene'] += 1

for k, v in result.items():
    print(k, v)
