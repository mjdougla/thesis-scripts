#!/usr/local/bin/python3
# Purpose: Compare three sets of transcripts. Return seperate sets that are
#          unique or common to one, two, or three of the sets.

import sys
import gff_utils as gff

def parse_features_gff3(path, ext='.gff3'):
    features = {}
    transcript = set()
    attr_id = ('transcript_id' if ext == '.gtf' else 'Parent')

    with open(path, 'r') as f:
        for i in gff.parse(f, ext):
            seqid = i.seqid, i.strand
            pos = i.start, i.end
            try:
                t_id = i.attr[attr_id][0]
            except IndexError:
                #print('[ERROR] no transcript_id for line:', line.strip(), file=sys.stderr)
                #sys.exit(1)
                continue
            if i.ftype not in ('intron', 'exon'):
                continue
            if t_id not in features:
                features[t_id] = [seqid, [], []]
            if i.ftype == 'intron':
                features[t_id][1].append(pos)
            elif i.ftype == 'exon':
                features[t_id][2].append(pos)

    for t_id in features:
        seqid, intron, exon = features[t_id]
        if len(exon) < 1:
            print('[ERROR] no exons for transcript:', t_id, file=sys.stderr)
            sys.exit(1)
        elif len(exon) == 1:
            t_sig = tuple([seqid] + [exon[0]])
        else:
            if len(intron) < 1:
                intron = gff.infer_introns(exon)
            t_sig = tuple([seqid] + sorted(intron + exon[1:-1], key=lambda x: x[1]))

        if len(t_sig) < 2:
            print('[ERROR] file {}, for transcript {}'.format(path_list, t_id), file=sys.stderr)
            print('[ERROR]', t_sig, file=sys.stderr)
            sys.exit(1)

        transcript.add(t_sig)

    return transcript


if __name__ == '__main__':
    pathA1 = sys.argv[1]
    pathB1 = sys.argv[2]
    pathC1 = sys.argv[3]
    transcriptA = parse_features_gff3(pathA1, '.gtf')
    transcriptB = parse_features_gff3(pathB1, '.gtf')
    transcriptC = parse_features_gff3(pathC1, '.gff3')
    uniqueA = transcriptA - transcriptB - transcriptC
    uniqueB = transcriptB - transcriptA - transcriptC
    uniqueC = transcriptC - transcriptA - transcriptB
    common = transcriptA.intersection(transcriptB.intersection(transcriptC))
    commonAB = transcriptA.intersection(transcriptB) - common
    commonAC = transcriptA.intersection(transcriptC) - common
    commonBC = transcriptB.intersection(transcriptC) - common

    print('A = {}'.format(pathA1))
    print('B = {}'.format(pathB1))
    print('C = {}'.format(pathC1))
    print('{:,}\tin A'.format(len(transcriptA)))
    print('{:,}\tin B'.format(len(transcriptB)))
    print('{:,}\tin C'.format(len(transcriptC)))
    print('{:,}\tunique to A'.format(len(uniqueA)))
    print('{:,}\tunique to B'.format(len(uniqueB)))
    print('{:,}\tunique to C'.format(len(uniqueC)))
    print('{:,}\tcommon to all'.format(len(common)))
    print('{:,}\tcommon to A and B'.format(len(commonAB)))
    print('{:,}\tcommon to A and C'.format(len(commonAC)))
    print('{:,}\tcommon to B and C'.format(len(commonBC)))

    with open('diff_uniqueA.txt', 'w') as f:
        for j in ['{}:{}-{}{}'.format(i[0][0], i[1][0], i[-1][-1], i[0][-1]) for i in uniqueA]:
            print(j, file=f)
    with open('diff_uniqueB.txt', 'w') as f:
        for j in ['{}:{}-{}{}'.format(i[0][0], i[1][0], i[-1][-1], i[0][-1]) for i in uniqueB]:
            print(j, file=f)
    with open('diff_uniqueC.txt', 'w') as f:
        for j in ['{}:{}-{}{}'.format(i[0][0], i[1][0], i[-1][-1], i[0][-1]) for i in uniqueC]:
            print(j, file=f)
    with open('diff_common.txt', 'w') as f:
        for j in ['{}:{}-{}{}'.format(i[0][0], i[1][0], i[-1][-1], i[0][-1]) for i in common]:
            print(j, file=f)
