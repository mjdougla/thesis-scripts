#!/home2/mattdoug/bin/python3
# Last updated: 6/12/2018
# Author: Matt Douglas
# Purpose: Compare two sets of transcripts to see how many match (or match
#          partially).

import os, sys
import gffutils as gff
from collections import defaultdict

qry = sys.argv[1]
ref = sys.argv[2]
# qry = '/home2/mattdoug/Thesis/transcript_database/3_merged_gffcmpX/all/gffcmp.combined.gff3'
# ref = '/home2/mattdoug/Thesis/reference/c_elegans.PRJNA13758.WS250.gene_models.gff3'
all_intron = '/home2/mattdoug/Thesis/intron_database/v2/intron_database.gff3'
qry_db_name = qry + '.db'
ref_db_name = ref + '.db'
outpref = (sys.argv[3].strip(' .') if len(sys.argv) > 3 else 'compare')
intron_thresh = 5
intron_c = 0
intron_lowc = 0
qry_c = 0
ref_c = 0
intron_low_d = defaultdict(set)
intron_d = defaultdict(set)
qry_d = defaultdict(list)
result = defaultdict(int)

out_match = open('{}.match.gff3'.format(outpref), 'w')
out_msub = open('{}.match_subset.gff3'.format(outpref), 'w')
out_miss = open('{}.missed.gff3'.format(outpref), 'w')

def parse_introns(path):
    with open(path, 'r') as f:
        for line in f:
            if len(line) >0 and line[0] != '#':
                yield line.strip().split('\t')


def max_intron_score(line):
    #Parent=Gene:WBGene00004418;ScorePerLibrary=SRX659949:2
    for i in line.strip().split(';'):
        k, v = i.split('=')
        if k == 'ScorePerLibrary':
            mscore = max([float(x.split(':')[1]) for x in v.split(',')])
            return mscore


print('Parsing introns...', file=sys.stderr)
for intron_c, i in enumerate(parse_introns(all_intron), 1):
    region = i[0], i[6]
    pos = int(i[3]), int(i[4])
    intron_d[region].add(pos)
    if max_intron_score(i[8]) < intron_thresh:
        intron_low_d[region].add(pos)
        intron_lowc += 1


if os.path.isfile(qry_db_name):
    print('Query database found. Importing...', file=sys.stderr)
    qry_db = gff.FeatureDB(qry_db_name)
else:
    print('Building query database...', file=sys.stderr)
    qry_db = gff.create_db(qry,
                             dbfn=qry_db_name,
                             merge_strategy='create_unique',
                             disable_infer_genes=True,
                             disable_infer_transcripts=True,
                             verbose=True
                            )

if os.path.isfile(ref_db_name):
    print('Reference database found. Importing...', file=sys.stderr)
    ref_db = gff.FeatureDB(ref_db_name)
else:
    print('Building reference database...', file=sys.stderr)
    ref_db = gff.create_db(ref,
                           dbfn=ref_db_name,
                           merge_strategy='create_unique',
                           disable_infer_genes=True,
                           disable_infer_transcripts=True,
                           verbose=True
                          )

for qry_c, i in enumerate(qry_db.features_of_type('mRNA'), 1):
    region = (i.seqid, i.strand)
    exons = [(x.start, x.end) for x in qry_db.children(i, featuretype='exon', order_by='start')]
    introns = set([(exons[n][1]+1, exons[n+1][0]-1) for n in range(len(exons)-1)])
    qry_d[region].append(introns)

print('Comparing reference to query...', file=sys.stderr)
for ref_c, i in enumerate(ref_db.features_of_type('transcript'), 1):
    found = False
    region = (i.seqid, i.strand)
    # introns = set([(x.start, x.end) for x in ref_db.children(i, featuretype='intron')])
    exons = [(x.start, x.end) for x in ref_db.children(i, featuretype='exon', order_by='start')]
    introns = set([(exons[n][1]+1, exons[n+1][0]-1) for n in range(len(exons)-1)])
    # compare the reference transcript to the query
    if len(introns) < 1:
        result['single']
        continue
    if introns in qry_d[region]:
        print(i, file=out_match)
        result['match'] += 1
        found = True
        continue
    for x in qry_d[region]:
        if introns.issubset(x):
            print(i, file=out_msub)
            result['match_subset'] += 1
            found = True
            break
    if found:
        continue
    else:
        print(i, file=out_miss)
        if any([x in intron_d[region] for x in introns]):
            result['missed_partial'] += 1
        else:
            result['missed_complete'] += 1
    # check to see if any of the introns are missed due to filtering
    for j in introns:
        # does transcript have an intron outside length threhold?
        if j[1]-j[0]+1 < 30 or 5000 < j[1]-j[0]+1:
            result['out_length'] += 1
            break
        # does transcript have an intron below score threhold?
        if j in intron_low_d[region]:
            result['out_score'] += 1
            break
print('', file=sys.stderr)

# print the results
print('Query path\t{}'.format(qry))
print('Reference path\t{}'.format(ref))
print('Intron path\t{}'.format(all_intron))
print('#'*10)
print('Query Transcripts\t{:,}'.format(qry_c))
print('Reference Transcripts\t{:,}'.format(ref_c))
print('Single-Exon Transcripts\t{:,}'.format(result['single']))
print('#'*10)
print('Introns\t{:,}'.format(intron_c))
print('Introns below threshold\t{:,}'.format(intron_lowc))
print('#'*10)
print('Novel\t{:,}'.format(qry_c-result['match']))
print('Match\t{:,}'.format(result['match']))
print('Subset\t{:,}'.format(result['match_subset']))
print('Missed (Completely)\t{:,}'.format(result['missed_complete']))
print('Missed (Partial)\t{:,}'.format(result['missed_partial']))
print('...intron too long/short\t{:,}'.format(result['out_length']))
print('...low score\t{:,}'.format(result['out_score']))

out_match.close()
out_msub.close()
out_miss.close()

print('Done!', file=sys.stderr)
