#!/usr/local/bin/python3
# Last updated: 5/28/2018
# Author: Matt Douglas
# Purpose: 

import sys
from collections import defaultdict
from itertools import chain

class Transcript(object):
    def __init__(self, seqid, left, right, strand, line):
        self.seqid = seqid
        self.left = left
        self.right = right
        self.strand = strand
        self.line = line
        self.feats = []
        self.gene = None


def parse_gtf_attr(attr):
    # e.g. gene_id "CUFF.1"; transcript_id "CUFF.1.1"; exon_number "1";
    result = {}
    for i in attr.strip()[:-1].split(';'):
        k, v = i.strip().split(' ')
        result[k] = v
    return result


def regions_to_search(parsed_introns, read_length=150):
    """Return a set of non-overlapping regions to search on each chromosome."""
    flatten = chain.from_iterable
    data = defaultdict(list)

    # process by chromosome
    for i in parsed_introns:
        data[i[0]].append([i[1], i[2]])

    # find overlapping regions on each chromosome
    for chrom, ranges in data.items():
        ranges = sorted(flatten(((start, 1), (end+read_length, -1)) for start, end in ranges))

        c, x = 0, 0
        for value, label in ranges:
            if c == 0:
                x = value
            c += label
            if c == 0:
                yield chrom, x-read_length, value


def print_as_gtf(t):
    t.line[-1] = 'gene_id "GENE.{}"; '.format(t.gene) + t.line[-1]
    print(*t.line, sep='\t')
    for i in t.feats:
        i[-1] = 'gene_id "GENE.{}"; '.format(t.gene) + i[-1]
        print(*i, sep='\t')


if __name__ == '__main__':
    flatten = chain.from_iterable
    transcript = {}
    data = defaultdict(list)

    print('Reading file...', file=sys.stderr)
    with open(sys.argv[1], 'r') as f:
        for line in f:
            line = line.strip()
            if line.startswith('#'):
                continue
            i = line.split('\t')
            seqid = i[0]
            kind = i[2]
            left = int(i[3])
            right = int(i[4])
            strand = i[6]
            t_id = parse_gtf_attr(i[8])['transcript_id']
            if kind == 'transcript':
                transcript[t_id] = Transcript(seqid, left, right, strand, i)
            else:
                transcript[t_id].feats.append(i)

    # process by chromosome
    print('Grouping transcripts...', file=sys.stderr)
    for t_id, t in transcript.items():
        data[(t.seqid, t.strand)].append((t.left, t.right, t))

    print('Checking for overlaps...', file=sys.stderr)
    # find overlapping regions on each chromosome
    n = 0
    for x, y in data.items():
        c, x = 0, 0
        overlap = set()
        ranges = sorted(flatten(((left, 1, t), (right+1, -1, t)) for left, right, t in y), key=lambda x: (x[0], x[1]))
        for value, label, t in ranges:
            if c == 0:
                x = value
                overlap = set()
            c += label
            overlap.add(t)
            if c == 0:
                n += 1
                for i in overlap:
                    i.gene = n
                    print_as_gtf(i)
