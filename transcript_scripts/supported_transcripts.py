#!/usr/local/bin/python3
# Last updated: 5/15/2018
# Author: Matt Douglas
# Purpose: Evaluate how many transcripts (in GTF format) have all introns and
#          all exons present in sets of introns and exons respectively.
# NOTE: This script does NOT retain information about expression levels.
# NOTE: "*-evaluate.tsv" only contains multi-exon transcripts.

import sys
from pathlib import Path
import gff_utils as gff
from collections import defaultdict

def as_gff3(seqid, kind, start, end, strand, attr):
    line = seqid, '.', kind, start, end, '.', strand, '.', attr
    line = '\t'.join(list(map(str, line)))
    return line


def evaluate(transcript_path):
    transcript_ext = Path(transcript_path).suffix
    transcript_pref = Path(transcript_path).stem
    parent_str = ('transcript_id' if transcript_ext == '.gtf' else 'Parent')
    has_introns = False
    transcripts = {}
    is_single = 0
    evaluate = defaultdict(set)
    result = open('{}-evaluate.tsv'.format(transcript_pref), 'w')
    final = open('{}-final.gff3'.format(transcript_pref), 'w')
    single = open('{}-single.gff3'.format(transcript_pref), 'w')

    print('Output files start with:', transcript_pref)

    try:
        print('Parsing transcripts:', transcript_path)
        with open(transcript_path, 'r') as f:
            for i in gff.parse(f, ext=transcript_ext):
                if i.ftype == 'intron':
                    t_id = i.seqid, i.strand, i.attr[parent_str][0]
                    has_introns = True
                    try:
                        transcripts[t_id][0].append((i.start, i.end))
                    except KeyError:
                        transcripts[t_id] = [[(i.start, i.end)], []]
                elif i.ftype == 'exon':
                    t_id = i.seqid, i.strand, i.attr[parent_str][0]
                    try:
                        transcripts[t_id][1].append((i.start, i.end))
                    except KeyError:
                        transcripts[t_id] = [[], [(i.start, i.end)]]
    except FileNotFoundError:
        print('Could not find file:', transcript_path)
        return

    if not has_introns:
        print('Infering introns...')
        for t_id in transcripts:
            exons = transcripts[t_id][1]
            introns = gff.infer_introns(exons)
            transcripts[t_id][0] = introns

    print('Checking if introns and exons are in our databases...')
    print('Name', 'Introns', 'Missed.Introns', 'Exons', 'Missed.Exons', '5.Term.Exact', '3.Term.Exact', sep='\t', file=result)
    n = 0 # set default value in case no transcript are found
    for n, t_id in enumerate(transcripts, 1):
        lb, rb = True, True
        seqid, strand, name = t_id
        introns = transcripts[t_id][0]
        exons = transcripts[t_id][1]
        if len(introns) < 1:
            evaluate['single'].add(t_id)
            continue
        l_exon = exons[0]
        r_exon = exons[-1]
        missed_introns = set(introns) - ref_introns[(seqid, strand)]
        missed_exons = set(exons[1:-1]) - ref_exons[(seqid, strand)]
        if l_exon not in ref_exons[(seqid, strand)]:
            if l_exon[1] not in ref_r_sites[(seqid, strand)]:
                missed_exons.add(l_exon)
                lb = False
                if strand == '+':
                    evaluate['diff_l'].add(t_id)
                else:
                    evaluate['diff_r'].add(t_id)
        if r_exon not in ref_exons[(seqid, strand)]:
            if r_exon[0] not in ref_l_sites[(seqid, strand)]:
                missed_exons.add(r_exon)
                rb = False
                if strand == '+':
                    evaluate['diff_r'].add(t_id)
                else:
                    evaluate['diff_l'].add(t_id)
        if len(missed_introns) + len(missed_exons) == 0:
            evaluate['all'].add(t_id)
        elif len(missed_introns) * len(missed_exons) > 0:
            evaluate['missed_both'].add(t_id)
        elif len(missed_introns) >  0:
            evaluate['missed_intron'].add(t_id)
        elif len(missed_exons) >  0:
            evaluate['missed_exon'].add(t_id)
        print(name, len(introns), len(missed_introns), len(exons), len(missed_exons), lb, rb,
              ', '.join(map(str, sorted(missed_exons))), sep='\t', file=result)

    print('{:,} transcripts:'.format(n))
    print('  {:,} were single-exon transcripts (ignored)'.format(len(evaluate['single'])))
    print('  {:,} had all introns and exons'.format(len(evaluate['all'])))
    print('  {:,} only had missing exons'.format(len(evaluate['missed_exon'])))
    print('  {:,} only had missing introns'.format(len(evaluate['missed_intron'])))
    print('  {:,} were missing both introns and exons'.format(len(evaluate['missed_both'])))
    print("  {:,} had a different 5' end".format(len(evaluate['diff_l'])))
    print("  {:,} had a different 3' end".format(len(evaluate['diff_r'])))

    for t_id in sorted(evaluate['all'], key=lambda x: x[2]):
        seqid, strand, name = t_id
        introns = transcripts[t_id][0]
        exons = transcripts[t_id][1]
        t_start, t_end = exons[0][0], exons[-1][1]
        line = as_gff3(seqid, 'mRNA', t_start, t_end, strand, 'ID={}'.format(name))
        print(line, file=final)
        for start, end in exons:
            line = as_gff3(seqid, 'exon', start, end, strand, 'Parent={}'.format(name))
            print(line, file=final)
        for start, end in introns:
            line = as_gff3(seqid, 'intron', start, end, strand, 'Parent={}'.format(name))
            print(line, file=final)

    for t_id in sorted(evaluate['single'], key=lambda x: x[2]):
        seqid, strand, name = t_id
        exon = transcripts[t_id][1][0]
        t_start, t_end = exon[0], exon[1]
        line = as_gff3(seqid, 'mRNA', t_start, t_end, strand, 'ID={}'.format(name))
        print(line, file=single)
        for start, end in exons:
            line = as_gff3(seqid, 'exon', start, end, strand, 'Parent={}'.format(name))
            print(line, file=single)

    result.close()
    final.close()
    single.close()


if __name__ == '__main__':
    intron_path = sys.argv[1]
    exon_path = sys.argv[2]
    transcript_path = sys.argv[3]
    ref_introns = defaultdict(set)
    ref_exons = defaultdict(set)
    ref_l_sites = defaultdict(set)
    ref_r_sites = defaultdict(set)

    print('Parsing intron database:', intron_path)
    with open(intron_path, 'r') as f:
        for i in gff.parse(f, ext='.gff3'):
            ref_introns[(i.seqid, i.strand)].add((i.start, i.end))

    print('Parsing exon database:', exon_path)
    with open(exon_path, 'r') as f:
        for i in gff.parse(f, ext='.gff3'):
            ref_exons[(i.seqid, i.strand)].add((i.start, i.end))
            ref_l_sites[(i.seqid, i.strand)].add(i.start)
            ref_r_sites[(i.seqid, i.strand)].add(i.end)

    print('Parsing transcript file(s)...')
    with open(transcript_path, 'r') as f:
        for path in [i.strip() for i in f.readlines()]:
            print('#'*60)
            evaluate(path)
#     print('Parsing transcript file...')
#     evaluate(transcript_path)
