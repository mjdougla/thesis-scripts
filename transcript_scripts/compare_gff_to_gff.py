#!/usr/local/bin/python3
# Purpose: Used to compare the number of isoforms per gene between a reference
#          gene set and a query gene set.

import sys

a_path = sys.argv[1] # FILE A = Reference
b_path = sys.argv[2] # FILE B = Sample
counts = {}
results = {i:[] for i in range(6)}
a_as = 0
a_noas = 0
b_as = 0
b_noas = 0

def parse_gff3(path, ind):
    with open(path, 'r') as f:
        for line in f:
            if line[0] != '#':
                i = line.strip().split('\t')
                pos = i[0], i[3], i[4], i[6]
                cnt = int(i[9])
                if pos not in counts:
                    counts[pos] = [0, 0]
                counts[pos][ind] = cnt

parse_gff3(a_path, 0)
parse_gff3(b_path, 1)

for pos in counts:
    a = counts[pos][0]
    b = counts[pos][1]
    pos = '{}:{}-{}{}'.format(*pos)
    if a > 1: a_as += 1
    else: a_noas += 1
    if b > 1: b_as += 1
    else: b_noas += 1
    if a == 1 and b == 1: results[0].append(pos)
    elif a == 1 and b > 1: results[1].append(pos)
    elif a > 1 and b == 1: results[2].append(pos)
    elif a > 1 and b > 1: results[3].append(pos)
    elif a >= 1 and b == 0: results[4].append(pos)

print('File.A', a_path, sep='\t')
print('File.B', b_path, sep='\t')
print('')
print('File', 'AS', 'No.AS', sep='\t')
print('File.A', a_as, a_noas, sep='\t')
print('File.B', b_as, b_noas, sep='\t')
print('')
print('File.A', 'File.B', 'Genes', sep='\t')
print('No.AS', 'No.AS', len(results[0]), sep='\t')
print('No.AS', 'AS', len(results[1]), sep='\t')
print('AS', 'No.AS', len(results[2]), sep='\t')
print('AS', 'AS', len(results[3]), sep='\t')
print('Detected', 'Not.Detected', len(results[4]), sep='\t')

with open('compare_gff_to_gff.a1b1.gff3', 'w') as f:
    for i in results[0]:
        print(i, file=f)
with open('compare_gff_to_gff.a1b2.gff3', 'w') as f:
    for i in results[1]:
        print(i, file=f)
with open('compare_gff_to_gff.a2b1.gff3', 'w') as f:
    for i in results[2]:
        print(i, file=f)
with open('compare_gff_to_gff.a2b1.gff3', 'w') as f:
    for i in results[3]:
        print(i, file=f)
with open('compare_gff_to_gff.a1b0.gff3', 'w') as f:
    for i in results[4]:
        print(i, file=f)
