#!/home2/mattdoug/python3/bin/python3
# Last updated: 2/15/2018
# Author: Matt Douglas

from __future__ import print_function, division
import argparse
import pysam
import sys
from Bio import SeqIO
from collections import defaultdict

def parse_arguments():
    parser = argparse.ArgumentParser(description='Calculate the total QV score and PID for each read.')
    parser.add_argument('-1', dest='one', type=str, nargs='?', help='FASTQ file of 1st reads in a pair')
    parser.add_argument('-2', dest='two', type=str, nargs='?', help='FASTQ file of 1st reads in a pair')
    parser.add_argument('-a', dest='bam', type=str, nargs='?', help='an alignment file to check the PID for each read')
    parser.add_argument('-o', dest='out', type=str, nargs='?', default='pid_vs_qv.csv', help='name for the output file')
    args = parser.parse_args()

    if None in (args.one, args.two, args.bam):
        parser.print_help()
        sys.exit(1)

    return args


def parse_cigar(line):
    """Check if the alignment is split, and count mapped bases.
    if(cigar_type == 0):   #match
    elif(cigar_type == 1): #insertions
    elif(cigar_type == 2): #deletion
    elif(cigar_type == 3): #skip
    elif(cigar_type == 4): #soft clipping
    elif(cigar_type == 5): #hard clipping
    elif(cigar_type == 6): #padding
    """
    cigar = line.cigar
    is_split = False
    matches = 0

    cigar_type = [i[0] for i in cigar]
    cigar_len = [i[1] for i in cigar]

    if 3 in cigar_type:
        is_split = True

    for i in [i for i, l in enumerate(cigar_type) if l == 0]:
        matches += cigar_len[i]

    return is_split, matches


def num_mismatches(line):
    edit = None

    for i in line.get_tags():
        if i[0].upper() == 'NM':
            edit = int(i[1])
            break

    return edit


if __name__ == '__main__':
    args = parse_arguments()
    pid_dict = {}
    qual_dict = {}

    # get the quality scores for each read from the FASTQ files
    for path, i in ((args.one, 0), (args.two, 1)):
        print('Reading:', path)
        for record in SeqIO.parse(path, 'fastq'):
            read = record.id.rsplit('/', 1)[0]
            qv = sum(record.letter_annotations['phred_quality'])
            if read not in qual_dict:
                qual_dict[read] = [None, None] # initialize the dict elements
                pid_dict[read] = [0, 0]
            qual_dict[read][i] = qv

    # check each alignment file for the PID of each read
    bam_file = pysam.AlignmentFile(args.bam, 'rb')
    print('Reading:', args.bam)
    for line in bam_file.fetch():
        is_split, matches = parse_cigar(line) # soft-clip bases do not count as mapped
        read = line.query_name
        if line.is_read1:
            i = 0
        elif line.is_read2:
            i = 1
        else:
            print('Could not determine if read 1 or 2:', line)
            sys.exit(1)
        edit = num_mismatches(line)
        pid = (matches - edit) / matches # PID based on aligned portion of read
        if read in pid_dict:
            if pid > pid_dict[read][i]: # if a read maps multiple times, take the highest PID
                pid_dict[read][i] = pid

    print('Printing results to:', args.out)
    not_printed = 0

    with open(args.out, 'w') as f:
        # print the header lines
        print('# 1st FASTQ file:', args.one, file=f)
        print('# 2nd FASTQ file:', args.two, file=f)
        print('# BAM file:', args.bam, file=f)
        print('# Read', 'sum(QV)', 'PID', sep='\t', file=f)
        # output each read seperately
        for read, qv in qual_dict.items():
            pid = pid_dict[read]
            if pid[0] > 0:
                print(read + '/1', qv[0], pid[0], sep='\t', file=f)
            else:
                not_printed += 1
            if pid[1] > 0:
                print(read + '/2', qv[1], pid[1], sep='\t', file=f)
            else:
                not_printed += 1

    print('{:,} reads not found in the alignments (or had PID of zero)'.format(not_printed))
