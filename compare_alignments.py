#!/usr/local/bin/python3
#last updated: 14/5/2017

import sys

# class pcolors:
#     HEADER = '\033[95m'
#     OKBLUE = '\033[94m'
#     OKGREEN = '\033[92m'
#     WARNING = '\033[93m'
#     FAIL = '\033[91m'
#     ENDC = '\033[0m'
#     BOLD = '\033[1m'
#     UNDERLINE = '\033[4m'

def is_first_in_pair(flag):
    """Check the SAM flag to see if the read is the 1st or 2nd in the pair."""
    return '{0:012b}'.format(int(flag))[-7] == '1'


def remove_SEQ_and_QUAL(line):
    """Return a SAM entry without the nucleotide sequence or quality scores."""
    i = line.split('\t')
    new_line = i[:9] + i[11:]
    return '\t'.join(new_line)


def find_same_reads(SAM_A, SAM_B):
    align_dict = {}

    # go through the first SAM file
    with open(SAM_A, 'r') as f:
        for line in f:
            line = line.strip()
            if line[0] != '@':
                read, flag = line.split('\t')[:2]
                if is_first_in_pair(flag):
                    read += '/1'
                else:
                    read += '/2'
                # check if read is in the dict
                if read not in align_dict:
                    align_dict[read] = ([], [])
                align_dict[read][0].append(remove_SEQ_and_QUAL(line))

    # go through the second SAM file
    with open(SAM_B, 'r') as f:
        for line in f:
            line = line.strip()
            if line[0] != '@':
                read, flag = line.split('\t')[:2]
                if is_first_in_pair(flag):
                    read += '/1'
                else:
                    read += '/2'
                # check if read is in the dict
                if read not in align_dict:
                    align_dict[read] = ([], [])
                align_dict[read][1].append(remove_SEQ_and_QUAL(line))

    return align_dict


if __name__ == '__main__':
    SAM_A = sys.argv[1]
    SAM_B = sys.argv[2]

    align_dict = find_same_reads(SAM_A, SAM_B)

    # print the output
    for read, lines in align_dict.items():
        # add a blank space if no entry was found
        if lines[0] == []:
            lines[0].append('')
        if lines[1] == []:
            lines[1].append('')
        # format the output
        print('### {} ###'.format(SAM_A))
        for i in lines[0]:
            print(i)
        print('### {} ###'.format(SAM_B))
        for i in lines[1]:
            print(i)
        print('-'*142)
