#!/usr/local/bin/python3
# Last updated: 2/3/2018
# Author: Matt Douglas

import sys
from collections import defaultdict

if __name__ == '__main__':
    length_dict = defaultdict(list)

    with open(sys.argv[1], 'r') as f:
        for line in f:
            if line[0] != '#':
                line = line.strip()
                i = line.split('\t')
                length = int(i[4]) - int(i[3]) + 1
                length_dict[length].append(line)

    sorted_lengths = sorted(length_dict.keys(), reverse=True) # sort in descending order

    for length in sorted_lengths:
        lines = length_dict[length]
        for i in lines:
            print(i)
