#!/usr/local/bin/python
# Last updated: 28/7/2017

from __future__ import print_function
import argparse, re, sys, pysam
from collections import defaultdict

#####################
# Utility functions #
#####################
def eprint(*args, **kwargs):
    """Print to stderr."""
    print(*args, file=sys.stderr, **kwargs)


def parse_name(attr):
    names = []

    for i in attr.split(';'):
        if i[:7] == 'Parent=':
            for j in i.split(','):
                name = j.split(':')[1]
                names.append(name)

    return names


def parse_parent(name):
    parent = '.'.join(name.split('.')[:2]) #Y71H2AM.19a.1 <- ".1" indicates different UTRs
    if parent[-1].isalpha():
        parent = parent[:-1]

    return parent


def parse_commandline_arguments():
    """Parse command line arguments and return:
        1) A SAM formatted file
        2) A list of introns to search for
    """
    parsed_introns = []

    # parse command line options
    parser = argparse.ArgumentParser(description='Return alignments supporting one or more specified introns.')
    parser.add_argument('-a', type=str, nargs='?', help='a SAM or BAM file')
    parser.add_argument('-g', type=str, nargs='?', help='a GFF3 file of gene models')
    args = parser.parse_args()

    return args.a, args.g


#######################################
# Functions for doing the actual work #
#######################################
def parse_GFF3(GFF3_file):
    """Parse a GFF3 file and return all intron coordinates."""
    transcript_dict = defaultdict(list)

    with open(GFF3_file, 'r') as f:
        for line in f:
            if line[0] != '#':
                i = line.strip().split('\t')
                kind, attr = i[2], i[8]
                pos = i[0], int(i[3]), int(i[4])
                if kind == 'CDS':
                    for name in parse_name(attr):
                        transcript_dict[name].append(pos)

    return transcript_dict


def transcript_coverage(samfile, transcript_dict):
    cov_dict = {}
    count = 0
    total = len(transcript_dict)

    for transcript, exons in transcript_dict.items():
        cov_dict[transcript] = []
        count += 1
        for exon in exons:
            cov = 0
            for pileupcolumn in samfile.pileup(*exon):
                pos = pileupcolumn.pos + 1
                if pos >= exon[1] and pos <= exon[2]:
                    for pileupread in pileupcolumn.pileups:
                        if not pileupread.is_del and not pileupread.is_refskip:
                            cov += 1 # query position is None if is_del or is_refskip is set.
                            break    # right now I don't care what the depth of coverage is
            cov_dict[transcript].append(cov)

        eprint('\r {:,}/{:,} transcripts checked'.format(count, total), end='')

    eprint('')
    return cov_dict


def output_matching(match, GFF3_file):
    with open(GFF3_file, 'r') as f:
        for line in f:
            line = line.strip()
            if line[0] != '#':
                i = line.strip().split('\t')
                kind, attr = i[2], i[8]
                if kind in ('CDS', 'exon', 'intron', 'three_prime_UTR', 'five_prime_UTR'):
                    for name in parse_name(attr):
                        if name in match:
                            yield line


#############
# Main loop #
#############
if __name__ == '__main__':
    summary = {'Complete':[], 'Partial':[], 'None':[], 'No exons found':[], 'Error':[]}

    eprint('Parsing input files...')
    alignments, gff_path = parse_commandline_arguments()
    transcript_dict = parse_GFF3(gff_path)
    samfile = pysam.AlignmentFile(alignments, 'rb')

    eprint('Checking transcript coverage...')
    results = transcript_coverage(samfile, transcript_dict)

    eprint('Categorizing coverage results...')
    for transcript, cov_list in results.items():
        try:
            if len(cov_list) == 0: # check for errors
                summary['No exons found'].append(transcript)
            elif min(cov_list) > 0:
                summary['Complete'].append(transcript)
            elif sum(cov_list) > 0:
                summary['Partial'].append(transcript)
            elif sum(cov_list) == 0:
                summary['None'].append(transcript)
        except ValueError:
            summary['Error'].append(transcript)

    for i in ['Complete', 'Partial', 'None', 'No exons found', 'Error']:
        print('{}\t{:,}'.format(i, len(summary[i])))

    # print each level of coverage to seperate files
    eprint('Sorting GFF3 features...')
    with open('coverage_complete.gff3', 'w') as f:
        for line in output_matching(summary['Complete'], gff_path):
            print(line, file=f)
    with open('coverage_partial.gff3', 'w') as f:
        for line in output_matching(summary['Partial'], gff_path):
            print(line, file=f)
    with open('coverage_none.gff3', 'w') as f:
        for line in output_matching(summary['None'], gff_path):
            print(line, file=f)

    # clean up
    samfile.close()
    eprint('Done!')
