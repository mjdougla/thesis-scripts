#!/usr/local/bin/python
#last updated: 18/12/2017

# PURPOSE: Annotate a GFF3 file with the splice signal of each intron.
# INPUT:   reference genome in FASTA format, a intron file in GFF3 format
# USAGE:   splice_junction_usage.py reference.fa junctions.gff3

from __future__ import print_function
import sys
from Bio import SeqIO
from collections import defaultdict

class Intron(object):
    __slots__ = ['start', 'end', 'strand', 'count', 'junction']
    def __init__(self, start, end, count, strand):
        self.start    = start
        self.end      = end
        self.count    = count
        self.strand   = strand
        self.junction = ''


def parse_GFF3(gff3_path):
    ''' Parse a GFF3 file to find the splice signal used by each intron '''
    intron_dict = defaultdict(list)

    with open(gff3_path, 'r') as f:
        for line in f:
            line = line.strip()
            if line[0] != '#':
                i = line.split('\t')
                chrom, start, end, strand = i[0], int(i[3]), int(i[4]), i[6]
                try: count = int(i[5])
		except ValueError: count = '.'
		intron = Intron(start, end, count, strand)
                intron_dict[chrom].append(intron)

    return intron_dict


def find_junctions(ref_path, intron_dict):
    ''' Find the nucleotides at each splice junction '''
    junction_dict = {}

    # check each chromosome for the specified introns
    for record in SeqIO.parse(ref_path, 'fasta'):
        chrom, nuc = record.id, record.seq
        for intron in intron_dict[chrom]:
            start, end, strand = intron.start, intron.end, intron.strand
            # translate the intron sequence
            f_seq = nuc[start-1:end].upper()
            r_seq = f_seq.reverse_complement()
            # find the splice sites
            if strand == '+':
                left  = f_seq[:2]
                right = f_seq[-2:]
                junction = left + '/' + right
            elif strand == '-':
                left  = r_seq[:2]
                right = r_seq[-2:]
                junction = left + '/' + right
            else:
                f_left   = f_seq[:2]
                f_right  = f_seq[-2:]
                r_left   = r_seq[:2]
                r_right  = r_seq[-2:]
                junction = junction = f_left + '/' + f_right + ' or ' + r_left + '/' + r_right
            # add the results
            intron.junction = junction

    return intron_dict


def print_as_gff3(intron_dict, gff3_path):
    print('##gff-version 3')
    print('##Input file =', gff3_path)
    for chrom, introns in intron_dict.items():
        for i in introns:
            start, end, count, strand = i.start, i.end, i.count, i.strand
            junction = 'Note=' + i.junction
            line   = chrom, '.', 'intron', start, end, count, strand, '.', junction
            print(*line, sep='\t')


if __name__ == '__main__':
    ref_path  = sys.argv[1]
    gff3_path = sys.argv[2]

    intron_dict = parse_GFF3(gff3_path)
    find_junctions(ref_path, intron_dict)
    print_as_gff3(intron_dict, gff3_path)
