#!/usr/local/bin/python3
# Last updated: 2/3/2018
# Author: Matt Douglas
# USAGE: categorize_wormbase_cds.py <reference.gff3> <output_prefix> <feature_type>

import sys
from collections import defaultdict

def parse_gff(gff_path, target):
    """Parse a GFF formatted file and return the coordinates, as well as the
    actual line, of features matching type 'target'
    """
    with open(gff_path, 'r') as f:
        for line in f:
            line = line.strip()
            if line[0] != '#':
                i = line.split('\t')
                pos = i[0], int(i[3]), int(i[4]), i[6]
                kind = i[2]
                if kind == target:
                    yield pos, line


def sort_features(features):
    """Sort GFF3 formatted entries by chromosome, then start position, then end
    position, then strand. NOTE: C. elegans uses roman numerals for chromosomes names.
    """
    numerals = {'I':1, 'II':2, 'III':3, 'IV':4, 'V':5, 'X':10, 'MtDNA':11}
    try:
        return sorted(features, key = lambda x: (numerals[x[0]], int(x[1]), int(x[2]), x[3]))
    except KeyError:
        return sorted(features, key = lambda x: (x[0], int(x[1]), int(x[2]), x[3]))


if __name__ == '__main__':
    line_dict = defaultdict(list)
    cds_dict  = {}
    adj_dict  = defaultdict(set)
    gff_path  = sys.argv[1]
    prefix    = [sys.argv[2] if len(sys.argv) > 1 else 'coding'][0]
    out1      = []
    out2      = []
    out3      = []
    out4      = []

    # get all the features labelled "CDS"
    for cds, line in parse_gff(gff_path, 'CDS'):
        chrom, left, right, strand = cds
        line_dict[cds].append(line)
        cds_dict[cds] = [ set(), set() ]
        adj_dict[(chrom, left, strand)].add(cds)
        adj_dict[(chrom, right, strand)].add(cds)

    # get all the features labelled "intron" and which CDSs they're adjacent to
    for intron, _ in parse_gff(gff_path, 'intron'):
        chrom, left, right, strand = intron
        for cds in adj_dict[(chrom, right+1, strand)]:
            cds_dict[cds][0].add(intron)
        for cds in adj_dict[(chrom, left-1, strand)]:
            cds_dict[cds][1].add(intron)

    # check which CDSs are adjacent to 2, 1, or 0 introns
    for cds, adj_introns in cds_dict.items():
        l_introns = adj_introns[0]
        r_introns = adj_introns[1]
        # internal CDSs have introns on both sides
        if len(l_introns) * len(r_introns) > 0:
            out1.append(cds)
        # terminal CDSs are adjacent to 1 intron
        elif len(l_introns) == 0 and len(r_introns) > 0:
            if cds[3] == '+':
                out2.append(cds)
            elif cds[3] == '-':
                out3.append(cds)
        elif len(l_introns) > 0 and len(r_introns) == 0:
            if cds[3] == '+':
                out3.append(cds)
            elif cds[3] == '-':
                out2.append(cds)
        # single CDSs have no intron on either side
        elif len(l_introns) * len(r_introns) == 0:
            out4.append(cds)

    # sort the results by position
    out1 = sort_features(out1)
    out2 = sort_features(out2)
    out3 = sort_features(out3)
    out4 = sort_features(out4)

    # output the results
    for out, suf in ( (out1, 'internal'), (out2, 'five_term'), (out3, 'three_term'), (out4, 'single')):
        with open(prefix+'.'+suf+'.gff3', 'w') as f:
            print('#gff-version 3', file=f)
            for cds in out:
                for line in line_dict[cds]:
                    print(line, file=f)
