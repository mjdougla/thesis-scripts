#!/usr/local/bin/python
#last updated: 15/12/2017

# PURPOSE: Return the frequency of each base at each splice site
# INPUT:   reference genome in FASTA format and a GFF3 file containing introns
# USAGE:   intron_consensus_seq.py reference.fa introns.gff3

# C. elegans consensus sequences:
# 5' splice site:
# -3 -2 -1 | +1 +2 +3 +4 +5 +6 +7 +8
#####################################
#  M  A  G |  G  T  R  A  G  T  T  T
#
# 3' splice site:
# -7 -6 -5 -4 -3 -2- 1 | +1
############################
#  T  T  T  T  C  A  G |  R

from __future__ import division, print_function
import sys
from collections import defaultdict
from Bio import SeqIO

def parse_gff3(path):
    intron_dict = defaultdict(list)

    with open(path, 'r') as f:
        for line in f:
            if line[0] != '#':
                line = line.split('\t')
                chrom = line[0]
                kind  = line[2]
                strnd = line[6]
                pos   = int(line[3]), int(line[4]), strnd
                if kind == 'intron' and strnd in ('+', '-'):
                    intron_dict[chrom].append(pos)

    return intron_dict


if __name__ == '__main__':
    genome_file = sys.argv[1]
    intron_dict = parse_gff3(sys.argv[2])
    prefix      = [sys.argv[3] if len(sys.argv) > 3 else 'consensus'][0]
    five_path   = prefix + '_five_prime.fa'
    three_path  = prefix + '_three_prime.fa'
    result_path = prefix + '_summary.csv'
    bases = 'GATCN'
    total = 0

    # open two files to store the splice site sequences
    five_seqs   = open(five_path, 'w')
    three_seqs  = open(three_path, 'w')
    result_file = open(result_path, 'w')

    # build two 2D matrices to hold the results (one for each splice site)
    l_sites = [ [0 for b in bases] for i in range(11)]
    r_sites = [ [0 for b in bases] for i in range(8)]

    # check each chromosome for the specified introns
    for record in SeqIO.parse(genome_file, 'fasta'):
        chrom, nuc = record.id, record.seq.upper()
        if chrom in intron_dict:
            for intron in intron_dict[chrom]:
                total += 1
                left, right, strand = intron
                if strand == '+':
                    l_pos = left-4, left+7
                    r_pos = right-7, right+1
                    l_seq = nuc[ l_pos[0] : l_pos[1] ]
                    r_seq = nuc[ r_pos[0] : r_pos[1] ]
                elif strand == '-':
                    l_pos = right-8, right+3
                    r_pos = left-2, left+6
                    l_seq = nuc[ l_pos[0] : l_pos[1] ].reverse_complement()
                    r_seq = nuc[ r_pos[0] : r_pos[1] ].reverse_complement()
                # print the sequences to files
                print('>{} {}-{}'.format( total, *l_pos ), file=five_seqs)
                print(l_seq, file=five_seqs)
                print('>{} {}-{}'.format( total, *r_pos ), file=three_seqs)
                print(r_seq, file=three_seqs)
                # add the results to the list
                for seq, results in ( (l_seq, l_sites), (r_seq, r_sites) ):
                    for n, i in enumerate(seq):
                        if i == 'G':
                            results[n][0] += 1
                        elif i == 'A':
                            results[n][1] += 1
                        elif i == 'T':
                            results[n][2] += 1
                        elif i == 'C':
                            results[n][3] += 1
                        else:
                            results[n][4] += 1

    # determine the most frequently used base at each position
    l_freq = [bases[i.index(max(i))] for i in l_sites]
    r_freq = [bases[i.index(max(i))] for i in r_sites]

    # calculate the percent usage for each nucleotide
    l_sites = [ [n / total for n in i] for i in l_sites]
    r_sites = [ [n / total for n in i] for i in r_sites]

    # print the raw results to file
    print("5' site", '-3', '-2', '-1', '+1', '+2', '+3', '+4', '+5', '+6', '+7', '+8', sep='\t', file=result_file)
    for n, nuc in enumerate(bases):
        print(nuc, *[i[n] for i in l_sites], sep='\t', file=result_file)
    print("3' site", '-7', '-6', '-5', '-4', '-3', '-2', '-1', '+1', sep='\t', file=result_file)
    for n, nuc in enumerate(bases):
        print(nuc, *[i[n] for i in r_sites], sep='\t', file=result_file)

    # print the results in human readable format
    print('Input file =', sys.argv[2])
    print('Total introns = {:,}'.format(total))
    print("Wrote 5' splice sites to: {}".format(five_path))
    print("Wrote 3' splice sites to: {}".format(three_path))
    print("Wrote base frequencies to: {}".format(result_path))

    print("\n5' Splice Site Consensus")
    print("########################")
    print('  ', '-3', '-2', '-1 |   +1', '+2', '+3', '+4', '+5', '+6', '+7', '+8', sep=' '*3)
    for n, nuc in enumerate(bases):
        l_line = ['{0:.2f}'.format(i[n]) for i in l_sites[:3]]
        r_line = ['{0:.2f}'.format(i[n]) for i in l_sites[3:]]
        print( nuc+':', ' '.join(map(str, l_line)), '|', ' '.join(map(str, r_line)))
    print(' '*5, '    '.join(l_freq[:3]), '|   ', '    '.join(l_freq[3:]))

    print("\n3' Splice Site Consensus")
    print("########################")
    print('  ', '-7', '-6', '-5', '-4', '-3', '-2', '-1 |   +1', sep=' '*3)
    for n, nuc in enumerate(bases):
        l_line = ['{0:.2f}'.format(i[n]) for i in r_sites[:7]]
        r_line = ['{0:.2f}'.format(i[n]) for i in r_sites[7:]]
        print( nuc+':', ' '.join(map(str, l_line)), '|', ' '.join(map(str, r_line)))
    print(' '*5, '    '.join(r_freq[:7]), '|   ', '    '.join(r_freq[7:]))

    # clean up
    five_seqs.close()
    three_seqs.close()
    result_file.close()
