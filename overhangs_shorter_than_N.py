#!/usr/local/bin/python3

import sys, re

def min_overhang_size(cigar):
    soft_clipped = False
    has_indel = False

    left = 0
    right = 0

    # split the CIGAR string
    nums = list(map(int, re.findall(r"[0-9]+", cigar)))
    lets = re.findall(r"[A-Z]+", cigar)

    # find the size of the overhang on each side, not counting soft-clipping
    junctions = [n for n, i in enumerate(lets) if i == 'N']
    for n, i in enumerate(nums[:junctions[0]]):
        if lets[n] == 'M':
            left += i
        elif lets[n] == 'D':
            left += i
            indel = True
        elif lets[n] == 'S':
            soft_clipped = True
    for n, i in enumerate(nums[junctions[-1]:]):
        if lets[n] == 'M':
            right += i
        elif lets[n] == 'D':
            right += i
            indel = True
        elif lets[n] == 'S':
            soft_clipped = True

    return min(left, right), soft_clipped, has_indel


if __name__ == '__main__':
    num_shorter_than = 0
    num_soft_clipped = 0
    num_indels = 0

    SAM_file = sys.argv[1]
    N = [int(sys.argv[2]) if len(sys.argv) > 2 else 2][0]

    with open(SAM_file, 'r') as f:
        for line in f:
            if line[0] != '@':
                cigar = line.split('\t')[5]
                if 'N' in cigar:
                    min_size, soft_clipped, has_indel = min_overhang_size(cigar)
                    if min_size <= N:
                        num_shorter_than += 1
                        if soft_clipped:
                            num_soft_clipped += 1
                        if has_indel:
                            num_indels += 1

    print('Number of alignments with overhangs {} or less = {}'.format(N, num_shorter_than))
    print('Number of those alignments that are soft-clipped (soft-clipping is not counted!) = {}'.format(num_soft_clipped))
    print('Number of those alignments that have insertions or deletions = {}'.format(num_indels))
