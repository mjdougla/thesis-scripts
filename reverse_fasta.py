#!/usr/bin/python

import sys

def reverse_complement(seq):
    ''' Return the reverse complement of a given DNA or RNA string. '''
    if 'U' in seq:
        seq_dict = {'A':'U', 'U':'A', 'G':'C', 'C':'G'}
    else:
        seq_dict = {'A':'T', 'T':'A', 'G':'C', 'C':'G'}

    return ''.join([seq_dict[base] for base in reversed(seq)])

print('Test: '+"ATCG")
print('Test: '+reverse_complement('ATCG'))

inf = str(sys.argv[1])
ouf1 = open(inf.strip('fasta').strip('reorder')+'fwd', 'w')
ouf2 = open(inf.strip('fasta').strip('reorder')+'rev', 'w')
inf = open(inf, 'r')

header = inf.readline() # write the sequence ID to the output
ouf1.write(header)
ouf2.write(header) # write the sequence ID to the output

# Read in the entire sequence
raw = ''
while True:
	line = inf.readline()
	if not line:
		break
	else:
		raw += line.strip()

if len(sys.argv) > 2:
	a = int(sys.argv[2])
else:
	a = 0
if len(sys.argv) > 3:
	b = int(sys.argv[3])
else:
	b = len(raw)

# Splice the sequence at the desired position
first = raw[:a].strip()
second = raw[a:b].strip()
third = raw[b:].strip()

ouf1.write(first+second+third)
ouf2.write(first+reverse_complement(second)+third)

'''
# Write the newly arranged sequence
for i in range(0, len(first), 100):
	x = first[i:i+100]
	ouf.write(x)
	if len(x) >= 100:
		ouf.write('\n')

for i in range(0, len(second), 100):
	x = second[i:i+100]
	ouf.write(x)
	if len(x) >= 100:
		ouf.write('\n')

for i in range(0, len(third), 100):
	x = third[i:i+100]
	ouf.write(x)
	if len(x) >= 100:
		ouf.write('\n')
'''

inf.close()
ouf1.close()
ouf2.close()

print('done')
