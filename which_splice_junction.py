#!/usr/local/bin/python
#last updated: 15/12/2017

# PURPOSE: Return the splice junction used by a given intron
# INPUT:   reference genome in FASTA format and an intron in the format
#          "I:1234..1345"
# USAGE:   which_splice_junction.py reference.fa <intron coordinate>

from __future__ import print_function
import sys, re
from collections import defaultdict
from Bio import SeqIO

def parse_intron(intron_coord):
    """Parse a set of coordinates in the form 'I:1234..1345+' into: chromosome,
    strand, start, and end position.
    """
    if intron_coord[-1] not in ['+', '-']:
        #print 'Please include strand information (e.g. I:1234..1345+)'
        intron_coord = intron_coord + '.'

    intron_coord, strand = intron_coord[:-1], intron_coord[-1]
    intron_coord = intron_coord.strip().replace(',', '').replace('..', '-')
    chrom, start, end = re.split(':|-|_', intron_coord)

    return chrom, int(start), int(end), strand


if __name__ == '__main__':
    genome_file = sys.argv[1]
    intron_list = sys.argv[2:]
    intron_dict = defaultdict(list)

    # assign each intron their respective chromosomes
    for intron in intron_list:
        chrom, start, end, strand = parse_intron(intron)
        intron_dict[chrom].append( (start, end, strand) )

    # check each chromosome for the specified introns
    for record in SeqIO.parse(genome_file, 'fasta'):
        chrom, nuc = record.id, record.seq
        for intron in intron_dict[chrom]:
            start, end, strand = intron
            # translate the intron sequence
            f_seq = nuc[start-1:end].upper()
            r_seq = f_seq.reverse_complement()
            # find the splice sites
            if strand == '+':
                left  = f_seq[:2]
                right = f_seq[-2:]
                junction = left + '/' + right
            elif strand == '-':
                left  = r_seq[:2]
                right = r_seq[-2:]
                junction = left + '/' + right
            else:
                f_left  = f_seq[:2]
                f_right = f_seq[-2:]
                r_left  = r_seq[:2]
                r_right = r_seq[-2:]
                junction = junction = f_left + '/' + f_right + ' or ' + r_left + '/' + r_right
            # print the result
            print('{}:{}-{} ({} strand)\t{}'.format( chrom, start, end, strand, junction ))
