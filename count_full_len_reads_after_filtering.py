#!/usr/local/bin/python

# Author: Matt Douglas
# Purpose: Compare 2 FASTQ files containing the same reads before and after
# filtering (e.g. with Trimmomatic), and report how many reads were not trimmed
# at all.

from __future__ import division, print_function
import gzip
import sys
from Bio import SeqIO

pprint = lambda *a, **k: print(*a, file=sys.stderr, **k) #print progress info to STDERR

class Read(object):
    __slots__ = ('a_before', 'a_after', 'b_before', 'b_after')
    def __init__(self):
        self.a_before = 0
        self.a_after = 0
        self.b_before = 0
        self.b_after = 0


def read_compressed_fastq(path):
    count = 0
    with gzip.open(path, 'rb') as f:
        # for record in SeqIO.parse(f, 'fastq'):
        #     yield record
        for line in f:
            count += 1
            if count == 1:
                header = line.strip().rsplit('/', 1)[0]
            elif count == 2:
                yield header, line
            if count >= 4:
                count = 0


def read_decompressed_fastq(path):
    with open(path, 'r') as f:
        # for record in SeqIO.parse(f, 'fastq'):
        #     yield record
        for line in f:
            count += 1
            if count == 1:
                header = line.strip().rsplit('/', 1)[0]
            elif count == 2:
                yield header, line
            if count >= 4:
                count = 0


def percent(count, total):
    if total == 0:
        return '0.00%'
    perc = count * 100 / total
    perc = '{0:.2f}%'.format(perc)

    return perc


if __name__ == '__main__':
    is_gzipped = lambda x: x[-3:].lower() == '.gz'

    a_before_path = sys.argv[1] #FASTQ file of reads #1 BEFORE filtering
    a_after_path = sys.argv[2]  #FASTQ file of reads #1 AFTER filtering
    b_before_path = sys.argv[3] #FASTQ file of reads #2 BEFORE filtering
    b_after_path = sys.argv[4]  #FASTQ file of reads #2 AFTER filtering
    length_dict = {}
    full_length = 0
    no_pair = [0, 0, 0, 0]
    total = 0

    if is_gzipped(a_before_path):
        fastq = read_compressed_fastq(a_before_path)
    else:
        fastq = read_decompressed_fastq(a_before_path)
    for n, record in enumerate(fastq):
        header, seq = record
        length_dict[header] = Read()
        length_dict[header].a_before = len(seq)
    #     pprint('\r(1/2) {:,} sequences read from file: {}'.format(n, a_before_path), end='')
    # pprint('')

    if is_gzipped(a_after_path):
        fastq = read_compressed_fastq(a_after_path)
    else:
        fastq = read_decompressed_fastq(a_after_path)
    for n, record in enumerate(fastq):
        header, seq = record
        try:
            length_dict[header].a_after = len(seq)
        except:
            length_dict[header] = Read()
            length_dict[header].a_after = len(seq)
    #     pprint('\r(2/2) {:,} sequences read from file: {}'.format(n, a_after_path), end='')
    # pprint('')

    if is_gzipped(b_before_path):
        fastq = read_compressed_fastq(b_before_path)
    else:
        fastq = read_decompressed_fastq(b_before_path)
    for n, record in enumerate(fastq):
        header, seq = record
        try:
            length_dict[header].b_before = len(seq)
        except:
            length_dict[header] = Read()
            length_dict[header].b_before = len(seq)
    #     pprint('\r(3/4) {:,} sequences read from file: {}'.format(n, b_before_path), end='')
    # pprint('')

    if is_gzipped(b_after_path):
        fastq = read_compressed_fastq(b_after_path)
    else:
        fastq = read_decompressed_fastq(b_after_path)
    for n, record in enumerate(fastq):
        header, seq = record
        try:
            length_dict[header].b_after = len(seq)
        except:
            length_dict[header] = Read()
            length_dict[header].b_after = len(seq)
    #     pprint('\r(4/4) {:,} sequences read from file: {}'.format(n, b_after_path), end='')
    # pprint('')

    for record in length_dict.values():
        total += 1
        if record.a_before == 0:
            no_pair[0] += 1
        elif record.a_after == 0:
            no_pair[1] += 1
        if record.b_before == 0:
            no_pair[2] += 1
        elif record.b_after == 0:
            no_pair[3] += 1
        else:
            if record.a_before == record.a_after and record.b_before == record.b_after:
                full_length += 1

    print('{:,} (out of {:,}) are full-length reads pairs ({})'
          .format(full_length, total, percent(full_length, total)))
    print('{:,} reads were not found in file: {}'.format(no_pair[0], a_before_path))
    print('{:,} reads were not found in file: {}'.format(no_pair[1], a_after_path))
    print('{:,} reads were not found in file: {}'.format(no_pair[2], b_before_path))
    print('{:,} reads were not found in file: {}'.format(no_pair[3], b_after_path))
