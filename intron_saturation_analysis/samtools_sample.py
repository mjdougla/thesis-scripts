#!/home2/mattdoug/bin/python3

import os, random, subprocess, sys
from collections import defaultdict

def convert_str_to_int(string):
    new_int = 0
    try:
        return int(string)
    except ValueError:
        for s in string:
            try:
                new_int += ord(s)
            except ValueError:
                new_int += 1

    return new_int


lib_conf = sys.argv[1]
name = sys.argv[2]
k = int(sys.argv[3])
threads = int(sys.argv[4])
seed_in = sys.argv[5]
seed = convert_str_to_int(seed_in) # seed must be an int to work with samtools
out_intron = '{}.gff3'.format(name)
paths = []
reads = []
probs = []
path_counts = {}
sample_path_n_times = defaultdict(int)
temp_files = []
sampled = 0
total_lines = 0

# set the seed (for both this script and samtools)
random.seed(seed)

print('='*80)
print('File with paths to alignments and alignment counts:', lib_conf)
print('Number of alignments to sample:', k)
print('Output introns to:', out_intron)
print('Threads used by samtools:', threads)
print('Seed: "{}" -> {}'.format(seed_in, seed))


def calc_prob(reads):
    probs = []
    total = sum(reads)
    for n, i in enumerate(reads):
        x = i / total
        probs.append(x)

    return probs


def lines_sampled(sample_path_n_times, sampled):
    for p, n in sample_path_n_times.items():
        r = path_counts[p]
        if n > r:
            n = r
            sample_path_n_times[p] = n
        sampled += n
        path_counts[p] -= n

    return sampled


def samtools_sample_SAM(sample_path_n_times, name, thread, seed, pref='temp'):
    temp_files = []

    for n, p in enumerate(sample_path_n_times):
        c = sample_path_n_times[p]
        if c <= 0:
            continue
        x = c / path_counts[p]
        x_str = '{:.30f}'.format(x)[2:]
        out = '{}_{}_{}.sam'.format(pref, name, n)
        print('='*80)
        print('Sampling {} of {}...'.format(x, p))
        args_str = 'samtools view -@ {} -h -s {}.{} -o {} {}'.format(threads, seed, x_str, out, p)
        args = args_str.split(' ')
        print('[RUNNING]', args_str)
        subprocess.check_call(args)
        temp_files.append(out)

    return temp_files


def samtools_sample_BAM(sample_path_n_times, name, thread, seed, pref='temp'):
    temp_files = []

    for n, p in enumerate(sample_path_n_times):
        c = sample_path_n_times[p]
        if c <= 0:
            continue
        x = c / path_counts[p]
        x_str = '{:.30f}'.format(x)[2:]
        print('='*80)
        print('Sampling {} of {}...'.format(x, p))
        outf_1 = 'temp_{}_{}.bam'.format(name, n)
        outf_2 = 'temp_{}_{}.sorted.bam'.format(name, n)
        # write to BAM, sort, and index
        args_str_1 = 'samtools view -@ {} -s {}.{} -b -o {} {}'.format(threads, seed, x_str, outf_1, p)
        args_str_2 = 'samtools sort -@ {} -o {} {}'.format(threads, outf_2, outf_1)
        args_str_3 = 'rm {}'.format(outf_1)
        args_str_4 = 'samtools index -@ {} {}'.format(threads, outf_2)
        args_1 = args_str_1.split(' ')
        args_2 = args_str_2.split(' ')
        args_3 = args_str_3.split(' ')
        args_4 = args_str_4.split(' ')
        print('[RUNNING]', args_str_1)
        subprocess.check_call(args_1)
        print('[RUNNING]', args_str_2)
        subprocess.check_call(args_2)
        print('[RUNNING]', args_str_3)
        subprocess.check_call(args_3)
        print('[RUNNING]', args_str_4)
        subprocess.check_call(args_4)
        temp_files.append(outf_2)

    return temp_files


# read the config file listing the path and number of reads
with open(lib_conf, 'r') as f:
    for line in f:
        p, r = line.strip().split('\t')[:2]
        r = int(r)
        paths.append(p)
        reads.append(r)
        path_counts[p] = r
        total_lines += r

if k > total_lines:
    raise ValueError('Trial "{}" asks for more alignments than are available (max: {:,})'.format(t, total_lines))

# calculate the probabilities of selecting each run, based on run size
probs = calc_prob(reads)

# determine how many times to sample each run
while True:
    sampled = lines_sampled(sample_path_n_times, sampled)
    if sampled >= k:
        break

    new_reads = []
    to_sample = max((k-sampled), 0)
    for p in random.choices(paths, probs, k=to_sample):
        sample_path_n_times[p] += 1

    for p in paths:
        new_reads.append(path_counts[p])

    probs = calc_prob(new_reads)

# report on how many reads are going to be sampled
print('='*80)
print('Path\tProbability\tCount')
for n, p in enumerate(paths):
    r = sample_path_n_times[p]
    x = probs[n]
    print(p, x, r, sep='\t')

# use samtools to randomly sample each BAM file
# temp_files = samtools_sample_BAM(sample_path_n_times, name, threads, seed)
temp_files = samtools_sample_SAM(sample_path_n_times, name, threads, seed)

# count introns in the temp files
args_str = 'generate_intron_gff3.py -a {}'.format(' '.join(temp_files))
args = args_str.split(' ')
print('='*80)
print('Searching for introns in {:,} temp files...'.format(len(temp_files)))
print('[RUNNING]', args_str)
with open(out_intron, 'w') as f:
    subprocess.check_call(args, stdout=f)

# clean up the temp files
print('='*80)
for i in temp_files:
    os.remove(i)
    #os.remove('{}.bai'.format(i))
    print('Removed temp file:', i)
