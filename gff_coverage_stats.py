#!/usr/local/bin/python
# Last updated: 30/1/2018

from __future__ import division, print_function
import pysam
import sys

def eprint(*args, **kwargs):
    """Print to stderr."""
    print(*args, file=sys.stderr, **kwargs)


def parse_gff3(path):
    """Parse a GFF3 file and return all the feature coordinates."""
    features = set()

    with open(path, 'r') as f:
        for line in f:
            if line[0] != '#':
                i = line.split('\t')
                feature = i[0], int(i[3]), int(i[4]), i[5] # chrom, start, end, support
                features.add(feature)

    features = list(features)

    return features


def get_cov_stats(features):
    cov_stats = { i:[0, 0, 0] for i in features }
    f_total = len(features)

    for n, f in enumerate(features, 1):
        eprint('\r{:,}/{:,} features checked...'.format(n, f_total), end='')
        region = f[0], f[1]-1, f[2]
        min_cov = None
        max_cov = 0
        bp_cov = 0
        total = f[2] - f[1] + 1
        for pileupcolumn in bamfile.pileup(*region, truncate=True):
            depth = 0
            for pileupread in pileupcolumn.pileups:
                if not pileupread.is_del and not pileupread.is_refskip:
                    depth += 1
            if depth < min_cov or min_cov is None:
                min_cov = depth
            if depth > max_cov:
                max_cov = depth
            if depth > 0:
                bp_cov += 1
        if min_cov is None: # no need to update if there is no coverage
            continue
        per_cov = bp_cov / total
        cov_stats[f] = [min_cov, max_cov, per_cov]
    eprint('\r{:,}/{:,} features checked!    '.format(n, total))

    return cov_stats


if __name__ == '__main__':
    bam_path = sys.argv[1]
    gff_path = sys.argv[2]

    bamfile = pysam.AlignmentFile(bam_path, 'rb')
    features = parse_gff3(gff_path)
    eprint('{:,} features total'.format(len(features)))

    cov_stats = get_cov_stats(features)
    sorted_by_pos = sorted(cov_stats, key=lambda x: (x[0], x[1], x[2]))

    print('chrom', 'start', 'end', 'support', 'min', 'max', 'fraction', sep='\t')
    for intron in sorted_by_pos:
        cov_stats[intron][2] = round(cov_stats[intron][2], 2)
        line = list(intron) + cov_stats[intron]
        print(*line, sep='\t')
