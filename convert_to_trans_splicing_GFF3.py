#!/usr/local/bin/python3

import sys

if __name__ == '__main__':
    gff3_file = sys.argv[1]

    with open(gff3_file, 'r') as f:
        for line in f:
            if line[0] == '#':
                print(line)
            else:
                cols = line.strip().split('\t')
                start, end, strand = int(cols[3]), int(cols[4]), cols[6]
                if strand == '+':
                    start = end - 1
                elif strand == '-':
                    end = start + 1
                else:
                    continue    # don't report introns without strand info
                new_line = '\t'.join(map(str, (cols[:3] + [start, end] + cols[5:])))
                print(new_line)
