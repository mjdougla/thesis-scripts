#!/usr/local/bin/python3
# Last updated: 13/10/2017

import sys
from collections import defaultdict
from math import floor, log

def bin_values(infile, bin_size, cutoff=None):
    bins = []
    minimum = 999999
    maximum = -999999

    with open(infile, 'r') as f:
        for line in f:
            line = line.strip()
            if len(line) > 0:
                size = float(line)
                if size < minimum:
                    minimum = size
                if size > maximum:
                    maximum = size

                if cutoff is not None and size > cutoff:
                    size = cutoff + 1

                index = floor(size/bin_size)
                try:
                    bins[index] += 1
                except IndexError:
                    while len(bins) <= index:
                        bins.append(0)
                    bins[index] += 1

    return bins, minimum, maximum


if __name__ == '__main__':
    infile   = sys.argv[1]
    bin_size = int(sys.argv[2])
    cutoff   = [int(sys.argv[3]) if len(sys.argv) > 3 else None][0]

    bins, minimum, maximum = bin_values(infile, bin_size, cutoff)
    if cutoff is not None:
        over_cutoff = bins.pop()

    print('##Total = {}'.format(sum(bins)))
    print('##Min = {}'.format(minimum))
    print('##Max = {}'.format(maximum))
    print('Size\tFreq')
    for v, i in enumerate(bins):
        print('{}-{}\t{}'.format( v*bin_size, (v+1)*bin_size, i ))

    if cutoff is not None:
        print('>{}\t{}'.format( len(bins)*bin_size, over_cutoff ))
