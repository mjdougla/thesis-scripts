#!/usr/local/bin/python3

import re, sys
from collections import defaultdict

attr_name     = re.compile('(?<=Name=)[^;]+(?=(;|))')
attr_seq_name = re.compile('(?<=sequence_name=)[^;]+(?=(;|))')
attr_biotype  = re.compile('(?<=biotype=)[^;]+(?=(;|))')

def eprint(*args, **kwargs):
    """Print to stderr."""
    print(*args, file=sys.stderr, **kwargs)


def parse_attr(attr, label):
    names = []

    for i in attr.split(';'):
        if i.startswith(label):
            for j in i.split(','):
                name = j.split(':')[1]
                names.append(name)

    return names


def parse_parent(name):
    parent = '.'.join(name.split('.')[:2]) #Y71H2AM.19a.1 <- ".1" indicates different UTRs
    if parent[-1].isalpha():
        parent = parent[:-1]

    return parent


if __name__ == '__main__':
    name_dict = {}
    type_dict = defaultdict(list)
    line_dict = {}
    header    = []

    gff_file = sys.argv[1]
    prefix   = [sys.argv[2] if len(sys.argv) > 1 else 'features'][0]

    # get the names of all genes and their biotypes
    with open(gff_file, 'r') as f:
        for line in f:
            line = line.strip()
            if line[0] != '#':
                i = line.split('\t')
                kind = i[2]
                if kind == 'gene':
                    name     = attr_name.search(i[8]).group(0)
                    seq_name = attr_seq_name.search(i[8]).group(0)
                    biotype  = attr_biotype.search(i[8]).group(0)
                    name_dict[seq_name] = name
                    type_dict[biotype].append(name)
                    line_dict[name] = [line]

    # loop through the GFF file and yield any entries that originate from a
    # protein coding gene
    with open(gff_file, 'r') as f:
        for line in f:
            line = line.strip()
            if line[0] == '#':
                header.append(line)
            else:
                i = line.split('\t')
                kind, attr = i[2], i[8]
                if kind != 'gene':
                    for name in parse_attr(attr, 'Parent='):
                        parent = parse_parent(name)
                        try:
                            line_dict[parent].append(line)
                        except KeyError:
                            parent = name_dict[parent]
                            line_dict[parent].append(line)

    # # print each type to seperate files
    # for biotype, name_list in type_dict.items():
    #     outname = '.'.join( (prefix, biotype, 'gff3') )
    #     counter = 0
    #     with open(outname, 'w') as f:
    #         for line in header:
    #             print(line, file=f)
    #         for name in name_list:
    #             for line in line_dict[name]:
    #                 print(line, file=f)
    #                 counter += 1
    #     eprint('Wrote {:,} features (from {:,} genes) matching type "{}" to: {}'
    #            .format(counter, len(name_list), biotype, outname))

    # ALTERNATIVE: split features into coding and non-coding
    counter1 = 0
    counter2 = 0
    path1 = prefix + '.protein_coding.gff3'
    path2 = prefix + '.non_coding.gff3'
    out1 = open(path1, 'w')
    out2 = open(path2, 'w')
    for line in header:
        print(line, file=out1)
        print(line, file=out2)
    for biotype, name_list in type_dict.items():
        if biotype == 'protein_coding':
            for name in name_list:
                for line in line_dict[name]:
                    print(line, file=out1)
                    counter1 += 1
        else:
            for name in name_list:
                for line in line_dict[name]:
                    print(line, file=out2)
                    counter2 += 1

    out1.close()
    out2.close()
    eprint('Wrote {:,} features (from {:,} genes) matching type "protein_coding" to: {}'
           .format(counter1, len(name_list), path1))
    eprint('Wrote {:,} features (from {:,} genes) matching type "non_coding" to: {}'
           .format(counter2, len(name_list), path2))
