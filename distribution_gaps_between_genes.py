#!/usr/local/bin/python3
# Purpose: Return simple stats on the sizes of intergenic regions.

import sys
from collections import defaultdict
from itertools import chain
from statistics import median

flatten = chain.from_iterable
ranges = defaultdict(set)
gaps = []
gc = 0

with open(sys.argv[1], 'r') as f:
    for line in f:
        if line[0] != '#':
            i = line.split('\t')
            region = i[0], i[6]
            pos = int(i[3]), int(i[4])
            if i[2] == 'gene':
                ranges[region].add(pos)
                gc += 1

for i in ranges.values():
    temp = []
    c, x = 0, 0
    i = sorted(flatten(((left, 1), (right, -1)) for left, right in i))
    for value, label in i:
        if c == 0:
            x = value
        c += label
        if c == 0:
            temp.append((x, value))
    for n, j in enumerate(temp[1:]):
        gaps.append(j[0]-temp[n][1])

print('Genes\t{:,}'.format(gc))
print('Gaps\t{:,}'.format(len(gaps)))
print('Min.Gap\t{:,}'.format(min(gaps)))
print('Max.Gap\t{:,}'.format(max(gaps)))
print('Median.Gap\t{:,}'.format(int(median(gaps))))
