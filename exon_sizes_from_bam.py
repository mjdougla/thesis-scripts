#!/usr/local/bin/python3
#last updated: 24/2/2016

import re, sys
from collections import defaultdict

def parse_CIGAR(chromosome, read_start, cigar):
    """Parse a CIGAR string to find the start, end, and size of any exons that
    are entirely covered by a single read.
    """
    exons = list()

    # Split the cigar sting into letteres and numbers
    nums = list(map(int, re.findall(r"[0-9]+", cigar)))
    lets = re.findall(r"[A-Z]+", cigar)
    if lets.count('N') < 2:
        return

    # Calculate the start and size of each exon
    for i in range(len(lets))[1:-1]:
        if lets[i-1] == 'N' and lets[i] == 'M' and lets[i+1] == 'N':   #if surrounded by introns
            start = read_start
            size = nums[i]
            for j in range(len(lets[:i])):
                if lets[j] in ['D', 'M', 'N']:
                    start += nums[j]
            end = start + size - 1
            exons.append((chromosome, start, end, size))

    return exons


def search_SAM(infile):
    """Infer exons where reads are split more than once."""
    exon_dict = defaultdict(int)
    size_freq = defaultdict(int)

    for row in infile:
        if not row.startswith('@'):
            values = row.split('\t')
            chrom, start, cigar = values[2], int(values[3]), values[5]
            if cigar.count('N') > 1:
                exons = parse_CIGAR(chrom, start, cigar)
                if exons is not None:
                    for i in exons:
                        exon_dict[i] += 1
                        size_freq[i[-1]] += 1

    return exon_dict, size_freq


if __name__ == '__main__':
    # Build a dictionary of exons and exon sizes
    with open(sys.argv[1], 'r') as f:
        exon_dict, size_freq = search_SAM(f)

    # Output
    with open('exon_sizes.csv', 'w') as f:
        f.write('Exon\tSize\tCoverage\n')
        for exon, coverage in exon_dict.items():
            #print exon[0], exon[1], exon[2], exon[3], coverage
            #print '{}:{}-{}\t{}\t{}'.format(exon[0], exon[1], exon[2], exon[3], coverage)
            f.write(str(exon[0]) + ':' + str(exon[1]) + '-' + str(exon[2]) + '\t' + str(exon[3]) + '\t' + str(coverage) + '\n')

    with open('exon_size_summary.csv', 'w') as f:
        f.write('Size\tFrequency\n')
        for size, freq in size_freq.items():
            f.write(str(size) + '\t' + str(freq))
