#!/usr/local/bin/python3
# Last updated: 16/3/2018
# Author: Matt Douglas
# PURPOSE: Parse the WormBase GFF3 format gene annotations and yeild the ones
#          with lower-case gene names. These typically have more information
#          available for them and are consequently more interesting for my
#          anlysis.

import sys
import gff3_utils as gff

print('##gff-version 3')
with open(sys.argv[1], 'r') as f:
    for line in gff.parse(f):
        if line.ftype == 'gene':
            if line.attr['biotype'][0] == 'protein_coding':
                for i in line.attr['Alias']:
                    if i.islower():
                        print(line)
