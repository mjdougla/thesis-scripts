#!/usr/local/bin/python3

import re, sys
from collections import defaultdict

attr_gene_name  = re.compile('(?<=sequence_name=)[^;]+(?=(;|))')
attr_biotype    = re.compile('(?<=biotype=)[^;]+(?=(;|))')
attr_trans_name = re.compile('(?<=ID=)[^;]+(?=(;|))')
attr_parent     = re.compile('(?<=Parent=)[^;]+(?=(;|))')

def eprint(*args, **kwargs):
    """Print to stderr."""
    print(*args, file=sys.stderr, **kwargs)


def fr(region):
    """Print a tuple of (chromosome, start position, end position, strand) into
    a readable format.
    """
    return '{}:{:,}-{:,} ({} strand)'.format(region[0], region[1], region[2], region[3])


def parent_gene_name(transcript_id):
    gene_name = '.'.join(transcript_id.split('.')[:2])
    if gene_name[-1].isalpha():
        gene_name = gene_name[:-1]  #Y71H2AM.19a.1 <- ".1" indicates different UTRs

    return gene_name


def transcript_id_names(attrs):
    transcript_ids = attr_parent.search(attrs).group(0)
    transcript_ids = transcript_ids.split(',')
    transcript_ids = [i.split(':')[1] for i in transcript_ids]

    return transcript_ids


def parse_wormbase_transcripts(GFF3_file):
    with open(GFF3_file, 'r') as f:
        for line in f:
            line = line.strip()
            if line[0] != '#':
                i = line.split('\t')
                kind = i[2]
                pos = i[0], int(i[3]), int(i[4]), i[6]
                try:
                    if kind == 'gene':
                        biotype  = attr_biotype.search(i[8]).group(0)
                        seq_name = attr_gene_name.search(i[8]).group(0)
                        gene_type_dict[seq_name] = biotype
                        gene_pos_dict[seq_name]  = pos
                    elif 'RNA' in kind:
                        transcript_id = attr_trans_name.search(i[8]).group(0).split(':')[1]
                        gene_name = parent_gene_name(transcript_id)
                        gene_dict[gene_name].append(transcript_id)
                    elif kind == 'CDS':
                        for i in transcript_id_names(i[8]):
                            trans_cds_dict[i].append(pos)
                    elif kind == 'exon':
                        for i in transcript_id_names(i[8]):
                            trans_exon_dict[i].append(pos)
                    elif kind == 'intron':
                        for i in transcript_id_names(i[8]):
                            trans_intron_dict[i].append(pos)
                except AttributeError:
                    eprint('[AttributeError] Could not parse line:')
                    eprint(line)
                    sys.exit(1)


def unique_features(transcripts, feature_dict):
    unique_features = set()

    for t in transcripts:
        for i in feature_dict[t]:
            unique_features.add(i)

    return unique_features


if __name__ == '__main__':
    gene_dict         = defaultdict(list)  # a list of transcripts per gene
    gene_type_dict    = {}                 # the type of gene (protein coding, etc.)
    gene_pos_dict     = {}                 # the genomic coordinates of each gene
    trans_cds_dict    = defaultdict(list)  # a list of CDSs per transcript
    trans_exon_dict   = defaultdict(list)  # a list of exon per transcript
    trans_intron_dict = defaultdict(list)  # a list of introns per transcript
    GFF3_file = sys.argv[1]

    parse_wormbase_transcripts(GFF3_file)

    print('Name', 'Feature', 'Biotype', 'No.Transcripts', 'No.CDS', 'No.Exons', 'No.Introns', 'Notes', sep='\t')
    for gene, transcripts in gene_dict.items():
        biotype     = gene_type_dict[gene]
        num_trans   = len(transcripts)
        num_cds     = len(unique_features(transcripts, trans_cds_dict))
        num_exon    = len(unique_features(transcripts, trans_exon_dict))
        num_introns = len(unique_features(transcripts, trans_intron_dict))
        notes       = 'Pos:{}, Transcripts:{}'.format( fr(gene_pos_dict[gene]), ','.join(transcripts) )
        print(gene, 'gene', biotype, num_trans, num_cds, num_exon, num_introns, notes, sep='\t')
        if biotype == 'protein_coding':
            for t in transcripts:
                num_cds = len(trans_cds_dict[t])
                num_exon = len(trans_exon_dict[t])
                num_introns = len(trans_intron_dict[t])
                print(t, 'transcript', biotype, num_trans, num_cds, num_exon, num_introns, '.', sep='\t')

    # for gene, transcripts in gene_dict.items():
    #     if gene_type_dict[gene] == 'protein_coding':
    #         cds = unique_features(transcripts, trans_cds_dict)
    #         if len(cds) == 2:
    #             for i in cds:
    #                 print(fr(i))
