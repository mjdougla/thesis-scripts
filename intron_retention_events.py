#!/usr/local/bin/python3
# Last updated: 20/4/2018

import argparse
import sys
from collections import defaultdict

def parse_commandline_arguments():
    parser = argparse.ArgumentParser(description='Compare a set of introns agains a set of exons and look for intron retention events.')
    parser.add_argument('-i',
                        type=str,
                        nargs='?',
                        help='a GFF3 file of introns')
    parser.add_argument('-e',
                        type=str,
                        nargs='?',
                        help="a GFF3 file of exons")
    parser.add_argument('-o',
                        type=str,
                        nargs='?',
                        default="retention",
                        help='prefix for the output file(s) [default: retention]')
    parser.add_argument('-t',
                        type=str,
                        nargs='?',
                        default="both",
                        help='return either "intron" "exon" or "both" [default: both]')
    args = parser.parse_args()

    if None in (args.i, args.e):
        parser.print_help()
        sys.exit(1)

    args.t = args.t.lower()
    if args.t not in ("intron", "exon", "both"):
        print('Unrecognized type:', args.t)
        parser.print_help()
        sys.exit(1)

    return args.i, args.e, args.o, args.t


def parse_gff3(path):
    """Parse a GFF3 file and return all the feature coordinates."""
    feat_dict = defaultdict(set)
    cov_dict = {}

    with open(path, 'r') as f:
        for line in f:
            if line[0] != '#':
                i = line.split('\t')
                chrom, start, end, strand = i[0], int(i[3]), int(i[4]), i[6]
                cov = i[5]
                feat_dict[(chrom, strand)].add((start, end))
                cov_dict[(chrom, start, end, strand)] = cov

    return feat_dict, cov_dict


def intron_retention_events(intron_dict, exon_dict):
    ret_introns = defaultdict(set)
    ret_exons = defaultdict(set)

    for region, introns in intron_dict.items():
        exons = exon_dict[region]
        for i in introns:
            for e in exons:
                if i[0] > e[0] and i[1] < e[1]:
                    ret_introns[region].add(i)
                    ret_exons[region].add(e)

    return ret_introns, ret_exons


def print_as_gff3(feature_dict, cov_dict, path, kind):
    with open(path, 'w') as f:
        print('##gff-version 3', file=f)
        for region, features in feature_dict.items():
            chrom, strand = region
            for i in features:
                start, end = i
                try:
                    cov = cov_dict[(chrom, start, end, strand)]
                except KeyError:
                    cov = '.'
                line = chrom, '.', kind, start, end, cov, strand, '.', '.'
                print(*line, sep='\t', file=f)


if __name__ == '__main__':
    intron_path, exon_path, out_prefix, out_type = parse_commandline_arguments()
    intron_dict, cov_dict = parse_gff3(intron_path)
    exon_dict, _ = parse_gff3(exon_path)
    ret_introns, ret_exons = intron_retention_events(intron_dict, exon_dict)
    if out_type in ("intron", "both"):
        print_as_gff3(ret_introns, cov_dict, out_prefix+'.introns.gff3', 'intron')
    if out_type in ("exon", "both"):
        print_as_gff3(ret_exons, cov_dict, out_prefix+'.exons.gff3', 'exon')
