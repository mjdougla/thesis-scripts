#!/usr/local/bin/python3
#Last updated: 4/1/2018
#Purpose: Change all the optional sequence identifiers in a FASTQ file (3rd
#line) to "+". Fixes issues caused when this identifier is different from the
#1st line (e.g. with Cutadapt)

import gzip, sys

path = sys.argv[1]

def read_gzipped_file(path):
    with gzip.open(sys.argv[1], 'rb') as f:
        for line in f:
            line = line.strip().decode('UTF-8')
            yield line


def read_text_file(path):
    with open(sys.argv[1], 'r') as f:
        for line in f:
            line = line.strip()
            yield line


# kinda hacky; if FASTQ file is gzip compressed, assume it'll end in ".gz"
if path[-3:] == '.gz':
    read_file = read_gzipped_file
else:
    read_file = read_text_file

count = 0
for line in read_file(path):
    count += 1
    if count == 3:
        print('+')
    else:
        print(line)
    # reset counter
    if count >= 4:
        count = 0
