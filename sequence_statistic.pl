#! /usr/bin/perl -w
use strict;
use warnings;

if(@ARGV!=2)
{
	print "\nperl $0 <sequence> <output>\n";
	exit;
}

# purpose:1. sequence sort by length; 2. sum of length, and number larger than 200, 1000; 3. contig, scaffold number, N50, N90, max and min scaffold, contig

my %seq;
my %length;
my $slen=0; # sum of length
my $snum=0; # scaffold number
my $snum1k=0;
my $cnum=0; # contig number
my $cnum1k=0;
my $smax=0; # longest scaffold
my $smaxid=""; # id
my $smin=0; # minimal scaffold
my $sminid=""; # id
my $cmax=0; # longest contig
my $cmaxid=""; # id
my $cmin=0; # minimal contig
my $cminid=""; # id
my $n50=0;
my $n90=0;
my $gc=0;
my $gcnum=0;
my $atnum=0;
open IN,"$ARGV[0]" || die "Cannot open the file '$ARGV[0]'.\n";
$/=">"; <IN>; $/="\n";
while(<IN>)
{
	my $ti=$_;
	chomp $ti;
	my $id=(split(/\s+/,$ti))[0];
	$/=">";
	$seq{$id}=<IN>;
	chomp $seq{$id};
	$seq{$id}=uc $seq{$id};
	my $tempseq=$seq{$id};
	$tempseq=~s/\n$//;
	$seq{$id}=~s/\s+//g;
	$length{$id}=length $seq{$id};
	my @tcn=split(/N+/,$seq{$id});
	$snum++;
	$snum1k++ if($length{$id}>=1000);
	$slen+=$length{$id};
	if($smax<$length{$id})
	{
		$smax=$length{$id};
		$smaxid=$id;
	}
	if($smin==0 || $smin>$length{$id})
	{
		$smin=$length{$id};
		$sminid=$id;
	}
	for(my $i=0;$i<@tcn;$i++)
	{
		my $tlen=length $tcn[$i];
		$cnum++;
		$cnum1k++ if($tlen>=1000);
		if($cmax<$tlen)
		{
			$cmax=$tlen;
			$cmaxid="$id\-contig".($i+1);
		}
		if($cmin==0 || $cmin>$tlen)
		{
			$cmin=$tlen;
			$cminid="$id\-contig".($i+1);
		}
		my $tgc=$tcn[$i];
		$tgc=~s/[GC]//g;
		my $tatlen=length $tgc;
		$atnum+=$tatlen;
		$gcnum+=$tlen-$tatlen;
	}
	$seq{$id}="\>$ti\n$tempseq";
	$/="\n";
}
close IN;
$gc=$gcnum/($gcnum+$atnum);
my $n50tag=0;
my $n90tag=0;
my $countlen=0;
open OA,">$ARGV[0]\.sort";
foreach my $id(sort{$length{$b}<=>$length{$a}} keys %length)
{
#	print OA "$seq{$id}\n";
	$countlen+=$length{$id};
	if($countlen/$slen>=0.5 && $n50tag==0)
	{
		$n50tag=1;
		$n50=$length{$id};
	}
	if($countlen/$slen>=0.9 && $n90tag==0)
	{
		$n90tag=1;
		$n90=$length{$id};
	}
}
close OA;
open OB,">$ARGV[1]";
printf OB "scaffold num: %10.0f\tscaffold num (>=1000): %8.0f\tscaffold length (bp): %16.0f\n",$snum,$snum1k,$slen;
printf OB "contig num: %12.0f\tcontig num (>=1000): %10.0f\t\n",$cnum,$cnum1k;
printf OB "max scaffold length (bp): %10.0f\tmax scaffold id: %16s\n",$smax,$smaxid;
printf OB "min scaffold length (bp): %10.0f\tmin scaffold id: %16s\n",$smin,$sminid;
printf OB "scaffold N50 (bp): %10.0f\tscaffold N90 (bp): %10.0f\tGC: %10.3f\n",$n50,$n90,$gc;
close OB;
