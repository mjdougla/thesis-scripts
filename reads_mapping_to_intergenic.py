#!/usr/local/bin/python

from __future__ import print_function
import pysam, sys
from collections import defaultdict
from itertools import chain

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


def get_intergenic_regions(gff_path):
    flatten = chain.from_iterable
    temp = defaultdict(list)
    genes = defaultdict(list)
    intergenic = {}

    # gene the genomic coordinates of each gene
    with open(gff_path, 'r') as f:
        for line in f:
            if line[0] != '#':
                i = line.split('\t')
                chrom, kind, start, end = i[0], i[2], int(i[3]), int(i[4])
                if kind == 'gene':
                    temp[chrom].append((start, end))

    # find overlapping regions on each chromosome
    for chrom, ranges in temp.items():
        ranges = sorted(flatten(((start, 1), (end+1, -1)) for start, end in ranges))

        c, x = 0, 0
        for value, label in ranges:
            if c == 0:
                x = value
            c += label
            if c == 0:
                genes[chrom].append((x-1, value))

    # get the regions NOT covered by the genes
    prev = 0
    end  = 999999999
    for chrom, ranges in genes.items():
        intergenic[chrom] = []
        for i in ranges[:-1]:
            j = prev, i[0]
            try:
                assert j[0] < j[1]
                intergenic[chrom].append(j)
            except AssertionError as e:
                pass
                # e.args += (segment, j)
                # raise
            prev = i[1]+1
        intergenic[chrom].append((ranges[-1][1]+1, end))

    return intergenic


def check_read_positions(bam_path, intergenic):
    bam = pysam.AlignmentFile(bam_path, 'rb')
    result = 0
    count = 0
    r_total = sum([len(i) for i in intergenic.values()])
    eprint('\rCounting reads...', end='')
    c_total = bam.count()

    for chrom, ranges in intergenic.items():
        for i in ranges:
            start, end = i
            result += bam.count(chrom, start, end)
            count += 1
            eprint('\r{:,}/{:,} ({:,}/{:,} intergenic)'.format(count, r_total, result, c_total), end='')

    eprint('')
    return result, c_total


if __name__ == '__main__':
    gff_path = sys.argv[1]
    bam_path = sys.argv[2]
    eprint('Path: {}'.format(bam_path))

    intergenic = get_intergenic_regions(gff_path)
    result, total = check_read_positions(bam_path, intergenic)

    print('{}: {:,}/{:,} reads mapped (at least partially) to intergenic regions'.format(bam_path, result, total))
