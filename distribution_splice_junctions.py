#!/usr/local/bin/python
#last updated: 12/18/2017

# PURPOSE: Calculate the distribution of splice junction_pos for a set of
#          alignments
# INPUT:   SAM formated alignment file, reference genome in FASTA format
# USAGE:   splice_junction_usage.py reference.fa introns.gff3

from __future__ import print_function
import operator, sys, re
from collections import defaultdict
from Bio import SeqIO

################################
# Utility script for SAM files #
################################
def parse_CIGAR(read_start, cigar):
    """Get the start, end, and size of any introns from the CIGAR string."""
    introns = list()

    nums = list(map(int, re.findall(r"[0-9]+", cigar)))
    lets = re.findall(r"[A-Z]+", cigar)

    for i in [i for i, l in enumerate(lets) if l == 'N']:
        size = nums[i]
        start = read_start
        for j in range(len(lets[:i])):
            if lets[j] in ['D', 'M', 'N']:
                start += nums[j]
        end = start + size - 1

        introns.append((start, end))

    return introns


####################################################
# Parse the input file (either SAM or GFF3 format) #
####################################################
def parse_GFF3(infile):
    ''' Parse a GFF3 file to find the location of each splice junction '''
    intron_dict = defaultdict(set)

    for line in infile:
        if line[0] != '#':
            cols = line.split('\t')
            chrom, start, end, strand = cols[0], int(cols[3]), int(cols[4]), cols[6]
            intron_dict[chrom].add((start, end, strand))

    return intron_dict


def parse_SAM(infile):
    ''' Parse a SAM file to find the location of each splice junction '''
    intron_dict = defaultdict(set)

    for line in infile:
        if line[0] != '@':
            cols = line.split('\t')
            chrom, read_start, cigar = cols[2], int(cols[3]), cols[5]
            for i in reversed(cols):
                if 'XS:A:' in i:
                    strand = i.split(':')[-1]
                    break
                else:
                    strand = '.'
            for start, end in parse_CIGAR(read_start, cigar):
                intron_dict[chrom].add((start, end, strand))

    return intron_dict


if __name__ == '__main__':
    junction_dict = defaultdict(int)
    ref_path   = sys.argv[1]
    query_path = sys.argv[2]

    # determine the type of input file and parse it
    with open(query_path, 'r') as f:
        ext = query_path.split('.')[-1].lower()
        if ext == 'gff3':
            intron_dict = parse_GFF3(f)
        elif ext == 'sam':
            intron_dict = parse_SAM(f)
        else:
            print('Input file must be either SAM or GFF3/GTF!')
            sys.exit(1)

    # check each chromosome for the specified introns
    for record in SeqIO.parse(ref_path, 'fasta'):
        chrom, nuc = record.id, record.seq
        for intron in intron_dict[chrom]:
            start, end, strand = intron
            # translate the intron sequence
            f_seq = nuc[start-1:end].upper()
            r_seq = f_seq.reverse_complement()
            # find the splice sites
            if strand == '+':
                left  = f_seq[:2]
                right = f_seq[-2:]
                junction = left + '/' + right
            elif strand == '-':
                left  = r_seq[:2]
                right = r_seq[-2:]
                junction = left + '/' + right
            else:
                junction = 'unknown'
            # add the results
            junction_dict[junction] += 1

    # output the results
    total = sum(junction_dict.values())
    print('Total', total, sep='\t')
    for junc, count in reversed(sorted(iter(junction_dict.items()), key=operator.itemgetter(1))):
        print(junc, count, sep='\t')
