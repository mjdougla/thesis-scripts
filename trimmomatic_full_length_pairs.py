#!/usr/local/bin/python3

# Trimmomatic log format: tab-seperated columns
# 1) the read name
# 2) the surviving sequence length
# 3) the location of the first surviving base, aka. the amount trimmed from the start
# 4) the location of the last surviving base in the original read
# 5) the amount trimmed from the end"

import sys

if __name__ == '__main__':
    total = 0
    one_trimmed = 0
    both_trimmed = 0
    first_is_trimmed = False
    second_is_trimmed = False

    with open(sys.argv[1], 'r') as f:
        for n, line in enumerate(f):
            line = line.strip().split(' ')
            read = line[0].rsplit('/', 1)[0]
            vals = list(map(int, line[1:]))
            if n % 2 == 0:
                if vals[0] == 0 or vals[1] + vals[3] != 0:
                    first_is_trimmed = True
                last = read
            else:
                total += 1
                if read != last:
                    print('Read pairs do not seem to match up:')
                    print(' '.join(line))
                    sys.exit(1)
                if vals[0] == 0 or vals[1] + vals[3] != 0:
                    second_is_trimmed = True
                if first_is_trimmed and second_is_trimmed:
                    both_trimmed += 1
                elif first_is_trimmed or second_is_trimmed:
                    one_trimmed += 1
                first_is_trimmed = False
                second_is_trimmed = False

    print('Total read pairs = {:,}'.format(total))
    print('Both reads in pair trimmed = {:,}'.format(both_trimmed))
    print('Only one read in pair trimmed = {:,}'.format(one_trimmed))
    print('Neither read trimmed = {:,}'.format(total- both_trimmed - one_trimmed))
