#!/usr/local/bin/python
# Last updated: 30/10/2017

from __future__ import print_function
import argparse, pysam, os, sys
from collections import defaultdict

def parse_commandline_arguments():
    parser = argparse.ArgumentParser(description='Return alignments supporting one or more specified introns.')
    parser.add_argument('-q', type=str, nargs='+', help="one or more SAM/BAM files of reads to query")
    parser.add_argument('-r', type=str, nargs='?', help='one reference SAM/BAM file to search')
    parser.add_argument('-s', type=str, nargs='?', help='suffix to append to the output files [default:ALT]')
    parser.add_argument('-o', type=str, nargs='?', help='if specified, merge all output to this file')
    args = parser.parse_args()

    if args.r is None or args.q is None:
        eprint('Not enough files specified!')
        parser.print_help()
        sys.exit(1)

    args.s = ['ALT' if args.s is None else args.s][0]

    return args.r, args.q, args.s, args.o


def optype(path, op='r'):
    """If the file is BAM formatted, read/write as binary."""
    ext = os.path.splitext(path)[1].lower()
    if ext == '.bam':
        op += 'b'
    return op


def eprint(*args, **kwargs):
    """Print to stderr."""
    print(*args, file=sys.stderr, **kwargs)


if __name__ == '__main__':
    # short function to get the file prefix
    bname = lambda x: os.path.splitext(os.path.basename(x))[0]

    read_dict = defaultdict(set)
    out_dict  = defaultdict(list)

    reference_file, query_files, suffix, out_path = parse_commandline_arguments()
    ref_sam  = pysam.AlignmentFile(reference_file, optype(reference_file, 'r'))

    # read through all the reads in each SAM file...
    for i in query_files:
        eprint('Checking {}'.format(i))
        samfile = pysam.AlignmentFile(i, optype(i, 'r'))
        for line in samfile.fetch():
            read = line.query_name
            read_dict[i].add(read)
        eprint(' {:,} reads in file: {}'.format(len(read_dict[i]), i))

    # ...search for those reads in a different SAM file
    eprint('Searching for corresponding reads in {}'.format(reference_file))
    for line in ref_sam.fetch():
        read = line.query_name
        for i in query_files:
            if read in read_dict[i]:
                out_dict[i].append(line)
    eprint(' {:,} matching reads in file: {}'.format(len(out_dict[i]), reference_file))

    # output the results
    eprint('Writing results...')
    if out_path is not None:
        out_sam = pysam.AlignmentFile(out_path, 'w', template=ref_sam)
        for lines in out_dict.values():
            for l in lines:
                out_sam.write(l)
    else:
        for i in query_files:
            out_path = bname(i)+'.'+suffix+'.bam'
            out_sam  = pysam.AlignmentFile(out_path, 'wb', template=ref_sam)
            for line in out_dict[i]:
                out_sam.write(line)
            eprint(' Wrote {:,} line(s) to {}'.format(len(out_dict[i]), out_path))

    eprint('Done!')
