#!/usr/local/bin/python

from __future__ import division, print_function
import pysam, sys

def compute_cov(exon, samfile):
    bp_cov = 0
    bp_tot = exon[2] - exon[1] + 1

    for col in samfile.pileup(exon[0], exon[1], exon[2]):
        if exon[1] <= col.pos <= exon[2]:
            bp_cov += 1

    return bp_cov / bp_tot


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('USAGE: percent_feature_coverage.py <GFF3> <BAM>')
        sys.exit(1)

    gff_file = sys.argv[1]
    bam_file = pysam.AlignmentFile(sys.argv[2], 'rb')

    with open(gff_file, 'r') as f:
        for line in f:
            if line[0] != '#':
                i    = line.split('\t')
                exon = i[0], int(i[3]), int(i[4]), i[6]
                pcov = compute_cov(exon, bam_file)
                print('{}:{}-{}{}\t{}'.format(exon[0], exon[1], exon[2], exon[3], pcov))
