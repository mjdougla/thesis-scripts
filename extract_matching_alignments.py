#!/usr/local/bin/python
#Last updated: 14/8/2017
#Author: Matt Douglas

from __future__ import print_function
import os, sys, pysam
from time import time

#####################
# Utility functions #
#####################
def eprint(*args, **kwargs):
    """Print to stderr."""
    print(*args, file=sys.stderr, **kwargs)


def optype(path, op='r'):
    """If the file is BAM formatted, read/write as binary."""
    ext = os.path.splitext(path)[1].lower()
    if ext == '.bam':
        op += 'b'
    return op


################
# Do the thing #
################
def parse_read_path(read_path):
    """Read a list of read IDs, each on their own line in a text file."""
    read_set = set()

    with open(read_path, 'r') as f:
        for line in f:
            if len(line) > 1:
                line = line.lstrip('@').lstrip('>').strip()
                read_set.add(line)

    return read_set


def find_matching_reads(samfile, read_set):
    """Read though the BAM file and find all introns, as specified in the CIGAR
    string, with number of supporting reads.
    """
    alignments_matching = {i:[] for i in read_set}
    line_count = 0
    found_count = 0

    # find alignments for each of the specified read IDs
    for line in samfile.fetch():
        line_count += 1
        read_id = line.query_name
        if read_id in read_set:
            found_count += 1
            alignments_matching[read_id].append(line)

    eprint('\r {:,} lines read. {:,} matching alignments found!{}'.format(line_count, found_count, ' '*20))

    # report read IDs that could not be found
    no_match = [read_id for read_id, lines in alignments_matching.items() if len(lines) < 1]
    if len(no_match) > 0:
        eprint(' Could not find alignments matching the following reads:')
        for i in no_match:
            eprint('  ' + i)

    return alignments_matching


######################
# Output the results #
######################
def print_as_SAM(samfile, alignments_matching, out_path):
    """Output the alignments in SAM format."""
    outfile = pysam.AlignmentFile(out_path, optype(out_path, 'w'), template=samfile)

    # print the output
    for read_id, lines in alignments_matching.items():
        for line in lines:
            outfile.write(line)

    outfile.close()


#############
# Main loop #
#############
if __name__ == '__main__':
    # parse command line arguments
    if len(sys.argv) < 3:
        eprint('Incorrect number of arguments!')
        eprint('USAGE: extract_matching_alignments.py alignments.bam [or .sam] list_of_read_ids.txt output_file.sam [or .bam]')
        quit()
    elif len(sys.argv) > 3:
        out_path = sys.argv[3]
    else:
        out_path = 'extracted_alignments.sam'

    in_path = sys.argv[1]
    read_path = sys.argv[2]
    samfile = pysam.AlignmentFile(in_path, optype(in_path, 'r'))
    read_set = parse_read_path(read_path)

    # find matching alignments
    eprint('Searching for {} different read IDs...'.format(len(read_set)))
    alignments_matching = find_matching_reads(samfile, read_set)

    # output the results
    eprint('Writing output to {}'.format(out_path))
    print_as_SAM(samfile, alignments_matching, out_path)

    # clean up
    samfile.close()
