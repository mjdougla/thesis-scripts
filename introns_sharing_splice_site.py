#!/usr/local/bin/python3

import sys
from collections import defaultdict

b_out = open('ss_both_sites.gff3', 'w')
l_out = open('ss_five_site.gff3', 'w')
r_out = open('ss_three_site.gff3', 'w')
n_out = open('ss_neither_site.gff3', 'w')
b_sites = defaultdict(set)
l_sites = defaultdict(set)
r_sites = defaultdict(set)
b_count = 0
l_count = 0
r_count = 0
n_count = 0

for f in (b_out, l_out, r_out, n_out):
    print('##gff-version 3', file=f)

# parse the reference file
with open(sys.argv[1], 'r') as f:
    for line in f:
        if not line.startswith('#'):
            i = line.split('\t')
            chrom, l, r, strand = i[0], int(i[3]), int(i[4]), i[6]
            b_sites[(chrom, strand)].add((l, r))
            l_sites[(chrom, strand)].add(l)
            r_sites[(chrom, strand)].add(r)

# parse the query file and check splice sites
with open(sys.argv[2], 'r') as f:
    for line in f:
        line = line.strip()
        if not line.startswith('#'):
            i = line.split('\t')
            chrom, l, r, strand = i[0], int(i[3]), int(i[4]), i[6]
            if (l, r) in b_sites[(chrom, strand)]:
                b_count += 1
                print(line, file=b_out)
            elif l in l_sites[(chrom, strand)]:
                if strand == '+':
                    l_count += 1
                    print(line, file=l_out)
                else:
                    r_count += 1
                    print(line, file=r_out)
            elif r in r_sites[(chrom, strand)]:
                if strand == '+':
                    r_count += 1
                    print(line, file=r_out)
                else:
                    l_count += 1
                    print(line, file=l_out)
            else:
                n_count += 1
                print(line, file=n_out)

# report the results
total = sum((b_count, l_count, r_count, n_count))
print("Total: {:,}".format(total))
print("Both sites shared: {:,}".format(b_count))
print("5' site only: {:,}".format(l_count))
print("3' site only: {:,}".format(r_count))
print("Neither: {:,}".format(n_count))

b_out.close()
l_out.close()
r_out.close()
n_out.close()
