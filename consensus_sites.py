#!/usr/local/bin/python3

import sys, csv

five_motif, three_motif = 'GT', 'UUUUCAG'
genome = {}
sj_sites = {}

# Build dictionary of chromosome sequences
with open('c_elegans.PRJNA13758.WS250.genomic.fa', 'r') as genome_file:
    for line in genome_file.readlines():
        if line.startswith('>'):
            seq_id = line.strip()
            if seq_id not in genome:
                genome[seq_id] = ''
        else:
            genome[seq_id] += line.strip()

# Get all the splice site locations
infile = open(sys.argv[1], 'r')
gff3reader = csv.reader(infile, delimiter='\t', quotechar='~')
for row in gff3reader:
    if len(row) > 1:
        chrom, five_site, three_site = row[0], int(row[3]), int(row[5])
        if chrom in sj_sites:
            sj_sites[chrom].add((five_site, three_site))
        else:
            sj_sites[chrom] = set((five_site, three_site))

# Loop through all the splice site locations
counter = 10
for chrom in list(sj_sites.keys()):
    for sites in sj_sites[chrom]:
        if counter <= 0:
            print(genome[chrom][sites[0]:sites[0]+len(five_motif)], genome[chrom][sites[1]:sites[1]-len(three_motif)])
            counter -= 1
        else:
            break
