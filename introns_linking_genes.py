#!/usr/local/bin/python3

import re, sys
from collections import defaultdict
from itertools import chain

def iprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


def flatten_regions(gene_dict):
    """Return a set of non-overlapping regions to search on each chromosome."""
    flatten = chain.from_iterable
    results = defaultdict(list)

    for region, ranges in gene_dict.items():
        ranges = sorted(flatten(((left, 1), (right+1, -1)) for left, right in ranges))

        c, x = 0, 0
        for value, label in ranges:
            if c == 0:
                x = value
            c += label
            if c == 0:
                results[region].append( (x-1, value) )

    return results


def parse_genes(path):
    results = defaultdict(list)

    # loop through the GFF file and get any features labelled 'gene'
    with open(path, 'r') as f:
        for line in f:
            if line[0] != '#':
                i = line.split('\t')
                kind = i[2]
                chrom, strand = i[0], i[6]
                left, right = int(i[3]), int(i[4])
                if kind == 'gene':
                    results[(chrom, strand)].append((left, right))

    # merge overlapping genes
    results = flatten_regions(results)

    # sort the genes by position
    for i in results:
        results[i].sort(key=lambda x: (x[0], x[1]))

    return results


def parse_introns(path):
    results = defaultdict(list)
    scores = {}

    # loop through the GFF file and get any features labelled 'intron'
    with open(path, 'r') as f:
        for line in f:
            if line[0] != '#':
                i = line.split('\t')
                chrom, strand = i[0], i[6]
                left, right = int(i[3]), int(i[4])
                score = i[5]
                results[(chrom, strand)].append((left, right))
                scores[(chrom, left, right, strand)] = score

    # sort the introns by position
    for i in results:
        results[i].sort(key=lambda x: (x[0], x[1]))

    return results, scores


def linked_genes(gene_dict, intron_dict):
    linked_dict = defaultdict(list)

    for region, introns in intron_dict.items():
        genes = gene_dict[region]
        # for each intron...
        for i in introns:
            n, g_left, g_right = None, None, None
            # search genes until finding the gene the intron starts in
            for n, g in enumerate(genes):
                if g[0] <= i[0] <= g[1]: # gene found
                    g_left = g
                    break
                elif i[1] <= g[0]: # gone past the gene
                    break
            # search onwards from that gene
            if n is not None:
                for g in genes[n+1:]:
                    if g[0] <= i[1] <= g[1]: # gene found
                        g_right = g
                        break
                    elif i[1] <= g[0]: # gone past the gene
                        break
                # add the result if found
                if None not in (g_left, g_right):
                    i = region[0], i[0], i[1], region[1]
                    linked_dict[(g_left, g_right)].append(i)

    return linked_dict


def print_as_gff3(linked_dict, scores):
    n = 0

    print('##gff-version 3')
    for introns in linked_dict.values():
        for i in introns:
            n += 1
            score = scores[i]
            chrom, start, end, strand = i
            line = chrom, '.', 'intron', start, end, score, strand, '.', 'ID='+str(n)
            print(*line, sep='\t')
        print('#'*90)


if __name__ == '__main__':
    gene_path = sys.argv[1]
    intron_path = sys.argv[2]

    iprint('Parsing gene models... ', end='')
    gene_dict = parse_genes(gene_path)
    iprint('{:,} genes found!'.format(sum(len(i) for i in gene_dict.values())))

    iprint('Parsing introns... ' , end='')
    intron_dict, scores = parse_introns(intron_path)
    iprint('{:,} introns found!'.format(len(scores)))

    iprint('Looking for introns linking genes... ' , end='')
    linked_dict = linked_genes(gene_dict, intron_dict)
    iprint('{:,} linked genes!'.format(len(linked_dict)))

    iprint('Printing results... ', end='')
    print_as_gff3(linked_dict, scores)
    iprint('Done!')
