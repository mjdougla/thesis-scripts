#!/usr/local/bin/python3
#Last updated: 31/7/2017

import sys

def parse_GFF3(infile):
    """Parse a GFF3 format file and merge entries of the same type and
    position.
    """
    est_set = set()

    # group entries by feature position
    with open(sys.argv[1], 'r') as f:
        for line in f:
            if line[0] != '#':
                attrs = line.split('\t')[8].strip().split(';')
                for i in attrs:
                    if i[:3] == 'ID=':
                        val = i.split('=', 1)[1]
                        est_set.add(val)

    return est_set


if __name__ == '__main__':
    infile = sys.argv[1]
    est_set = parse_GFF3(infile)
    print('{:,} unique EST IDs found'.format(len(est_set)))
