#!/home2/mattdoug/python3/bin/python3
# Last updated: 17/4/2018
# Author: Matt Douglas

# PURPOSE: Filter out misalignments due to tandem duplications in the genome.
# USAGE: filter_alignments.py alignments.bam [alignments.filtered.bam] [removed_prefix]

# TODO: account for changed TLEN when removing reads?

import os, pysam, random, re, sys, time
from collections import defaultdict

random.seed('chenlab2018')

def eprint(*args, **kwargs):
    """Print to stderr."""
    print(*args, file=sys.stderr, **kwargs)


def optype(path, op='r'):
    """If the file is BAM formatted, read/write as binary."""
    ext = os.path.splitext(path)[1].lower()
    if ext == '.bam':
        op += 'b'
    return op


def index_alignments(samfile):
    mate1_dict = defaultdict(list)
    mate2_dict = defaultdict(list)
    unpair_dict = defaultdict(list)
    n_unmap = 0
    n_uniq = 0
    n_multi = 0
    n_multi_read = 0
    n = 0

    eprint('Indexing alignment file...')

    for n, line in enumerate(samfile.fetch()):
        read = line.query_name
        length = line.reference_length # does not count soft-clipped bases
        if line.is_unmapped:
            n_unmap += 1
            continue
        if line.get_tag('NH') == 1:
            n_uniq += 1
            continue
        n_multi += 1
        if line.is_read1:
            mate1_dict[read].append((length, n))
        elif line.is_read2:
            mate2_dict[read].append((length, n))
        else:
            unpair_dict[read].append((length, n))

    # count the number of multi-mapping alignments
    for d in (mate1_dict, mate2_dict, unpair_dict):
        for v in d.values():
            if len(v) > 1:
                n_multi_read += 1

    eprint('  Total alignments = {:,}'.format(n))
    eprint('  Uniquely mapped reads = {:,}'.format(n_uniq))
    eprint('  Multi-mapped reads = {:,} ({:,} alignments)'.format(n_multi_read, n_multi))
    eprint('  Unmapped reads = {:,}'.format(n_unmap))

    return mate1_dict, mate2_dict, unpair_dict, n


def filter_alignments(mate1_dict, mate2_dict, unpair_dict):
    discard_short = set()
    discard_rando = set()
    filter_count = 0

    eprint('Selecting alignments to keep...')

    for d in (mate1_dict, mate2_dict, unpair_dict):
        for read, v in d.items():
            min_len = min(v, key = lambda x: x[0])[0]
            new_v = []
            # select the shortest alignment for each read
            for i in v:
                if i[0] == min_len:
                    new_v.append(i)
                else:
                    discard_short.add(i[1])
            # if multiple alignments are equally short, select one a random
            line = random.choice(new_v)
            for i in new_v:
                if i != line:
                    discard_rando.add(i[1])

    eprint('  Alignments removed when selecting the shorter alignment = {:,}'
           .format(len(discard_short)))
    eprint('  Alignments removed when selecting alignments at random = {:,}'
           .format(len(discard_rando)))

    return discard_short, discard_rando


if __name__ == '__main__':
    # short function to get the file prefix
    bname = lambda x: os.path.splitext(os.path.basename(x))[0]

    if len(sys.argv) < 2:
        eprint('USAGE: filter_alignments.py alignments.bam')
        sys.exit(1)
    elif len(sys.argv) == 2:
        in_path = sys.argv[1]
        out_path = bname(in_path) + '.filtered.bam'
        discard_path1 = 'removed_reads.shorter.bam'
        discard_path2 = 'removed_reads.random.bam'
    else:
        in_path = sys.argv[1]
        out_path = sys.argv[2]
        discard_path1 = 'removed_reads.shorter.bam'
        discard_path2 = 'removed_reads.random.bam'

    eprint('Start time: {}'.format(time.strftime('%c')))
    eprint('Path to input: {}'.format(in_path))
    eprint('Path to results: {}'.format(out_path))
    eprint('Reads discraded based on length written to: {}'.format(discard_path1))
    eprint('Reads discraded at random written to: {}'.format(discard_path2))

    samfile = pysam.AlignmentFile(in_path, optype(in_path, 'r'))
    outfile = pysam.AlignmentFile(out_path, 'wb', template=samfile)
    discard_file1 = pysam.AlignmentFile(discard_path1, 'wb', template=samfile)
    discard_file2 = pysam.AlignmentFile(discard_path2, 'wb', template=samfile)

    mate1_dict, mate2_dict, unpair_dict, n_total = index_alignments(samfile)

    discard_short, discard_rando = filter_alignments(mate1_dict, mate2_dict, unpair_dict)

    eprint('Filtering alignments...')
    # build a list of integers the length of the number of lines in the BAM file
    # each integer represents a different output file (0=filtered, 1=shortest,
    # 2=random). this is faster than checking for inclusion at each line
    outdir = [outfile, discard_file1, discard_file2]
    outidx = [0 for i in range(n_total+1)]
    for i in discard_short:
        outidx[i] = 1
    for i in discard_rando:
        outidx[i] = 2
    for n, line in enumerate(samfile.fetch()):
        outdir[outidx[n]].write(line)

    samfile.close()
    outfile.close()
    discard_file1.close()
    discard_file2.close()
    eprint('Finished: {}'.format(time.strftime('%c')))
