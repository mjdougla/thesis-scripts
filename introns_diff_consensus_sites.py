#!/usr/local/bin/python
#last updated: 18/12/2017

# PURPOSE: Return the frequency of each base at each splice site
# INPUT:   reference genome in FASTA format and a GFF3 file containing introns
# USAGE:   intron_consensus_seq.py reference.fa introns.gff3

# C. elegans consensus sequences:
# 5' splice site:
# -3 -2 -1 | +1 +2 +3 +4 +5 +6 +7 +8
#####################################
#  M  A  G |  G  T  R  A  G  T  T  T
#
# 3' splice site:
# -7 -6 -5 -4 -3 -2- 1 | +1
############################
#  T  T  T  T  C  A  G |  R

from __future__ import division, print_function
import sys
from collections import defaultdict
from Bio import SeqIO

def parse_gff3(path):
    intron_dict = defaultdict(list)

    with open(path, 'r') as f:
        for line in f:
            if line[0] != '#':
                line = line.split('\t')
                chrom = line[0]
                kind  = line[2]
                strnd = line[6]
                pos   = int(line[3]), int(line[4]), strnd
                if kind == 'intron' and strnd in ('+', '-'):
                    intron_dict[chrom].append(pos)

    return intron_dict


if __name__ == '__main__':
    l_diff_dict = defaultdict(set)
    r_diff_dict = defaultdict(set)
    b_diff_dict = defaultdict(set)
    total       = 0
    genome_file = sys.argv[1]
    intron_dict = parse_gff3(sys.argv[2])
    l_consensus = 'AAGGTAAGTTT'
    r_consensus = 'TTTTCAGA'

    # check each chromosome for the specified introns
    for record in SeqIO.parse(genome_file, 'fasta'):
        chrom, nuc = record.id, record.seq.upper()
        if chrom in intron_dict:
            for intron in intron_dict[chrom]:
                total += 1
                left, right, strand = intron
                l_diff, r_diff = 0, 0
                if strand == '+':
                    l_pos = left-4, left+7
                    r_pos = right-7, right+1
                    l_seq = nuc[ l_pos[0] : l_pos[1] ]
                    r_seq = nuc[ r_pos[0] : r_pos[1] ]
                elif strand == '-':
                    l_pos = right-8, right+3
                    r_pos = left-2, left+6
                    l_seq = nuc[ l_pos[0] : l_pos[1] ].reverse_complement()
                    r_seq = nuc[ r_pos[0] : r_pos[1] ].reverse_complement()
                # compute the edit distance between these sequences and the
                # consensus sequences
                for n, base in enumerate(l_seq): # 5' splice site
                    ref_base = l_consensus[n]
                    if base != ref_base:
                        l_diff += 1
                for n, base in enumerate(r_seq): # 3' splice site
                    ref_base = r_consensus[n]
                    if base != ref_base:
                        r_diff += 1
                # add to the results
                l_diff_dict[l_diff].add( (chrom, left, right, strand) )
                r_diff_dict[r_diff].add( (chrom, left, right, strand) )
                b_diff_dict[ l_diff+r_diff ].add( (chrom, left, right, strand) )

    # summarize the results
    # print('{:,}\tintrons total'.format(total))
    # print('#'*20, "5' Consensus Sites", '#'*20)
    # print('Diff.bp\tIntrons')
    # for n in range(max(l_diff_dict.keys())+1):
    #     print("{}\t{:,}".format(n, len(l_diff_dict[n])))
    #
    # print('#'*20, "3' Consensus Sites", '#'*20)
    # print('Diff.bp\tIntrons')
    # for n in range(max(r_diff_dict.keys())+1):
    #     print("{}\t{:,}".format(n, len(r_diff_dict[n])))
    #
    # print('#'*20, "Both Consensus Sites", '#'*18)
    # print('Diff.bp\tIntrons')
    # for n in range(max(b_diff_dict.keys())+1):
    #     print("{}\t{:,}".format(n, len(b_diff_dict[n])))

    print('##', 3, '##')
    for i in l_diff_dict[3]:
        print('{}:{}-{} ({} strand)'.format(*i))
