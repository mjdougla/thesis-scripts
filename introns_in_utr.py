#!/usr/local/bin/python3

import sys
from collections import defaultdict

utr_dict = defaultdict(list)
intron_dict = defaultdict(list)
found = set()

with open(sys.argv[1], 'r') as f:
    for n, line in enumerate(f):
        if line.startswith('#'):
            found.add(n)
        else:
            i = line.split('\t')
            seqid, left, right, strand = i[0], int(i[3]), int(i[4]), i[6]
            if i[2] == 'intron':
                intron_dict[(seqid, strand)].append((left, right, n))
            elif 'UTR' in i[2]:
                utr_dict[(seqid, strand)].append((left, right))

for region, intron in intron_dict.items():
    for i in intron:
        for u in utr_dict[region]:
            if u[0] <= i[0] and i[1] <= u[1]:
                found.add(i[2])

found = sorted(found)

with open(sys.argv[1], 'r') as f:
    lines = [i.strip() for i in f.readlines()]
    for n in found:
        print(lines[n])
