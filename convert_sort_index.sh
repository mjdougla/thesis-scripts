#!/bin/bash
# written for samtools version 1.6
set -e

BASE_NAME="${1%.*}"   # NOTE: keeps the path intact
FILE_TYPE="${1##*.}"
EXTRA_THREADS=${2:-0}

echo "File=$BASE_NAME (.$FILE_TYPE format)"

if [ "${FILE_TYPE^^}" == "SAM" ]; then
  echo "samtools view -@ $EXTRA_THREADS -b -o $BASE_NAME.bam $1"
  samtools view -@ $EXTRA_THREADS -b -o $BASE_NAME.bam $1
fi

echo "samtools sort -@ $EXTRA_THREADS -o $BASE_NAME.sorted.bam $BASE_NAME.bam"
samtools sort -@ $EXTRA_THREADS -o $BASE_NAME.sorted.bam $BASE_NAME.bam

echo "samtools index -@ $EXTRA_THREADS $BASE_NAME.sorted.bam"
samtools index -@ $EXTRA_THREADS $BASE_NAME.sorted.bam

if [ "${FILE_TYPE^^}" == "SAM" ]; then
  echo "rm $BASE_NAME.bam"
  rm $BASE_NAME.bam
fi

echo "done"
