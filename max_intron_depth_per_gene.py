#!/home2/mattdoug/python3/bin/python3
# Purpose: Return the maximum number of overlapping introns per gene. These
#          correspond to the minimum number of alternative splicing events
#          occuring for that gene.

import re, sys
from collections import defaultdict
from itertools import chain
import numpy as np

attr_name = re.compile('(?<=ID=)[^;]+(?=(;|))')
flatten = chain.from_iterable

def parse_genes(path):
    gmap = defaultdict(dict)
    name_set = set()

    with open(path, 'r') as f:
        for n, line in enumerate(f):
            if line[0] != '#':
                i = line.split('\t')
                if i[2] == 'gene':
                    region = i[0], i[6]
                    x, y = int(i[3]), int(i[4])
                    found = re.search(attr_name, i[8])
                    name = (found.group(0) if found else n)
                    gmap[region][(x, y)] = name
                    name_set.add(name)

    print('  {:,} genes found.'.format(len(name_set)))
    return gmap


def parse_introns(path):
    intron_map = defaultdict(list)

    with open(path, 'r') as f:
        for line in f:
            if line[0] != '#':
                i = line.split('\t')
                if i[2] == 'intron':
                    region = i[0], i[6]
                    x, y = int(i[3]), int(i[4])
                    score = (0.0 if i[5] == '.' else int(i[5]))
                    intron_map[region].append((x, y, score))

    return intron_map


def assign_to_genes(gmap, intron_map):
    assign = defaultdict(set)

    for region in intron_map:
        last = 0
        genes = sorted(gmap[region])
        introns = sorted(intron_map[region])
        for i in introns:
            x, y, _ = i
            name = None
            for n, j in enumerate(genes[last:]):
                if j[0] <= x <= j[1]:
                    name = gmap[region][j]
                    last += n
                    break
                elif j[0] <= y <= j[1]:
                    name = gmap[region][j]
                    break
            if name:
                assign[name].add(i)

    return assign


def max_intron_depth(introns):
    depth = 0
    max_depth = -1
    max_scores = []
    temp = []

    ranges = sorted(flatten(((l, 1, s), (r+1, -1, s)) for l, r, s in introns))

    for value, label, score in ranges:
        depth += label
        if label > 0:
            temp.append(score)
        else:
            temp.remove(score)
        if depth == max_depth and sum(temp) > sum(max_scores):
            max_scores = list(temp)
        if depth > max_depth:
            max_depth = depth
            max_scores = list(temp)

    return max_depth, max_scores


if __name__ == '__main__':
    gene_path = sys.argv[1]
    intron_conf = sys.argv[2]
    d_depth = defaultdict(int)
    d_ratios = defaultdict(list)
    skip = 0
    itotal = 0
    gtotal = 0
    out = open('max_intron_depth_raw.tsv', 'w')
    print(*list(range(1, 21)), sep='\t', file=out)

    print('Reading config file...')
    with open(intron_conf, 'r') as f:
        paths = [i.strip() for i in f.readlines()]
        print('  {:,} files to read.'.format(len(paths)))

    print('Identifying gene boundaries...')
    gmap = parse_genes(gene_path)

    for n, p in enumerate(paths, 1):
        print('\rParsing introns files... ({:,}/{:,})'.format(n, len(paths)), end='')
        assign = defaultdict(set)
        intron_map = parse_introns(p)
        assign = assign_to_genes(gmap, intron_map)
        for gene, introns in assign.items():
            max_depth, scores = max_intron_depth(introns)
            d_depth[max_depth] += 1
            scores.sort(reverse=True)
            ratios = [i/scores[0] for i in scores]
            if len(ratios) > 1:
                ratios_out = ['{:.3f}'.format(i) for i in ratios[:21]] + ['#N/A' for i in range(20-len(ratios))]
                print(*ratios_out, sep='\t', file=out)
                for d, x in enumerate(ratios, 1):
                    d_ratios[d].append(x)
            else:
                skip += 1
    print('')

    print('Outputting results...')
    with open('max_depth_counts.tsv', 'w') as f:
        print('Max.Depth\tCounts', file=f)
        for d in sorted(d_depth):
            gtotal += d_depth[d]
            print(d, d_depth[d], sep='\t', file=f)
        print('Total', gtotal, sep='\t', file=f)

    with open('max_depth_distribution.tsv', 'w') as f:
        print('Total', itotal, sep='\t', file=f)
        print('Skipped', skip, sep='\t', file=f)
        print('Depth\tCount\tMin\tQ1\tMedian\tQ3\tMax', file=f)
        for d in sorted(d_ratios):
            s = d_ratios[d]
            itotal += len(s)
            print(d,
                 len(s),
                 '{:.3f}'.format(min(s)),
                 '{:.3f}'.format(np.percentile(s, 25)),
                 '{:.3f}'.format(np.percentile(s, 50)),
                 '{:.3f}'.format(np.percentile(s, 75)),
                 '{:.3f}'.format(max(s)),
                 sep='\t',
                 file=f)

    out.close()
