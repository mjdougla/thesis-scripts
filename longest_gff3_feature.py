#!/usr/local/bin/python3

import sys
from collections import defaultdict

top_n = 10
count = 0

len_dict = defaultdict(list)

for path in sys.argv[1:]:
    with open(path, 'r') as f:
        for line in f:
            if line[0] != "#":
                i = line.split('\t')
                feat = i[0], int(i[3]), int(i[4]), i[6]
                length = feat[2] - feat[1] + 1
                len_dict[length].append(feat)

len_sorted = sorted(len_dict.keys(), reverse=True)

print('# Top {:,} longest features:'.format(top_n))
print('Length (bp)\tPosition')
for l in len_sorted:
    feat_list = len_dict[l]
    for f in feat_list:
        print('{}\t{}:{}-{} ({} strand)'.format(l, *f))
        if count > top_n:
            sys.exit(0)
        else:
            count += 1
