#!/usr/local/bin/python3
#Purpose: Get the frequency of each alignment score in one or more SAM files so
#you can make a histogram.

import statistics, sys
from collections import defaultdict

def alignment_scores(f, score_dict):
    for line in f:
        if line[0] != '@':
            for i in line.split('\t')[::-1]:
                if 'AS:i:' in i:
                    score = int(i.split(':')[-1])
                    score_dict[score] += 1
                    break

    return score_dict


def output(score_dict):
    print('Score\tFreq\tFreq (%)')
    total = sum([i for i in score_dict.values()])
    sum_p = 0
    for i in range(min(score_dict), max(score_dict)+1):
        try:
            freq = score_dict[i]
        except KeyError:
            freq = 0
        freq_p = freq / total * 100
        sum_p += freq_p
        print(i, freq, '%.2f' % freq_p + '%', sep='\t')
    print('Total', total, '%.2f' % sum_p + '%', sep='\t')


def main(SAM_files):
    score_dict = defaultdict(int)

    # get the frequency of alignment scores in each SAM file
    for i in SAM_files:
        with open(i, 'r') as f:
            score_dict = alignment_scores(f, score_dict)

    # print results
    output(score_dict)


if __name__ == '__main__':
    main(sys.argv[1:])
