#!/usr/local/bin/python3

import re, sys
from collections import defaultdict

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


def fr(region):
    """Return a tuple of (chromosome, start position, end position, strand) in
    a readable format.
    """
    return '{}:{:,}-{:,} ({} strand)'.format(*region)


def parse_genes(path):
    results = defaultdict(list)

    with open(path, 'r') as f:
        for line in f:
            if line[0] != '#':
                i = line.split('\t')
                kind = i[2]
                chrom, strand = i[0], i[6]
                pos = int(i[3]), int(i[4])
                attr = i[8]
                if kind == 'gene':
                    results[(chrom, strand)].append(pos)

    return results


def parse_intron(path):
    results = defaultdict(list)

    with open(path, 'r') as f:
        for line in f:
            if line[0] != '#':
                i = line.split('\t')
                kind = i[2]
                chrom, strand = i[0], i[6]
                pos = int(i[3]), int(i[4])
                if kind == 'intron':
                    results[(chrom, strand)].append(pos)

    return results


def compare(genes, introns):
    gene_dict = defaultdict(set)
    found = 0
    count = 0
    total = sum([len(i) for i in introns.values()])

    for region, ranges in introns.items():
        chrom, strand = region
        for i, j in ranges:
            for m, n in genes[region]:
                if m <= i <= n or m <= j <= n:
                    gene = chrom, m, n, strand
                    intron = chrom, i, j, strand
                    found += 1
                    gene_dict[gene].add(intron)
                    break
            count +=1
            eprint('\r{:,}/{:,} introns checked ({:,} found)...'.format(count, total, found), end='')
    eprint('')

    return gene_dict


if __name__ == '__main__':
    counts = []

    gene_path = sys.argv[1]
    intron_path = sys.argv[2]

    eprint('Parsing gene models...')
    genes = parse_genes(gene_path)

    eprint('Parsing introns...')
    introns = parse_intron(intron_path)

    eprint('Comparing gene and intron positions...')
    gene_dict = compare(genes, introns)

    for gene, introns in gene_dict.items():
        c = len(introns)
        counts.append(c)
        print('{}: = {} introns:'.format( fr(gene), c ))
        for i in introns:
            print(' ', fr(i))

    avg = sum(counts) / float(len(counts))
    avg = '{0:.1f}'.format(avg)
    eprint('Average no. introns per gene =', avg)
