#!/usr/local/bin/python
# Last updated: 6/4/2017

from __future__ import print_function
import os, sys
from collections import defaultdict
from math import floor, log

########################
# Define colours (RGB) #
########################
red        = '255,0,0'
yellow     = '255,255,120'
orange     = '255,128,0'
green      = '0,204,0'
blue       = '0,102,204'
light_blue = '0,128,204'
grey       = '96,96,96'
white      = '255,255,255'

###########################
# Multi-purpose functions #
###########################
def bin_GFF3(f):
    feature_bins = defaultdict(list)
    support_bins = defaultdict(list)
    strand_bins = defaultdict(list)

    # bin features
    for line in f:
        if line[0] != '#':
            col = line.split('\t')
            chrom, start, end = col[0], int(col[3]), int(col[4])
            # read the support count, if available
            try:
                support = int(col[5])
            except ValueError:
                support = 0
            # assign a value to the strand
            if col[6] == '+':
                strand = 1
            elif col[6] == '-':
                strand = -1
            else:
                col[6] = 0
            # bin based on the middle position of the feature
            index = int(floor((start+end) / 2 / bin_size))
            try:
                feature_bins[chrom][index] += 1
                support_bins[chrom][index] += support
                strand_bins[chrom][index] += strand
            except IndexError:
                while len(feature_bins[chrom]) <= index:
                    feature_bins[chrom].append(0)
                    support_bins[chrom].append(0)
                    strand_bins[chrom].append(0)
                feature_bins[chrom][index] += 1
                support_bins[chrom][index] += support
                strand_bins[chrom][index] += strand

    return feature_bins, support_bins, strand_bins


def bin_SAM(f):
    # bins = [unique, multi]
    bins = defaultdict(list)

    # bin reads
    for line in f:
        if line[0] != '@':
            col = line.split('\t')
            chrom, start = col[2], int(col[3])
            index = int(floor(start/bin_size))
            try:
                bins[chrom][index] += 1
            except IndexError:
                while len(bins[chrom]) <= index:
                    bins[chrom].append(0)
                bins[chrom][index] += 1

    return bins


########################################
# Functions to create the actual lines #
########################################
def generate_histogram_track(bin_dict, threshold=999999, colour=light_blue, threshold_colour=red):
    # color-code col
    for chrom, col in bin_dict.items():
        for n, i in enumerate(col):
            size = col[n]
            if size > threshold:
                colour = threshold_colour
                #print(' Tall peak in bin {}:{}-{}, size={}'.format(chrom, n*bin_size, (n+1)*bin_size, size))
            # set max height
            # if size > 1000:
            #     print(' Cropping peak in bin {}:{}-{}, size={} to size={}'.format(chrom, n*bin_size, (n+1)*bin_size, size, 1000))
            #     size = 1000

            # yield next line
            line = ' '.join(map(str, (chrom, n*bin_size, (n+1)*bin_size, size, 'fill_color='+colour)))
            yield line


def generate_heatmap_track(bin_dict, R_max=255, G_max=255, B_max=0):
    # R_max=255, G_max=128, B_max=204
    # placeholder to set scale
    yield 'I 0 1 1 fill_color=255,255,255'

    # all_cols = []
    # for i in bin_dict.values():
    #     all_cols += i
    # arr = np.array(all_cols)
    # mean = int(np.mean(arr))
    # std = int(np.std(arr))
    # max_val = np.max(arr)
    # print(' Mean = {}\n STD = {}\n Max = {}'.format(mean, std, max_val))
    # thresh1 = mean - std
    # thresh2 = mean + std

    # get the average value
    max_val = 0
    total = 0
    bins = 0
    for chrom, col in bin_dict.items():
        total += sum(col)
        bins += len(col)
        if max(col) > max_val:
            max_val = max(col)
    print(' Total = {}\n No. bins = {}'.format(total, bins))

    average = total // bins
    thresh1 = average // 2
    thresh2 = (max_val + average) // 2
    print(' Threshold #1 = {}\n Threshold #2 = {}\n Maximum = {}'.format(thresh1, thresh2, max_val))

    # color-code col
    for chrom, col in bin_dict.items():
        for n, i in enumerate(col):
            # percent = i / average
            # R = int(percent * R_max)
            # G = int((1 - percent) * G_max)
            # B = int((1 - percent) * B_max)
            # colour = ','.join(map(str, (R,G,B)))
            if i < thresh1:
                colour = light_blue
            elif i >= thresh1 and i < thresh2:
                colour = yellow
            else:
                colour = orange

            # yield next line
            line = ' '.join(map(str, (chrom, n*bin_size, (n+1)*bin_size, 100, 'fill_color='+colour)))
            yield line


#################
# Main run loop #
#################
if __name__ == '__main__':
    bin_size = 100000

    transcript_file = '/home2/mattdoug/PROJECTS/project2/c_elegans.PRJNA13758.WS250.transcript.gff3'
    intron_file = sys.argv[1]
    wormbase_intron_file = sys.argv[2]

    # make the necessary folder structure, if it doesn't already exist
    if not os.path.exists('data'):
        os.makedirs('data')
    if not os.path.exists('circos_plots/data'):
        os.makedirs('circos_plots/data')

    # start
    print('preparing results data for Circos...')
    with open(intron_file, 'r') as f:
        feature_bins, support_bins, strand_bins = bin_GFF3(f)

    with open('circos_plots/data/intron_density.txt', 'w') as f:
        for line in generate_histogram_track(feature_bins):
            print(line, file=f)
    with open('circos_plots/data/support_density.txt', 'w') as f:
        for line in generate_heatmap_track(support_bins):
            print(line, file=f)
    with open('circos_plots/data/strand_density.txt', 'w') as f:
        for line in generate_histogram_track(strand_bins):
            print(line, file=f)

    print('preparing WormBase data for Circos...')
    with open(wormbase_intron_file, 'r') as f:
        feature_bins, support_bins, strand_bins = bin_GFF3(f)

    with open('circos_plots/data/wormbase_intron_density.txt', 'w') as f:
        for line in generate_histogram_track(feature_bins, colour=grey):
            print(line, file=f)
    with open('circos_plots/data/wormbase_strand_density.txt', 'w') as f:
        for line in generate_histogram_track(strand_bins):
            print(line, file=f)
