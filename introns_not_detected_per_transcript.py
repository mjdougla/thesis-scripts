#!/usr/local/bin/python3

import sys
from collections import defaultdict

def get_parent(attr):
    attr = attr.strip().split(';')
    for i in attr:
        i = i.split('=')
        if i[0] == 'Parent':
            parent = i[1].split(':')[-1]
            return parent


def parse_transcript(path):
    transcript = defaultdict(list)

    with open(path, 'r') as f:
        for line in f:
            if line[0] != '#':
                i = line.split('\t')
                kind = i[2]
                pos = i[0], int(i[3]), int(i[4]), i[6]
                if kind == 'intron':
                    parent = get_parent(i[8])
                    transcript[parent].append(pos)

    return transcript


def parse_intron(path):
    intron = set()

    with open(path, 'r') as f:
        for line in f:
            if line[0] != '#':
                i = line.split('\t')
                pos = i[0], int(i[3]), int(i[4]), i[6]
                intron.add(pos)

    return intron


def as_gff3(seqid, left, right, strand):
    line = seqid, '.', 'intron', left, right, '.', strand, '.', '.'
    return '\t'.join(map(str, line))


if __name__ == '__main__':
    transcript_path = sys.argv[1]
    rna_intron_path = sys.argv[2]
    none = 0
    partial = 0
    complete = 0

    transcripts = parse_transcript(transcript_path)
    rna_intron = parse_intron(rna_intron_path)

    for introns in transcripts.values():
        introns = set(introns)
        missed = introns - rna_intron
        if missed == introns:
            none += 1
        elif len(missed) > 0:
            partial += 1
        elif len(missed) == 0:
            complete += 1
        else:
            print('ERROR')
            sys.exit(1)

    print('{:,} intron-containing transcripts...'.format(complete + partial + none))
    print('{:,} had all introns identified'.format(complete))
    print('{:,} had some introns identified'.format(partial))
    print('{:,} had no introns identified'.format(none))
