#!/home2/mattdoug/bin/python3

import sys

def eprint(*args, **kwargs):
    """Print to stderr."""
    print(*args, file=sys.stderr, **kwargs)


f = open(sys.argv[1], 'r')
line = f.readline().strip()
d = [None, None, None, None]
n = 0
x = 1
c = 0

# loop through the FASTQ file
while True:
    if not line:
        break
    if n == 0:
        if line[0] == '@':
            d[n] = line
            n += 1
        else:
            d = [None, None, None, None]
            eprint('[Malformed entry, line {}] {}'.format(x, line))
        line = f.readline().strip()
        x += 1
    elif n == 1:
        if line[0].isalpha():
            d[n] = line
            n += 1
            line = f.readline().strip()
            x += 1
        else:
            d = [None, None, None, None]
            eprint('[Malformed entry, line {}] {}'.format(x, line))
            n = 0
    elif n == 2:
        if line[0] == '+':
            d[n] = line
            n += 1
            line = f.readline().strip()
            x += 1
        else:
            d = [None, None, None, None]
            eprint('[Malformed entry, line {}] {}'.format(x, line))
            n = 0
    elif n == 3:
        d[n] = line
        if None not in d:
            # check if Seq and Qual are the same length
            if len(d[1]) == len(d[3]):
                c += 1
                for i in d:
                    print(i)
            else:
                eprint('[Length Error, line {}] Entry "{}", "{}" and "{}"'.format(x, d[0], d[1], d[3]))
        d = [None, None, None, None]
        n = 0
        line = f.readline().strip()
        x += 1

# check if the last entry is truncated
if d != [None, None, None, None]:
    for i in d:
        if i is not None:
            eprint('[Truncated entry, line {}] {}'.format(x-1, i))

eprint('Read {:,} lines.'.format(x-1))
eprint('Printed {:,} entries ({:,} lines).'.format(c, c*4))

# clean up
f.close()
