#!/usr/local/bin/python3

import re, sys
from collections import defaultdict

attr_name     = re.compile('(?<=Name=)[^;]+(?=(;|))')
attr_seq_name = re.compile('(?<=sequence_name=)[^;]+(?=(;|))')

def parse_attr(attr, label):
    names = []

    for i in attr.split(';'):
        if i.startswith(label):
            for j in i.split(','):
                name = j.split(':')[1]
                names.append(name)

    return names


def parse_parent(name):
    parent = '.'.join(name.split('.')[:2]) #Y71H2AM.19a.1 <- ".1" indicates different UTRs
    if parent[-1].isalpha():
        parent = parent[:-1]

    return parent


if __name__ == '__main__':
    name_dict  = {}
    trans_dict = {}

    gff_file = sys.argv[1]

    with open(gff_file, 'r') as f:
        # get the gene names
        for line in f:
            line = line.strip()
            if line[0] != '#':
                i = line.split('\t')
                kind, attr = i[2], i[8]
                if kind == 'gene':
                    name = attr_name.search(attr).group(0)
                    seq_name = attr_seq_name.search(attr).group(0)
                    name_dict[name] = seq_name
                    trans_dict[seq_name] = set()

        # loop through the GFF file and yield any entries that originate from a
        # protein coding gene
        for line in f:
            line = line.strip()
            if line[0] != '#':
                i = line.split('\t')
                kind, attr = i[2], i[8]
                if kind != 'gene':
                    for name in parse_attr(attr, 'Parent='):
                        parent = parse_parent(name)
                        try:
                            trans_dict[parent].add(name)
                        except KeyError:
                            parent = name_dict[parent]
                            trans_dict[parent].add(name)

    for gene, transcripts in trans_dict.items():
        print(gene, len(transcripts), ', '.join(transcripts), sep='\t')
