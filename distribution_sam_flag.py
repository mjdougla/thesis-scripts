#!/usr/local/bin/python3

import sys, operator
from operator import itemgetter
from collections import defaultdict

def percentage(num):
    percent = num / total * 100
    return '%.1f' % percent

def parse_SAM(SAM_file):
    total = 0
    flag_dict = defaultdict(int)
    flag_results = [0 for i in range(12)]
    mapq_dict = defaultdict(int)

    for line in SAM_file:
        if line[0] != '@':
            values = line.split('\t')
            flag, mapq = int(values[1]), int(values[4])
            flag_dict[flag] += 1
            mapq_dict[mapq] += 1
            total += 1
            for pos, val in enumerate(reversed('{0:012b}'.format(flag))):
                if val == '1':
                    flag_results[pos] += 1

    return flag_dict, flag_results, mapq_dict, total


if __name__ == '__main__':
    with open(sys.argv[1], 'r') as f:
        flag_dict, flag_results, mapq_dict, total = parse_SAM(f)

    print('{:,}\talignments have a mate ({}%)'.format(flag_results[0], percentage(flag_results[0])))
    print('{:,}\talignments are mapped as a pair ({}%)'.format(flag_results[1], percentage(flag_results[1])))
    print('{:,}\talignments are unaligned ({}%)'.format(flag_results[2], percentage(flag_results[2])))
    print('{:,}\talignments have an unaligned mate ({}%)'.format(flag_results[3], percentage(flag_results[3])))
    print('{:,}\talignments have a mate aligned to the reverse strand ({}%)'.format(flag_results[5], percentage(flag_results[5])))
    print('{:,}\talignments are aligned to the reverse strand ({}%)'.format(flag_results[4], percentage(flag_results[4])))
    print('{:,}\talignments are first in the pair ({}%)'.format(flag_results[6], percentage(flag_results[6])))
    print('{:,}\talignments are second in the pair ({}%)'.format(flag_results[7], percentage(flag_results[7])))
    print('{:,}\talignments are secondary ({}%)'.format(flag_results[8], percentage(flag_results[8])))

    sorted_flag_dict = reversed(sorted(flag_dict.items(), key=itemgetter(1)))
    print('\nFLAG\tCount')
    for k, v in sorted_flag_dict:
        print('{}\t{} ({}%)'.format(k, v, v*100//total))

    sorted_mapq_dict = reversed(sorted(mapq_dict.items(), key=itemgetter(1)))
    print('\nMAPQ\tCount')
    for k, v in sorted_mapq_dict:
        print('{}\t{} ({}%)'.format(k, v, v*100//total))
