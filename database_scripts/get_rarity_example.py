#!/~/bin/python3
# Purpose: Return features with usage ratio between X and Y, with a score of at
#          least Z.

import sys
from collections import defaultdict
import gff_utils as gff

p1 = '/home2/mattdoug/Thesis/intron_database/v2/intron_database.min5.ratios.gff3'
p2 = '/home2/mattdoug/Thesis/intron_database/v2/intron_database.min5.gff3'
minr = 0.01
maxr = 0.7
mins = 50
temp = {}

with open(p1, 'r') as f:
    for feat in gff.parse(f):
        s1 = float(feat.score)
        s2 = []
        l2 = []
        for i in feat.attr['ScorePerLibrary']:
            x, y = i.split(':')
            l2.append(x)
            s2.append(float(y))
        if s1 < minr and maxr < max(s2):
            temp[feat.pos] = [s1, max(s2), l2]

with open(p2, 'r') as f:
    for feat in gff.parse(f):
        if feat.pos in temp:
            s2 = defaultdict(list)
            for i in feat.attr['ScorePerLibrary']:
                x, y = i.split(':')
                y = float(y)
                s2[y].append(x)
            if max(s2) > mins:
                a, b, c = temp[feat.pos]
                print('{}:{}-{}\t{}'.format(*feat.pos), a, b, ','.join(c), sep='\t')
