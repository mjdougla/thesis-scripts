#!/~/bin/python3
# Purpose: Bin features (introns/exons) by score.

import sys
import gff_utils as gff

# User defined variables
path = sys.argv[1]
bins = [i for i in range(0, 12)]

# Script variables
total = 0
bins_d = {i:0 for i in bins}

with open(path, 'r') as f:
    for feat in gff.parse(f):
        total += 1
        score = int(feat.score)
        for n, i in enumerate(bins):
            if score < i:
                bins_d[bins[n-1]] += 1
                break
            if n == len(bins)-1:
                bins_d[bins[n]] += 1

for n, i in enumerate(bins):
    if n < len(bins)-1:
        print('{}-{}\t{}'.format(i, bins[n+1], bins_d[i]))
    else:
        print('{}+\t{}'.format(i, bins_d[i]))
print('Total\t{}'.format(str(total)))
