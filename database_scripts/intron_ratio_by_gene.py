#!/usr/local/bin/python3
# Last updated: 12/6/2018
# Author: Matt Douglas
#
# Purpose: Calulate the relative usage ratios for introns within a gene (1 base
# adjacent to the gene counts as "within the gene").
#
# Note: Based on the procedure in the paper by Tourasse et al. (2017), which
# did not include introns that do not overlap annoated genes. For introns that
# do not overlap genes, we assign a ratio of 0 just to aid visualization.
# 
# Note: Some genes overlap (e.g. II:4,991,332..4,993,331)

import sys
import gff_utils as gff
from collections import defaultdict

def prog(count, total):
    """Return a number formatted as a percentage, to 2 decimal places."""
    try:
        progress = count * 100 / total
        progress = '{0:.1f}%'.format(progress)
        return progress
    except ZeroDivisionError:
        return '0%'


def sort_features(feats):
    """Sort GFF3 formatted entries by chromosome, then start position, then end
    position. NOTE: C. elegans uses roman numerals for chromosomes names.
    """
    numerals = {'I':1, 'II':2, 'III':3, 'IV':4, 'V':5, 'X':10, 'MtDNA':11}
    try:
        return sorted(feats, key=lambda x: (numerals[x[0]], x[1], x[2]))
    except KeyError:
        return sorted(feats, key=lambda x: (x[0], x[1], x[2]))


def print_as_gff3(ratio_dict):
    """Take a list of Intron() objects and print out in GFF3 format."""
    print('##gff-version 3')
    for n, intron in enumerate(sort_features(ratio_dict)):
        seqid, start, end, strand = intron
        score = global_score[intron]
        attr = 'ID=Intron{};ScorePerLibrary='.format(n)
        for library, ratio in ratio_dict[intron].items():
            attr += library + ':' + '{0:.3f}'.format(ratio) + ','
        line = seqid, '.', 'intron', start, end, score, strand, '.', attr[:-1]
        print(*line, sep='\t')


if __name__ == '__main__':
    gene_dict = {}                 #{ gene:[introns] }
    intron_dict = defaultdict(list)
    lib_dict = defaultdict(list)   #{ library:[introns] }
    global_score = {}
    max_score = defaultdict(int)
    score_dict = defaultdict(dict) #{ intron: { library:score } }
    ratio_dict = defaultdict(dict) #{ intron: { library:ratio } }
    gene_gff = sys.argv[1]
    intron_gff = sys.argv[2]

    # build a dictionary of gene boundaries on each strand
    print('Parsing gene GFF3...', end='', file=sys.stderr)
    with open(gene_gff, 'r') as f:
        for line in f:
            if line[0] != '#':
                i   = line.split('\t')
                if i[2] == 'gene':
                    gene = i[0], int(i[3])-1, int(i[4])+1, i[6] # add 1 to ends to count introns that might extend gene
                    gene_dict[gene] = []

    if len(gene_dict) < 1:
        print('No genes found. Exiting.', file=sys.stderr)
        sys.exit(0)
    print('\rParsing gene GFF3... {:,} found!'.format(len(gene_dict)), file=sys.stderr)

    # identify all the introns
    print('Parsing intron database...', end='', file=sys.stderr)
    with open(intron_gff, 'r') as f:
        for i in gff.parse(f):
            region = i.seqid, i.strand
            intron = i.seqid, i.start, i.end, i.strand
            intron_dict[region].append(intron)
            global_score[intron] = i.score
            for j in i.attr['ScorePerLibrary']:
                library, score = j.split(':')
                score = int(score)
                lib_dict[library].append(intron)
                score_dict[intron][library] = score
                ratio_dict[intron][library] = 0

    if len(intron_dict) < 1:
        print('No introns found. Exiting.', file=sys.stderr)
        sys.exit(0)
    print('\rParsing intron database... {:,} found!'.format(len(score_dict)), file=sys.stderr)

    # assign introns to genes where one or more splice sites are located within
    # the gene boudaries
    window = 500000
    last = 0
    assign = 0
    for c, gene in enumerate(sort_features(gene_dict)):
        print('\rAssigning introns to genes... {}'.format(prog(c, len(gene_dict))), end='', file=sys.stderr)
        introns = intron_dict[(gene[0], gene[3])]
        a, b = gene[1:3]
        for n, j in enumerate(introns):
            c, d = j[1:3]
            if a <= c <= b: # intron start lies within gene
                gene_dict[gene].append(j)
                assign += 1
            elif a <= d <= b: # intron end lies within gene
                gene_dict[gene].append(j)
                assign += 1
            elif c < a - window: # for performance only
                last = n
            elif b < c: # stop if we go past the end of the gene
                break
    print('\rAssigning introns to genes... {:,} assigned.'.format(assign), file=sys.stderr)

    # for each gene, compute junction usage ratios
    c = 0
    for gene, g_introns in gene_dict.items():
        c += 1
        print('\rCalculating usage ratios... {}'.format(prog(c, len(gene_dict))), end='', file=sys.stderr)
        for library in lib_dict:
            introns = [i for i in g_introns if i in lib_dict[library]]
            if len(introns) < 1:
                continue
            max_score = max([score_dict[i][library] for i in introns])
            for i in introns:
                ratio_dict[i][library] = score_dict[i][library] / max_score
    print('\rCalculating usage ratios... Done! ', file=sys.stderr)

    # print the results to file
    print_as_gff3(ratio_dict)
