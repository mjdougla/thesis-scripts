#!/usr/local/bin/python3
# Purpose: Take a tab-seperated file generated from 'embryo_ratio_report.py' and
#          average the results for each time points, for each intron.

import sys
from collections import defaultdict

# Each tuple contains integers corresponding to the time-points (in minutes)
# that the libraries were sampled at.
bins = [(0, 30, 60, 90, 120, 150, 180, 210, 240, 270, 300, 330), # proliferation
        (360, 390, 420, 450, 480, 510, 540, 570),                # metamorphosis and elongation
        (600, 630, 660, 690, 720)]                               # quickening

path = sys.argv[1]
maxr = 0.50
minr = 0.10
result_d = {}
ratio_d = {}
gene_d = {}
total = 0
discard = 0
out_intron = { 0:set(), 1:set(), 2:set(), 3:set(), 4:set() } # early, mid, late, multi, none
out_gene = { 0:set(), 1:set(), 2:set(), 3:set(), 4:set() }

splitline = lambda line: line.strip().split('\t')

with open(path, 'r') as f:
    header1 = splitline(f.readline()) # 1st header: Intron, Gene, Biotype, Global, 0min, 30min, etc...
    tpoint = {n:int(i) for n, i in enumerate(header1[4:])}
    next(f) # 2nd header: Intron, Gene, Biotype, Global, Library1, Library2, etc...
    for i in f:
        # parse the line
        total += 1
        i = splitline(i)
        intron = i[0]
        gene_d[intron] = i[1].split(',')
        btypes = i[2].split(',')
        g_score = (0.0 if i == 'NA' else float(i[3]))
        ratios = i[4:]
        # for now, we only care about protein coding genes
        if 'protein_coding' not in btypes:
            discard += 1
            continue
        # calculate the average ratio for each time point
        time_d = defaultdict(list)
        avg = {}
        for n, i in enumerate(ratios):
            t = tpoint[n]
            i = (0 if i == 'NA' else float(i))
            time_d[t].append(i)
        for t in sorted(time_d):
            i = time_d[t]
            avg[t] = sum(i)/len(i)
        # check if the pattern of ratios meets our criteria
        # intron must be found in >= 1/2 the libraries in that time point
        ratio_d[intron] = [0, 0, 0]
        result_d[intron] = [0, 0, 0]
        for n, i in enumerate(bins):
            for t in i:
                a = avg[t]
                ratio_d[intron][n] = max(a, ratio_d[intron][n])
                if a >= maxr and time_d[t].count(0) < len(time_d[t])/2:
                    result_d[intron][n] = 2
                elif a < minr:
                    result_d[intron][n] = max(result_d[intron][n], 0)
                else:
                    result_d[intron][n] = max(result_d[intron][n], 1)

for intron, vals in result_d.items():
    if vals.count(0) == 3: #rare in all stages
        out_intron[4].add(intron)
    elif vals.count(2) == 1 and vals.count(0) == 2: #strong specific expression
        if vals[0] == 2:
            out_intron[0].add(intron)
            for gene in gene_d[intron]:
                out_gene[0].add(gene)
        elif vals[1] == 2:
            out_intron[1].add(intron)
            for gene in gene_d[intron]:
                out_gene[1].add(gene)
        elif vals[2] == 2:
            out_intron[2].add(intron)
            for gene in gene_d[intron]:
                out_gene[2].add(gene)
    else:                     #less obvious expression pattern
        out_intron[3].add(intron)

print('{:,}\tintrons total'.format(total))
print('{:,}\tintrons matching "protein_coding"'.format(total-discard))
print('{:,}\tnon-rare in early'.format(len(out_intron[0])))
print('{:,}\tnon-rare in mid'.format(len(out_intron[1])))
print('{:,}\tnon-rare in late'.format(len(out_intron[2])))
print('{:,}\tnon-rare in multiple ranges'.format(len(out_intron[3])))
print('{:,}\trare in all ranges'.format(len(out_intron[4])))

with open('early_intron.txt', 'w') as f:
    for intron in out_intron[0]:
        print(intron, sep='\t', file=f)
with open('mid_intron.txt', 'w') as f:
    for intron in out_intron[1]:
        print(intron, sep='\t', file=f)
with open('late_intron.txt', 'w') as f:
    for intron in out_intron[2]:
        print(intron, sep='\t', file=f)

with open('early_gene.txt', 'w') as f:
    for gene in out_gene[0]:
        print(gene, sep='\t', file=f)
with open('mid_gene.txt', 'w') as f:
    for gene in out_gene[1]:
        print(gene, sep='\t', file=f)
with open('late_gene.txt', 'w') as f:
    for gene in out_gene[2]:
        print(gene, sep='\t', file=f)
