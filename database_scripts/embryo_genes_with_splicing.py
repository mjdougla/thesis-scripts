#!/usr/bin/local/python3
# Purpose: Check which genes show splicing at "early," "mid," or "late" embryo
#          development.

import sys
import gff_utils as gff
from collections import defaultdict

# User defined variables
bins = ((0, 330), (360, 600), (630, 720))
step = 30 # interval libraries were sampled at (i.e. 30min apart)
conf_path = sys.argv[1]
intron_path = sys.argv[2]
ref_path = '/home2/mattdoug/Thesis/reference/c_elegans.PRJNA13758.WS250.gene_models.gff3'

# Script variables
ref_d = {}
lib_set = set()
time_d = defaultdict(set)
result = defaultdict(dict)

# get the positions of each gene
with open(ref_path, 'r') as f:
    for line in gff.parse(f):
        if line.ftype == 'gene':
            gene = line.attr['ID'][0]
            alias = line.attr['Alias'][0]
            btype = line.attr['biotype'][0]
            ref_d[gene] = (alias, btype, line.pos)

# read the config file (RUN, LIBRARY, TIME-POINT)
with open(conf_path, 'r') as f:
    for line in f:
        run, library, time = line.strip().split('\t')
        time = int(time)
        lib_set.add(library)
        time_d[time].add(library)

# read the intron database
with open(intron_path, 'r') as f:
    for line in gff.parse(f):
        lib = [i.split(':') for i in line.attr['ScorePerLibrary'] if i.split(':')[0] in lib_set]
        for gene in line.attr['Parent']:
            for i in lib:
                try:
                    result[gene][i[0]] = max(result[gene][i[0]], int(i[1]))
                except KeyError:
                    result[gene][i[0]] = int(i[1])

# print the results
header1 = ['WB ID', 'Gene', 'Biotype', 'Pos']
header2 = header1[:] # copy header1
for i in sorted(time_d):
    libs = sorted(time_d[i])
    header1 += [i] * len(libs)
    header2 += libs
header1 += ['Early', 'Mid', 'Late']
header2 += ['Early', 'Mid', 'Late']
print(*header1, sep='\t')
print(*header2, sep='\t')

for gene in result:
    final = [False, False, False]
    alias, btype, pos = ref_d[gene]
    pos = '{}:{}-{}{}'.format(*pos)
    line = [gene.split(':')[-1], alias, btype, pos]
    for lib in header2[4:]:
        line.append((result[gene][lib] if lib in result[gene] else 0))
    for n, b in enumerate(bins):
        val = 0
        for i in range(b[0], b[1]+1, step):
            for lib in time_d[i]:
                try:
                    val = max(val, result[gene][lib])
                except KeyError:
                    pass
        if val == 0:
            final[n] = 0
        elif val < 5:
            final[n] = 1
        else:
            final[n] = 2
    line += final
    print(*line, sep='\t')
