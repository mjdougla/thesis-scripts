from collections import defaultdict

class Feature(object):
    def __init__(self, line, ext):
        self.line = line.strip().split('\t')
        self.is_gtf = (True if ext == '.gtf' else False)
        self.seqid = self.line[0]
        self.source = self.line[1]
        self.ftype = self.line[2]
        self.start = int(self.line[3])
        self.end = int(self.line[4])
        self.score = self.line[5]
        self.strand = self.line[6]
        self.phase = self.line[7]
        self.attr = self.parse_attr(self.line[8])
        self.extra = self.line[9:]
        self.pos = self.seqid, self.start, self.end, self.strand

    def parse_attr(self, attr):
        attr_dict = defaultdict(list)
        sep = (' ' if self.is_gtf else '=')
        attr = attr.split(';')
        for i in attr:
            i = i.strip()
            if len(i) > 0:
                if self.is_gtf:
                    i = i.replace('"', '')
                key, val = i.split(sep)
                attr_dict[key] = val.split(',')

        return attr_dict

    def __str__(self):
        attr_line = []

        if self.is_gtf:
            for key in sorted(self.attr):
                val = self.attr[key]
                val = '"' + ','.join(val) + '"'
                tmp = ' '.join((key, val))
                attr_line.append(tmp)
            attr_line = '; '.join(attr_line) + ';'
        else:
            for key in sorted(self.attr):
                val = self.attr[key]
                val = ','.join(val)
                tmp = '='.join((key, val))
                attr_line.append(tmp)
            attr_line = ';'.join(attr_line)

        return '\t'.join((self.seqid,
                          self.source,
                          self.ftype,
                          str(self.start),
                          str(self.end),
                          self.score,
                          self.strand,
                          self.phase,
                          attr_line))


################################################################################
def parse(f, ext='.gff3'):
    for line in f:
        if len(line) > 1 and line[0] != '#':
            yield Feature(line, ext)


def headers(f):
    for line in f:
        if line[0] == '#':
            yield line.strip()
        else:
            break

def infer_introns(exons):
    introns = []
    exons.sort()
    return [(exons[n][1]+1, exons[n+1][0]-1) for n in range(len(exons)-1)]
