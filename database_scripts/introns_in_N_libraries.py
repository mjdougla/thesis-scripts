#!/home2/mattdoug/bin/python3
# Purpose: Return features that were identified between X and Y libraries.

import sys
import gff3_utils as gff

path = sys.argv[1]
min_lib = sys.argv[2]
max_lib = sys.argv[3]
lrange = tuple(map(int, (sys.argv[2], sys.argv[2])))

print('##gff-version 3')
with open(path, 'r') as f:
    for feat in gff.parse(f):
        c = len(feat.attr['ScorePerLibrary'])
        if lrange[0] <= c <= lrange[1]:
            print(feat)
