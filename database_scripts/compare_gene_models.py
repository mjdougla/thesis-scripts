#!/usr/local/bin/python3
# Last updated: 7/17/2018
# Author: Matt Douglas
# Purpose: Compare two sets of gene models and count the number of reference
#          models that are extended, shortened, split, etc.

import sys
import gff_utils as gff
from collections import defaultdict

qry_path = sys.argv[1]
ref_path = sys.argv[2]
gname_lookup = {}
qry_d = {}
ref_d = defaultdict(list)
gmap = defaultdict(dict)
results = {'no_overlap':0,
           'match':0,
           'extend':0,
           'shorter':0,
           'link':0,
           'split':0
          }

print('Reading reference file...', file=sys.stderr)
with open(ref_path, 'r') as f:
    for feat in gff.parse(f):
        start = int(feat.start)
        end = int(feat.end)
        seqid = feat.seqid
        strand = feat.strand
        if feat.ftype == 'gene':
            name = feat.attr['ID'][0]
            if feat.attr['biotype'] != 'protein_coding':
                if name not in ref_d:
                    ref_d[name] = [start, end, seqid, strand]
        elif 'RNA' in feat.ftype:
            name = feat.attr['ID'][0]
            parent = feat.attr['Parent'][0]
            gname_lookup[name] = parent
        elif feat.ftype == 'CDS':
            name = feat.attr['ID'][0]
            parent_t = feat.attr['Parent'][0]
            parent_g = gname_lookup[parent_t]
            if parent_g not in ref_d:
                ref_d[parent_g] = [None, None, seqid, strand]
            if ref_d[parent_g][0] is None or start < ref_d[parent_g][0]:
                ref_d[parent_g][0] = start
            if ref_d[parent_g][1] is None or end > ref_d[parent_g][1]:
                ref_d[parent_g][1] = end

print('Mapping genes to coordinates...', file=sys.stderr)
for gene, pos in ref_d.items():
    start, end, seqid, strand = pos
    print(gene, start, end)
    for i in range(start, end+1):
        gmap[(seqid, strand)][i] = gene

print('Comparing against query file...', file=sys.stderr)
with open(qry_path, 'r') as f:
    for feat in gff.parse(f):
        if feat.ftype == 'gene':
            region = feat.seqid, feat.strand
            start = int(feat.start)
            end = int(feat.end)
            l_match = (gmap[region][start] if start in gmap[region] else None)
            r_match = (gmap[region][end] if end in gmap[region] else None)
            if l_match == None and r_match == None:
                results['no_overlap'] += 1
            elif l_match != r_match:
                results['link'] += 1
            elif l_match == r_match:
                qry_len = end-start+1
                ref_len = ref_d[l_match][1]-ref_d[l_match][0]+1
                if qry_len == ref_len:
                    results['match'] += 1
                elif qry_len > ref_len:
                    results['extend'] += 1
                elif qry_len < ref_len:
                    results['shorter'] += 1

print('{:,} genes match exactly'.format(results['match']))
print('{:,} genes extended'.format(results['extend']))
print('{:,} genes shortened'.format(results['shorter']))
print('{:,} genes split'.format(results['split']))
print('{:,} genes linked'.format(results['link']))
print('{:,} genes do not overlap a ref gene'.format(results['no_overlap']))
