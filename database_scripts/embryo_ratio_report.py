#!/usr/bin/local/python3
# Purpose: Generate a tab-seperated file of usage ratios for each intron in each
#          embryo library.

import sys
import gff_utils as gff
from collections import defaultdict

conf_path = sys.argv[1]
intron_path = sys.argv[2]
lib_d = {}
time_d = defaultdict(set)
db = defaultdict(dict)
name_d = defaultdict(str)
btype_d = defaultdict(str)
db_g = {}

# read the config file (RUN, LIBRARY, TIME-POINT)
with open(conf_path, 'r') as f:
    for line in f:
        run, library, time = line.strip().split('\t')
        time = int(time)
        lib_d[library] = time
        time_d[time].add(library)

# read the embryo intron database
with open(intron_path, 'r') as f:
    for line in gff.parse(f):
        feat = line.seqid, line.start, line.end, line.strand
        db_g[feat] = line.score
        name_d[feat] = ','.join([i.split('Gene:')[-1] for i in line.attr['Parent']])
        btype_d[feat] = ','.join([i for i in line.attr['biotype']])
        for i in line.attr['ScorePerLibrary']:
            library, score = i.split(':')
            if library in lib_d: # only include libraries in the list
                db[feat][library] = float(score)

header1 = []
header2 = []
for i in sorted(time_d):
    libs = sorted(time_d[i])
    header1 += [i] * len(libs)
    header2 += libs

print('Intron', 'Gene', 'Biotype', 'Global', *header1, sep='\t')
print('Intron', 'Gene', 'Biotype', 'Global', *header2, sep='\t')

for feat in db:
    line = ['{}:{}-{}{}'.format(*feat), name_d[feat], btype_d[feat], db_g[feat]]
    for lib in header2:
        line.append((db[feat][lib] if lib in db[feat] else 'NA'))
    print(*line, sep='\t')
