#!/usr/local/bin/python3
# Purpose: Evaluate a set of exons in relation to reference gene models.

import sys
from collections import defaultdict

exon_path = sys.argv[1]
intron_path = sys.argv[2]
ref_path = '/home2/mattdoug/Thesis/reference/c_elegans.PRJNA13758.WS250.gene_models.gff3'
match_f = open('eval_exons.match.gff3', 'w')
new_c_f = open('eval_exons.combo_sites.gff3', 'w')
new_s_f = open('eval_exons.new_site.gff3', 'w')
new_b_f = open('eval_exons.new_both_sites.gff3', 'w')
overlap_f = open('eval_exons.overlaps.gff3', 'w')
overlap_g_f = open('eval_exons.overlaps.genes.gff3', 'w')
extend_f = open('eval_exons.extends.gff3', 'w')
extend_g_f = open('eval_exons.extends.genes.gff3', 'w')
link_f = open('eval_exons.links.gff3', 'w')
link_g_f = open('eval_exons.links.genes.gff3', 'w')
no_over_f = open('eval_exons.no_overlap.gff3', 'w')
g_dict = defaultdict(dict)
ng_dict = defaultdict(dict)
s_dict = defaultdict(dict)
e_dict = defaultdict(dict)
match = 0
new_c = 0
new_s = 0
new_b = 0
overlap = 0
overlap_g = set()
extend = 0
extend_g = set()
link = 0
link_g = set()
no_over = 0

def parse_attr(attr, label='Name'):
    for i in attr.split(';'):
        if i.startswith(label):
            for j in i.split(','):
                name = j.split('=')[-1].strip()
                return name

# read the reference gene models
with open(ref_path, 'r') as f:
    for line in f:
        if line[0] != '#':
            i = line.split('\t')
            region = i[0], i[6]
            left, right = int(i[3]), int(i[4])
            if i[2] == 'gene':
                name = parse_attr(i[8])
                for j in range(left, right+1):
                    g_dict[region][j] = name
            elif i[2] == 'CDS':
                try:
                    s_dict[region][left].append(right)
                except KeyError:
                    s_dict[region][left] = [right]
                try:
                    s_dict[region][right].append(left)
                except KeyError:
                    s_dict[region][right] = [left]

# read the intron database
with open(intron_path, 'r') as f:
    for line in f:
        if line[0] != '#':
            i = line.split('\t')
            region = i[0], i[6]
            left, right = int(i[3]), int(i[4])

# read the exons, and check which ones modify a reference gene
with open(exon_path, 'r') as f:
    for line in f:
        if line[0] != '#':
            line = line.strip()
            i = line.split('\t')
            region = i[0], i[6]
            left, right = int(i[3]), int(i[4])
            if left in s_dict[region]:
                if right in s_dict[region][left]: # annotated site
                    match += 1
                    print(line, file=match_f)
                elif right in s_dict[region]: # novel combination of known sites
                    new_c += 1
                    print(line, file=new_c_f)
                else:                         # one site new
                    new_s += 1
                    print(line, file=new_s_f)
            elif right in s_dict[region]:     # one site new
                new_s += 1
                print(line, file=new_s_f)
            else:                             # both sites new
                new_b += 1
                print(line, file=new_b_f)
            g_left = (g_dict[region][left] if left in g_dict[region] else None)
            g_right = (g_dict[region][right] if right in g_dict[region] else None)
            if (g_left, g_right).count(None) == 0:
                if g_left == g_right:           # both ends of intron map to same gene
                    overlap += 1
                    overlap_g.add(g_left)
                    print(line, file=overlap_f)
                elif g_left != g_right:         # each end of the intron maps to a different gene
                    link += 1
                    link_g.add(g_left)
                    link_g.add(g_right)
                    print(line, file=link_f)
            elif (g_left, g_right).count(None) == 1: # only one end maps to a gene
                extend += 1
                print(line, file=extend_f)
                if g_left is not None:
                    extend_g.add(g_left)
                elif g_right is not None:
                    extend_g.add(g_right)
            else:                            # neither end maps to a gene
                no_over += 1
                print(line, file=no_over_f)

# re-read the reference genes; print any that were modified
with open(ref_path, 'r') as f:
    for line in f:
        if line[0] != '#':
            match = False
            line = line.strip()
            i = line.split('\t')
            if i[2] != 'gene':
                continue
            name = parse_attr(i[8])
            if name in overlap_g:
                print(line, file=overlap_g_f)
                match = True
            if name in extend_g:
                print(line, file=extend_g_f)
                match = True
            if name in link_g:
                print(line, file=link_g_f)
                match = True

# report a summary
print('Reference\t{}'.format(ref_path))
print('Query:\t{}'.format(exon_path))
print('#'*50)
print('{:,}\tmatch exactly'.format(match))
print('{:,}\tuse a new combo of splice sites'.format(new_c))
print('{:,}\thave one new splice site'.format(new_s))
print('{:,}\thave both new splice sites'.format(new_b))
print('#'*50)
print('{:,}\tcompletely overlap a gene'.format(overlap))
print('{:,}\t...genes'.format(len(overlap_g)))
print('{:,}\textend a gene'.format(extend))
print('{:,}\t...genes'.format(len(extend_g)))
print('{:,}\tlink two or more genes'.format(link))
print('{:,}\t...genes'.format(len(link_g)))
print('{:,}\tdo not overlap a gene'.format(no_over))

# finish up
match_f.close()
new_c_f.close()
new_s_f.close()
new_b_f.close()
overlap_f.close()
overlap_g_f.close()
extend_f.close()
extend_g_f.close()
link_f.close()
link_g_f.close()
no_over_f.close()
