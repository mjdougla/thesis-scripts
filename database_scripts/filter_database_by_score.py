#!/usr/local/bin/python3
# Purpose: Return features with score N or more.

import sys
import gff_utils as gff

if len(sys.argv) < 2:
    print('USAGE: {} <intron_db.gff3> <min_score>'.format(sys.argv[0]))
    sys.exit(1)

path = sys.argv[1]
x = float(sys.argv[2])

print('##gff-version 3')
with open(path, 'r') as f:
    for feat in gff.parse(f):
        for i in feat.attr['ScorePerLibrary']:
            score = float(i.split(':')[1])
            if score >= x:
                print(feat)
                break
