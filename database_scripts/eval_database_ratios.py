#!/usr/local/bin/python3
# Purpose: Return how many features (introns/exons) that are rare or non-rare,
#          globally or locally.

import os, sys
import gff_utils as gff

threshold = 0.01
g_rare_c = 0
g_nonrare_c = 0
l_rare_c = 0
l_nonrare_c = 0

outdir = 'eval_database_ratios'

if not os.path.exists(outdir):
    os.makedirs(outdir)

g_rare = open('{}/eval.global_rare.gff3'.format(outdir), 'w')
g_nonrare = open('{}/eval.global_nonrare.gff3'.format(outdir), 'w')
l_rare = open('{}/eval.local_rare.gff3'.format(outdir), 'w')
l_nonrare = open('{}/eval.local_nonrare.gff3'.format(outdir), 'w')

################################################################################
with open(sys.argv[1], 'r') as f:
    for count, line in enumerate(gff.parse(f), 1):
        l_ratio_max = 0
        # check the global usage ratio
        if float(line.score) < threshold:
            print(line, file=g_rare)
            g_rare_c += 1
        else:
            print(line, file=g_nonrare)
            g_nonrare_c += 1
        # check the local usage ratios
        for i in line.attr['ScorePerLibrary']:
            l_ratio = float(i.split(':')[1])
            l_ratio_max = max((l_ratio_max, l_ratio))
        if l_ratio_max < threshold:
            print(line, file=l_rare)
            l_rare_c += 1
        else:
            print(line, file=l_nonrare)
            l_nonrare_c += 1

print('{:,} total'.format(count))
print('{:,} are globally rare'.format(g_rare_c))
print('{:,} are globally non-rare'.format(g_nonrare_c))
print('{:,} are locally rare'.format(l_rare_c))
print('{:,} are locally non-rare'.format(l_nonrare_c))

g_rare.close()
g_nonrare.close()
l_rare.close()
l_nonrare.close()
