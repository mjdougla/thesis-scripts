#!/usr/bin/local/python3
# Purpose: Return introns that have a high usage ratio in one of "early," "mid,"
#          or "late" embryo development.

import sys
from collections import defaultdict

def check_peak(avg):
    crosses_min = []
    above_max = []
    last = 0

    for t in sorted(avg):
        i = avg[t]
        if last < minr and i >= minr:
            crosses_min.append(t)
        elif last >= minr and i < minr:
            crosses_min.append(t)
        if i >= maxr:
            above_max.append(t)
        last = i

    if len(crosses_min) == 2 and len(above_max) > 0:
        return (above_max[0], above_max[-1]), (crosses_min[0], crosses_min[-1])
    else:
        return False


# User defined variables
bin_d = {(0, 300):[], (330, 570):[], (600, 720):[]}
maxr = 0.55
minr = 0.10

# Script variables
total = 0
discard = 0
all_d = []
gene_d = {}
btype_d = {}

splitline = lambda line: line.strip().split('\t')

with open(sys.argv[1], 'r') as f:
    header1 = splitline(f.readline()) #1st header: Intron, Gene, Biotype, Global, 0min, 30min, etc...
    tpoint = {n:int(i) for n, i in enumerate(header1[4:])}
    next(f) #2st header: Intron, Gene, Biotype, Global, Library1, Library2, etc...
    for i in f:
        time_d = defaultdict(list)
        avg = {}
        # parse the line
        total += 1
        i = splitline(i)
        intron = i[0]
        gene_d[intron] = i[1].split(',')
        btype_d[intron] = i[2].split(',')
        g_score = (0.0 if i == 'NA' else float(i[3]))
        ratios = i[4:]
        # for now, we only care about protein coding genes
        if 'protein_coding' not in btype_d[intron]:
            discard += 1
            continue
        # calculate the average ratio for each time point
        for n, i in enumerate(ratios):
            t = tpoint[n]
            i = (0 if i == 'NA' else float(i))
            time_d[t].append(i)
        for t in sorted(time_d):
            i = time_d[t]
            avg[t] = sum(i)/len(i)
        # check if the pattern of ratios meets our criteria
        result = check_peak(avg)
        if not result:
            continue
        max_range, min_range = result
        all_d.append((intron, max_range, min_range, ratios))
        for i in bin_d:
            if i[0] <= min_range[0] and min_range[1] <= i[1]:
                bin_d[i].append((intron, max_range, min_range, ratios))

print('Intron', 'Gene', 'Biotype', 'L.Max', 'R.Max', 'L.Min', 'R.Min', *[tpoint[n] for n in range(len(ratios))], sep='\t')
for k, v in bin_d.items():
    for i in sorted(v, key=lambda x: (x[1][0], x[1][1])):
        intron, max_range, min_range, ratios = i
        print(intron, ', '.join(gene_d[intron]), ', '.join(btype_d[intron]), max_range[0], max_range[1], min_range[0], min_range[1], *ratios, sep='\t')
# for i in sorted(all_d, key=lambda x: (x[1][0], x[1][1])):
#     intron, max_range, min_range, ratios = i
#     print(intron,
#           ', '.join(gene_d[intron]),
#           ', '.join(btype_d[intron]),
#           max_range[0],
#           max_range[1],
#           min_range[0],
#           min_range[1],
#           *ratios,
#           sep='\t')
