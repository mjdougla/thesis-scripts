#!/usr/local/bin/python3
# Purpose: Return features with score N or more in at least X libraries, but
#          less than Y libraries.

import sys
import gff_utils as gff

def strip_float(f):
    f = str(f)
    if f[-2:] == '.0': # dumb way to check if the score should be a float or not
        f = f[:-2]
    return f

if len(sys.argv) < 3:
    print('USAGE: {} <intron_db.gff3> <min_score> <min_libraries> <max_libraries>'.format(sys.argv[0]))
    sys.exit(1)

path = sys.argv[1]
min_score = float(sys.argv[2])
min_lib = int(sys.argv[3])
max_lib = int(sys.argv[4])

print('##gff-version 3')
with open(path, 'r') as f:
    for feat in gff.parse(f):
        max_score = []
        for i in feat.attr['ScorePerLibrary']:
            lib, score = i.split(':')
            score = float(score)
            if score >= min_score:
                max_score.append(score)
        if min_lib <= len(max_score) <= max_lib:
            print(feat)
