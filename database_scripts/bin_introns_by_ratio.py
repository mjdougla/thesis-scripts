#!/~/bin/python3
# Purpose: Bin features (introns/exons) by the maximum local usage ratio.

import sys
import gff_utils as gff

# User defined variables
path = sys.argv[1]
bins = [0, 0.01, 0.1, 0.5, 0.99]

# Script variables
total = 0
skipped = 0
bins_l = {i:0 for i in bins}
bins_g = bins_l.copy()

with open(path, 'r') as f:
    for feat in gff.parse(f):
        total += 1
        score_l = 0
        score_g = float(feat.score)
        if len(feat.attr['ScorePerLibrary']) <= 1:
            skipped += 1
            continue
        for i in feat.attr['ScorePerLibrary']:
            temp = float(i.split(':')[1])
            score_l = max(score_l, temp)
        for n, i in enumerate(bins):
            if score_l < i:
                bins_l[bins[n-1]] += 1
                break
            if n == len(bins)-1:
                bins_l[bins[n]] += 1
        for n, i in enumerate(bins):
            if score_g < i:
                bins_g[bins[n-1]] += 1
                break
            if n == len(bins)-1:
                bins_g[bins[n]] += 1


print('Bin\tLocal\tGlobal')
for n, i in enumerate(bins):
    if n < len(bins)-1:
        print('{}-{}\t{}\t{}'.format(i, bins[n+1], bins_l[i], bins_g[i]))
    else:
        print('{}+\t{}\t{}'.format(i, bins_l[i], bins_g[i]))
print('Total\t{}'.format(str(total)))
print('Skipped\t{}'.format(str(total)))
