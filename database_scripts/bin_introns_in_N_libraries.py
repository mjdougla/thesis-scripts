#!/~/bin/python3
# Purpose: Bin features (introns/exons) by the number of libraries they were
#          identified.

import sys
import gff_utils as gff

# User defined variables
pathl = sys.argv[1:]
bins = [(0, 1), (2, 5)] + [(i+1, i+5) for i in range(5, 1001, 5)]

# Script variables
total = 0
bins_d = {path:{i:0 for i in bins} for path in pathl}

for path in pathl:
    with open(path, 'r') as f:
        for feat in gff.parse(f):
            total += 1
            lib_c = len(feat.attr['ScorePerLibrary'])
            for i in bins:
                m, n = i
                if m <= lib_c <= n:
                    bins_d[path][i] += 1
                    break
                if i == bins[-1]:
                    bins_d[path][i] += 1

for i in bins[:-1]:
    m, n = i
    results = '\t'.join(map(str, [bins_d[path][i] for path in pathl]))
    print('{}-{}\t{}'.format(m, n, results))
results = '\t'.join(map(str, [bins_d[path][bins[-1]] for path in pathl]))
print('{}+\t{}'.format(bins[-1][0], results))
