#!/usr/bin/local/python3
# Last updated: 12/6/2018
# Author: Matt Douglas
#
# NOTE: NOT USED IN MY THESIS. USE 'intron_ratio_by_gene.py' INSTEAD.
# Purpose: Calulate the relative usage ratios for introns that overlap.

import sys
import gff_utils as gff
from collections import defaultdict
from itertools import chain
flatten = chain.from_iterable

db = defaultdict(dict)
db_global = defaultdict(list)
overlap_local = defaultdict(dict)
overlap_global = {}
result_local = defaultdict(dict)
result_global = {}

print('Reading intron database...', file=sys.stderr)
with open(sys.argv[1], 'r') as f:
    for line in gff.parse(f):
        region = line.seqid, line.strand
        db_global[region].append((line.start, line.end, int(line.score)))
        for i in line.attr['ScorePerLibrary']:
            library, score = i.split(':')
            score = int(score)
            try:
                db[region][library].append((line.start, line.end, score))
            except KeyError:
                db[region][library] = [(line.start, line.end, score)]

print('Checking which introns overlap locally...', file=sys.stderr)
for region in db:
    for library, introns in db[region].items():
        c = 0
        temp = []
        ranges = sorted(flatten(((i[0], 1, i), (i[1], -1, i)) for i in introns))
        for pos, label, i in ranges:
            c += label
            if label == 1:
                temp.append(i)
                for t in temp:
                    x = region[0], t[0], t[1], region[1], t[2]
                    try:
                        overlap_local[library][x] += [i[2]]
                    except KeyError:
                        overlap_local[library][x] = [y[2] for y in temp]
            else:
                temp.remove(i)

print('Calculating local ratios...', file=sys.stderr)
for library in overlap_local:
    for intron in overlap_local[library]:
        x = intron[:4]
        score = intron[4]
        total = sum(overlap_local[library][intron])
        ratio = '{0:.3f}'.format(score/total)
        result_local[x][library] = ratio

print('Checking which introns overlap globally...', file=sys.stderr)
for region, introns in db_global.items():
    c = 0
    temp = []
    ranges = sorted(flatten(((i[0], 1, i), (i[1], -1, i)) for i in introns))
    for pos, label, i in ranges:
        c += label
        if label == 1:
            temp.append(i)
            for t in temp:
                x = region[0], t[0], t[1], region[1], t[2]
                try:
                    overlap_global[x] += [i[2]]
                except KeyError:
                    overlap_global[x] = [y[2] for y in temp]
        else:
            temp.remove(i)

print('Calculating global ratios...', file=sys.stderr)
for intron in overlap_global:
    x = intron[:4]
    score = intron[4]
    total = sum(overlap_global[intron])
    ratio = '{0:.3f}'.format(score/total)
    result_global[x] = ratio

print('Reporting the results...', file=sys.stderr)
with open(sys.argv[1], 'r') as f:
    for line in gff.headers(f):
        print(line)

for intron in result_global:
    seqid, start, end, strand = intron
    score_global = result_global[intron]
    attr = []
    for library, ratio in result_local[intron].items():
        attr.append((library, ratio))
    attr.sort(key=lambda x: x[1], reverse=True)
    attr = ['{}:{}'.format(*i) for i in attr]
    line = seqid, '.', 'intron', start, end, score_global, strand, '.', 'ScorePerLibrary='+','.join(attr)
    print(*line, sep='\t')

print('Done!', file=sys.stderr)
