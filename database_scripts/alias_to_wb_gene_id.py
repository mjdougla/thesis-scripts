#!/usr/local/bin/python3
# Purpose: Take a list of WormBase gene IDs (e.g. WBGene00001234) and check the
#          gene models for the corresponding gene alias (e.g. unc-32); print the
#          alias.

import sys
import gff_utils as gff

source = []
refpath = '/home2/mattdoug/Thesis/reference/c_elegans.PRJNA13758.WS250.gene_models.gff3'

with open(sys.argv[1], 'r') as f:
    for line in f:
        alias = line.strip()
        source.append(alias)

with open(refpath, 'r') as f:
    for line in gff.parse(f):
        if line.ftype == 'gene':
            alias = line.attr['Alias']
            wbgene = line.attr['Name'][0]
            for i in alias:
                if i in source:
                    print(i, wbgene, sep='\t')
