#!/usr/local/bin/python3
# Purpose: Build gene models based on regions of contiguous introns and exons.
#
# Note: Can use 'awk' and 'bedtools' to annotate the assembled gene models:
# awk '$3 == "gene" {print}' $REF | \
#     bedtools intersect -s -wao -a gene_model.gff3 -b stdin | \
#     awk '{
# 	if ( $19 != 0 ) last=$18;
#         else last=$9;
#         printf ("%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n", $1,$2,$3,$4,$5,$6,$7,$8,last);
#         }' - \
# > gene_model.annotated.gff3

import sys
from collections import defaultdict
from itertools import chain

intron_path = sys.argv[1]
exon_path = sys.argv[2]

def parse_gff3(path):
    features = []

    with open(path, 'r') as f:
        for line in f:
            if line[0] != '#' or len(line) < 1:
                i = line.split('\t')
                pos = i[0], int(i[3]), int(i[4]), i[6]
                features.append(pos)

    return features


def define_genes(introns, exons):
    flatten = chain.from_iterable
    temp = defaultdict(list)
    genes = []

    # merge sets of introns and internal exons
    for n, f in enumerate((introns, exons)):
        for i in f:
            seqid, l, r, strand = i
            temp[(seqid, strand)].append((l, r, n))

    # find contiguous regions of exons joined by introns
    for region, ranges in temp.items():
        ranges = sorted(flatten(((l-1, n, 1), (r+1, n, -1)) for l, r, n in ranges))
        c = 0
        x, y = None, None
        for value, kind, label in ranges:
            if c == 0 and kind == 1:
                x = value
            elif c != 0 and kind == 1:
                y = value - 1
            c += label
            if c == 0:
                if all((x, y)):
                    genes.append((region[0], x+1, y, region[1]))
                x, y = None, None

    return genes


def sort_by_pos(features):
    numerals = {'I':1, 'II':2, 'III':3, 'IV':4, 'V':5, 'X':10, 'MtDNA':11}
    try:
        return sorted(features, key=lambda x: (numerals[x[0]], int(x[1]), int(x[2])))
    except KeyError:
        return sorted(features, key=lambda x: (x[0], int(x[1]), int(x[2])))


introns = parse_gff3(intron_path)
exons = parse_gff3(exon_path)
genes = define_genes(introns, exons)
genes = sort_by_pos(genes)

for n, g in enumerate(genes, 1):
    line = g[0], '.', 'gene', g[1], g[2], '.', g[3], '.', 'ID=Gene:DBGene{0:0>8}'.format(n)
    print(*line, sep='\t')
