#!/usr/local/bin/python3
# Purpose: Retrieve individual exon scores from each library. 

import sys
import gff_utils as gff
from collections import defaultdict

def percent(x, y):
    if y == 0:
        return '0%'
    return '{0:.2f}%'.format(x / y * 100)


edb_path = sys.argv[1]
lib_conf = sys.argv[2]
score_d = {}
lib_path = []
has_score = 0

print('Reading exon database...', file=sys.stderr)
with open(edb_path, 'r') as f:
    for feat in gff.parse(f):
        score_d[feat.pos] = []

print('Reading config file...', file=sys.stderr)
with open(lib_conf, 'r') as f:
    for line in f.readlines():
        entry = line.strip().split('\t')
        lib_path.append(entry)

for n, entry in enumerate(lib_path, 1):
    print('\rChecking individual libraries for exons... ({:,}/{:,})'
          .format(n, len(lib_path)), file=sys.stderr, end='')
    lib, path = entry
    with open(path, 'r') as f:
        for feat in gff.parse(f):
            exon = feat.pos
            score = feat.score
            try:
                score_d[exon].append((lib, score))
            except KeyError: # if the exon is not in the database, continue
                pass
print('', file=sys.stderr)

print('Outputting the results...', file=sys.stderr)
with open(edb_path, 'r') as f:
    for feat in gff.parse(f):
        exon = feat.pos
        score_per_lib = [':'.join(i) for i in score_d[exon]]
        feat.attr['ScorePerLibrary'] = score_per_lib
        print(feat)
        if len(score_per_lib) > 0 :
            has_score += 1

print('{:,}/{:,} ({}) exons were found in at least one library.'
      .format(has_score, len(score_d), percent(has_score, len(score_d))),
      file=sys.stderr)
