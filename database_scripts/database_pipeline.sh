set -e
set -x

# WormBase gene models
REFERENCE="/home2/mattdoug/Thesis/reference/c_elegans.PRJNA13758.WS250.gene_models.gff3"

# $DBCONF is a text file of runs to merge and their library information:
# each line has the format: runID <tab> libraryID <tab> path_to_file
DBCONF="build_database.conf"

# variables determined by the script
DBCONF_R="build_database_ratios.conf"
OUTPUT="database.gff3"
OUTPUT_M="database.min5.gff3"
OUTPUT_R="database.min5.global_ratios.gff3"
OUTPUT_RL="database.min5.all_ratios.gff3"

# build the database
build_database.py $DBCONF $OUTPUT

# filter the database to just those that have N support in at least 1 library
filter_database_by_score.py $OUTPUT 5 > $OUTPUT_M

# calculate the GLOBAL intron usage ratios for the database
intron_ratio_by_gene.py $REFERENCE $OUTPUT_M > $OUTPUT_R 2> `basename $OUTPUT_R .gff3`".log"

# calculate the LOCAL usage ratios for each library (individually)
mkdir -p database_libraries_ratios
rm -f $DBCONF_R

for i in database_libraries/*.gff3; do
    LOG="database_libraries_ratios/"`basename $i .gff3`".log"
    OUT="database_libraries_ratios/"`basename $i`
    echo "`basename $i .gff3`	$OUT" >> $DBCONF_R
    intron_usage_ratio.py $REFERENCE $i > $OUT 2> $LOG
done

# build a database with the usage ratios instead of just read support
build_database_ratios.py $DBCONF_R $OUTPUT_R $OUTPUT_RL

# evaluate which features are locally or globally rare/non-rare
# output GFF3 files are placed in the 'eval_database_ratios' directory
eval_database_ratios.py $OUTPUT_RL > "eval_database_ratios.log"
