#!/usr/local/bin/python3
# Last updated: 7/6/2018
# Author: Matt Douglas
# Purpose: Build a nonredundant database of introns or exons from seperate GFF3
#          files.

import os, sys
from collections import defaultdict

if len(sys.argv) < 3:
    print('Input: A tab-seperated text file listing each run, libary, and path to file (RUN[\\t]LIBRARY[\\t]PATH)')
    print('Usage: {} <database.conf> <output.gff3>'.format(sys.argv[0]))
    sys.exit(1)

conf = sys.argv[1]
outf = sys.argv[2]
outdir = 'database_libraries'
d_lib = defaultdict(set)
d_path = {}
d_score = defaultdict(list)
d_lib_done = defaultdict(set)

if not os.path.exists(outdir):
    os.makedirs(outdir)

################################################################################
def parse_gff3(path):
    global ftype

    with open(path, 'r') as f:
        for line in f:
            if line[0] != '#':
                i = line.split('\t')
                ftype = i[2]
                seqid, start, end, strand = i[0], int(i[3]), int(i[4]), i[6]
                score = int(i[5]) if i[5] != '.' else 0
                yield ((seqid, start, end, strand), score)


def sort_features(features):
    """Sort tuples of features by chromosome, then start position, then end
    position. NOTE: C. elegans uses roman numerals for chromosomes names.
    """
    numerals = {'I':1, 'II':2, 'III':3, 'IV':4, 'V':5, 'X':10, 'MtDNA':11}
    try:
        return sorted(features, key=lambda x: (numerals[x[0]], int(x[1]), int(x[2])))
    except KeyError:
        return sorted(features, key=lambda x: (x[0], int(x[1]), int(x[2])))


def format_database(header, d_score, ftype, note_str, path):
    with open(path, 'w') as f:
        for line in header:
            print(line, file=f)
        for intron in sort_features(d_score):
            seqid, left, right, strand = intron
            sources = []
            final_score = 0
            for n, i in enumerate(d_score[intron]):
                run, score = i
                final_score += score
                sources.append(run + ':' + str(score))
            notes = note_str + '=' + ','.join(sources)
            line = seqid, '.', ftype, left, right, final_score, strand, '.', notes
            print(*line, sep='\t', file=f)

################################################################################
# read the config file
with open(conf, 'r') as f:
    for line in f:
        i = line.strip().split('\t')
        d_lib[i[1]].add(i[0])
        d_path[i[0]] = i[2]

# for each library, get the results from each run
for library, runs in d_lib.items():
    temp_score = defaultdict(list)
    for run in runs:
        path = d_path[run]
        try:
            for intron, score in parse_gff3(path):
                temp_score[intron].append((run, score))
            d_lib_done[library].add(run)
        except (FileNotFoundError, IsADirectoryError) as e:
            print("[WARNING] Could not find file '{}' for run '{}' (library '{}'). Skipping.".format(path, run, library))
            continue
    # print results per library
    header = ['##gff-version 3', '##{}={}'.format(library, ','.join(sorted(runs)))]
    format_database(header, temp_score, ftype, 'ScorePerLibrary', '{}/{}.gff3'.format(outdir, library))
    # merge the results of each run
    for intron, scoretup in temp_score.items():
        d_score[intron].append((library, sum(s for run, s in scoretup)))

# sort the scores for each library in descending order
for intron in d_score:
    d_score[intron].sort(key=lambda x: x[1], reverse=True)

# output the combined results from all libraries
print('[INFO] Writing output to "{}"'.format(outf))
header = ['##gff-version 3']
for library in sorted(d_lib_done.keys()):
    runs = sorted(d_lib_done[library])
    header.append('##{}={}'.format(library, ','.join(runs)))

format_database(header, d_score, ftype, 'ScorePerLibrary', outf)
