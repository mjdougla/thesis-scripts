#!/usr/local/bin/python3
# Purpose: Assign a each feature (intron/exon) to WormBase gene model based on
#          overlap - represented as the 'Parent' atttribute in a GFF3 format.

import sys
import gff_utils as gff
from collections import defaultdict

ref_path = '/home2/mattdoug/Thesis/reference/c_elegans.PRJNA13758.WS250.gene_models.gff3'
qry_path = sys.argv[1]
g_dict = defaultdict(dict)

print('Indexing gene positions (Warning: This is memory intensive!)...', file=sys.stderr)
with open(ref_path, 'r') as f:
    for line in gff.parse(f):
        region = line.seqid, line.strand
        left, right = line.start, line.end
        if line.ftype == 'gene':
            name = line.attr['ID'][0]
            for i in range(left, right+1):
                try:
                    g_dict[region][i].add(name)
                except KeyError:
                    g_dict[region][i] = set([name])

print('Assigning features to genes...', file=sys.stderr)
with open(qry_path, 'r') as f:
    for line in gff.headers(f):
        print(line)
with open(qry_path, 'r') as f:
    for line in gff.parse(f):
        region = line.seqid, line.strand
        left, right = line.start, line.end
        line.attr = {'ScorePerLibrary':line.attr['ScorePerLibrary']}
        name_set = set()
        try:
            for name in g_dict[region][left]:
                name_set.add(name)
        except KeyError:
            pass
        try:
            for name in g_dict[region][right]:
                name_set.add(name)
        except KeyError:
            pass
        if len(name_set) > 0:
            line.attr['Parent'] = sorted(name_set)
        print(line)

print('Done!', file=sys.stderr)
