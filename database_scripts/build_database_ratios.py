#!/usr/local/bin/python3
# Last updated: 17/6/2018
# Author: Matt Douglas
# Purpose: Build a nonredundant database of introns or exons from seperate GFF3
#          files.
# Note: Any introns not in the "global" file will be ignored!

import sys
from collections import defaultdict

if len(sys.argv) < 4:
    print('Input: A tab-seperated text file listing each libary and path to file (LIBRARY[\\t]PATH)')
    print('Usage: {} <database.conf> <global_ratios.gff3> <output.gff3>'.format(sys.argv[0]))
    sys.exit(1)

conf = sys.argv[1]
glbl = sys.argv[2]
outf = sys.argv[3]
ftype = None
glbl_score = {}
d_lib = set()
d_lib_done = set()
d_path = {}
d_score = defaultdict(list)

################################################################################
def parse_gff3(path):
    global ftype

    with open(path, 'r') as f:
        for line in f:
            if line[0] != '#':
                i = line.split('\t')
                ftype = i[2]
                seqid, start, end, strand = i[0], int(i[3]), int(i[4]), i[6]
                if i[5] == '.':
                    score = 0
                else:
                    score = float(i[5])
                yield ((seqid, start, end, strand), score)


def sort_features(features):
    """Sort tuples of features by chromosome, then start position, then end
    position. NOTE: C. elegans uses roman numerals for chromosomes names.
    """
    numerals = {'I':1, 'II':2, 'III':3, 'IV':4, 'V':5, 'X':10, 'MtDNA':11}
    try:
        return sorted(features, key=lambda x: (numerals[x[0]], int(x[1]), int(x[2])))
    except KeyError:
        return sorted(features, key=lambda x: (x[0], int(x[1]), int(x[2])))


def format_database(d_score, ftype, note_str, path):
    with open(path, 'w') as f:
        print('##gff-version 3', file=f)
        for intron in sort_features(d_score):
            seqid, left, right, strand = intron
            sources = []
            if intron in glbl_score:
                g_score = glbl_score[intron]
            else:
                continue
            for n, i in enumerate(d_score[intron]):
                library, score = i
                sources.append(library + ':' + str(score))
            notes = note_str + '=' + ','.join(sources)
            line = seqid, '.', ftype, left, right, g_score, strand, '.', notes
            print(*line, sep='\t', file=f)

################################################################################
# read the config file:
with open(conf, 'r') as f:
    for line in f:
        i = line.strip().split('\t')
        d_lib.add(i[0])
        d_path[i[0]] = i[1]

# read the global ratios
for intron, ratio in parse_gff3(glbl):
    glbl_score[intron] = ratio

# for each library, get the results from each library
for library in d_lib:
    temp_score = defaultdict(list)
    path = d_path[library]
    try:
        for intron, score in parse_gff3(path):
            d_score[intron].append((library, score))
        d_lib_done.add(library)
    except (FileNotFoundError, IsADirectoryError) as e:
        print("[WARNING] Could not find file '{}' for library '{}'".format(path, library))
        continue

# sort the scores for each library in descending order
for intron in d_score:
    d_score[intron].sort(key=lambda x: x[1], reverse=True)

# output the combined results from all libraries
print('[INFO] Writing output to "{}"'.format(outf))
format_database(d_score, ftype, 'ScorePerLibrary', outf)
