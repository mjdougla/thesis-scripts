#!/usr/local/bin/python3
# Purpose: Validate reference gene models at the levels of introns, exons, and
#          both together. 

import sys
import gff_utils as gff
from collections import defaultdict

# print usage statement
if len(sys.argv) < 2:
    print('USAGE: {} <introns> <exons>'.format(sys.argv[0]))
    sys.exit(1)

# input files
idb_path = sys.argv[1]
edb_path = sys.argv[2]
ref_path = sys.argv[3] # '/home2/mattdoug/Thesis/reference/c_elegans.PRJNA13758.WS250.protein_coding.gff3'

# filtering thresholds for introns
minl = 30
maxl = 5000

# variables
gene_c = 0
out_of_range = 0
transcript = {}
transcript_pos = {}
ref_intron = set()
ref_exon = set()
db_intron = set()
db_exon = set()
db_exon_l = defaultdict(set)
db_exon_r = defaultdict(set)
single = set()
single_intron = 0
discarded = set()
t_intron_eval = {'complete':set(), 'partial':set(), 'partialx':set(), 'none':set(), 'nonex':set()}
t_exon_eval = {'complete':set(), 'partial':set(), 'none':set()}
t_eval = {'complete':set(), 'partial':set(), 'none':set()}
exon_internal = {'complete':set(), 'partial':set(), 'none':set()}
exon_lterm = {'complete':set(), 'partial':set(), 'none':set()}
exon_rterm = {'complete':set(), 'partial':set(), 'none':set()}
exon_single = {'complete':set(), 'partial':set(), 'none':set()}


def discard_utr_introns(introns, exons):
    # enforce that an intron must be flanked on both sides by CDSs
    is_flanked = {i:[False, False] for i in introns}

    for i in introns:
        l = i[1]-1
        r = i[2]+1
        for e in exons:
            if e[2] == l:
                is_flanked[i][0] = True
            if e[1] == r:
                is_flanked[i][1] = True

    keep = set([i for i in is_flanked if all(is_flanked[i])])
    discard = introns - keep

    return keep, discard


# parse the WormBase annotation
print('Reading reference transcript models...', file=sys.stderr)
with open(ref_path, 'r') as f:
    for feat in gff.parse(f):
        if feat.ftype == 'gene':
            gene_c += 1
        elif feat.ftype == 'mRNA':
            name = feat.attr['ID'][0].split(':')[-1] #e.g. Y74C9A.2a.2
            transcript_pos[name] = feat.pos
            if name not in transcript:
                transcript[name] = [set(), set()]
        elif feat.ftype == 'CDS':
            ref_exon.add(feat.pos)
            for i in feat.attr['Parent']:
                name = i.split(':')[-1]
                try:
                    transcript[name][0].add(feat.pos)
                except KeyError:
                    transcript[name] = [set([feat.pos]), set()]
        elif feat.ftype == 'intron':
            ref_intron.add(feat.pos)
            for i in feat.attr['Parent']:
                name = i.split(':')[-1]
                try:
                    transcript[name][1].add(feat.pos)
                except KeyError:
                    transcript[name] = [set(), set([feat.pos])]

# parse the rna-seq exon database
print('Reading exons...', file=sys.stderr)
with open(edb_path, 'r') as f:
    for feat in gff.parse(f):
        db_exon.add(feat.pos)
        left = feat.seqid, feat.start, feat.strand
        right = feat.seqid, feat.end, feat.strand
        db_exon_l[left].add(right)
        db_exon_r[right].add(left)

# we will subtract from the 'novel' set if they have a match in the reference
novel = db_exon.copy()
match = set()

# parse the rna-seq intron database
print('Reading introns...', file=sys.stderr)
with open(idb_path, 'r') as f:
    for feat in gff.parse(f):
        db_intron.add(feat.pos)

# check how many introns and exons in WormBase transcripts are validated
print('Evaluating transcript models...', file=sys.stderr)
for name in transcript:
    exons, introns = transcript[name]
    exons = sorted(exons, key=lambda x: (x[1], x[2]))
    introns, x = discard_utr_introns(introns, exons) # UTR introns are only ignored for the transcript evaluation!
    discarded = discarded.union(x)
    l_exon = exons[0]
    r_exon = exons[-1]
    la = l_exon[0], l_exon[2], l_exon[3]
    lb = l_exon[0], l_exon[1], l_exon[3]
    ra = r_exon[0], r_exon[1], r_exon[3]
    rb = r_exon[0], r_exon[2], r_exon[3]
    match_i = set()
    match_e = set()
    # evaluate the completeness of the transcript
    if len(exons) > 1:
        for i in introns:
            if i in db_intron:
                match_i.add(i)
        for i in exons[1:-1]: # check internal exons first
            if i in db_exon:
                match_e.add(i)
                match.add(i)
                novel.discard(i)
                exon_internal['complete'].add(i)
        # check for partially matching terminal exons:
        if la in db_exon_r:
            match_e.add(l_exon)
            if lb in db_exon_r[la]:
                exon_lterm['complete'].add(l_exon)
            else:
                exon_lterm['partial'].add(l_exon)
            for lb in db_exon_r[la]:
                x = lb[0], lb[1], la[1], lb[2]
                novel.discard(x)
                match.add(x)
        else:
            exon_lterm['none'].add(l_exon)
        if ra in db_exon_l:
            match_e.add(r_exon)
            if rb in db_exon_l[ra]:
                exon_rterm['complete'].add(r_exon)
            else:
                exon_rterm['partial'].add(r_exon)
            for rb in db_exon_l[ra]:
                x = rb[0], ra[1], rb[1], rb[2]
                novel.discard(x)
                match.add(x)
        else:
            exon_rterm['none'].add(r_exon)
    else:
        if l_exon in db_exon:
            match_e.add(l_exon)
            novel.discard(l_exon)
            match.add(l_exon)
            exon_single['complete'].add(l_exon)
    # add up the results for introns
    if len(match_i) == len(introns):
        t_intron_eval['complete'].add(name)
    elif len(match_i) > 0:
        t_intron_eval['partial'].add(name)
        for i in introns:
            if i[2]-i[1]+1 < minl or i[2]-i[1]+1 > maxl: # intron is the wrong length
                t_intron_eval['partialx'].add(name)
    else:
        t_intron_eval['none'].add(name)
        if len(introns) == 1:
            single_intron += 1
        for i in introns:
            if i[2]-i[1]+1 < minl or i[2]-i[1]+1 > maxl: # intron is the wrong length
                t_intron_eval['nonex'].add(name)
    # add up the results for exons
    if len(match_e) == len(exons):
        t_exon_eval['complete'].add(name)
    elif len(match_i) > 0:
        t_exon_eval['partial'].add(name)
    else:
        t_exon_eval['none'].add(name)
        if len(exons) < 2:
            single.add(name)
    # add up the results for both
    if len(match_i) + len(match_e) == len(introns) + len(exons):
        t_eval['complete'].add(name)
    elif len(match_i) + len(match_e) > 0:
        t_eval['partial'].add(name)
    else:
        t_eval['none'].add(name)

# calculate some results
ref_e_match = set()
for d in (exon_internal, exon_lterm, exon_rterm, exon_single):
    ref_e_match = ref_e_match.union(d['complete'])
    ref_e_match = ref_e_match.union(d['partial'])
avg_exon = sum([len(i[0]) for i in transcript.values()]) / len(transcript)
avg_intron = sum([len(i[1]) for i in transcript.values()]) / len(transcript)

# report the results
for i in ('complete', 'partial', 'none'):
    with open('eval_wormbase.{}.txt'.format(i), 'w') as f:
        for name in t_eval[i]:
            print(name, '{}:{}-{}'.format(*transcript_pos[name][:3]), sep='\t', file=f)

    with open('eval_wormbase.{}_introns.txt'.format(i), 'w') as f:
        for name in t_intron_eval[i]:
            print(name, '{}:{}-{}'.format(*transcript_pos[name][:3]), sep='\t', file=f)

    with open('eval_wormbase.{}_exons.txt'.format(i), 'w') as f:
        for name in t_exon_eval[i]:
            print(name, '{}:{}-{}'.format(*transcript_pos[name][:3]), sep='\t', file=f)

with open('eval_wormbase.novel_exons.gff3', 'w') as f:
    for i in sorted(novel, key=lambda x: (x[0], x[1], x[2])):
        print(i[0], '.', 'CDS', i[1], i[2], '.', i[3], '.', '.', '.', sep='\t', file=f)

with open('eval_wormbase.match_exons.gff3', 'w') as f:
    for i in sorted(match, key=lambda x: (x[0], x[1], x[2])):
        print(i[0], '.', 'CDS', i[1], i[2], '.', i[3], '.', '.', '.', sep='\t', file=f)

print('Reference\t', ref_path)
print('Intron database\t', idb_path)
print('Exon database\t', edb_path)
print('###############################')
print('## Intron and exon databases ##')
print('###############################')
print('{:,}\tDatabase introns'.format(len(db_intron)))
print("{:,}\t...are novel".format(len(db_intron-ref_intron)))
print("{:,}\t...support reference intron".format(len(db_intron.intersection(ref_intron))))
print('{:,}\tDatabase exons'.format(len(db_exon)))
print("{:,}\t...are novel".format(len(novel)))
print("{:,}\t...support reference exons".format(len(match)))
print("{:,}\tMissed refernce introns".format(len(ref_intron-db_intron)))
print("{:,}\tMissed refernce exons".format(len(ref_exon)-len(ref_e_match)))
print('###########################')
print('## Reference transcripts ##')
print('###########################')
print('{:,}\tReference coding genes'.format(gene_c))
print('{:,}\t...transcripts'.format(len(transcript)))
print('{:,}\t...exons'.format(len(ref_exon)))
print('{:,}\t...introns'.format(len(ref_intron)))
print('{:,}\t...ignored UTR introns'.format(len(discarded)))
print('#####################################')
print('## Reference transcript evaluation ##')
print('#####################################')
print('{:,}\tAll introns are supported'.format(len(t_intron_eval['complete'])))
print('{:,}\tSome introns are supported'.format(len(t_intron_eval['partial'])))
print('{:,}\t...have an intron <{:,}bp or >{:,}bp'.format(len(t_intron_eval['partialx']), minl, maxl))
print('{:,}\tNo introns are supported'.format(len(t_intron_eval['none'])))
print('{:,}\t...have an intron <{:,}bp or >{:,}bp'.format(len(t_intron_eval['nonex']), minl, maxl))
print('{:,}\t...are single intron transcripts'.format(single_intron))
print('##')
print('{:,}\tAll exons are supported'.format(len(t_exon_eval['complete'])))
print('{:,}\tSome exons are supported'.format(len(t_exon_eval['partial'])))
print('{:,}\tNo exons are supported'.format(len(t_exon_eval['none'])))
print('{:,}\t...are single exon transcripts'.format(len(single)))
print('##')
print("{:,}\tInternal exons match exactly".format(len(exon_internal['complete'])))
print("{:,}\t5' terminal exons match exactly".format(len(exon_lterm['complete'])))
print("{:,}\t5' terminal exons match partially".format(len(exon_lterm['partial'])))
print("{:,}\t5' terminal exons have no match".format(len(exon_lterm['none'])))
print("{:,}\t5' terminal exons total".format(sum([len(i) for i in exon_lterm.values()])))
print("{:,}\t3' terminal exons match exactly".format(len(exon_rterm['complete'])))
print("{:,}\t3' terminal exons match partially".format(len(exon_rterm['partial'])))
print("{:,}\t3' terminal exons have no match".format(len(exon_rterm['none'])))
print("{:,}\t3' terminal exons total".format(sum([len(i) for i in exon_rterm.values()])))
print("{:,}\tSingle-exons match exactly".format(len(exon_single['complete'])))
print('##')
print('{:,}\tComplete support'.format(len(t_eval['complete'])))
print('{:,}\tPartial support'.format(len(t_eval['partial'])))
print('{:,}\tNo support'.format(len(t_eval['none'])))
print('##')
print("{0:.2f}\tAverage exons per transcript".format(avg_exon))
print("{0:.2f}\tAverage introns per transcript".format(avg_intron))
