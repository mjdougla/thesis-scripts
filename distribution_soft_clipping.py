#!/usr/local/bin/python3
#Purpose: Get the frequency of each alignment score in one or more SAM files so
#you can make a histogram.

import re, sys
from collections import defaultdict

def bases_soft_clipped(cigar):
    sc_bases = [0, 0, 0]

    nums = list(map(int, re.findall(r"[0-9]+", cigar)))
    lets = re.findall(r"[A-Z]+", cigar)

    if lets[0] == 'S':
        sc_bases[0] = nums[0]
    if lets[-1] == 'S':
        sc_bases[1] = nums[-1]
    sc_bases[2] = sum(sc_bases[:2])

    return sc_bases


def parse_SAM(f, f_dict, r_dict, t_dict):
    for line in f:
        if line[0] != '@':
            cigar = line.split('\t')[5]
            f, r, t = bases_soft_clipped(cigar)
            f_dict[f] += 1
            r_dict[r] += 1
            t_dict[t] += 1

    return f_dict, r_dict, t_dict


def main(SAM_files):
    f_dict, r_dict, t_dict = defaultdict(int), defaultdict(int), defaultdict(int)

    # get the frequency of alignment scores in each SAM file
    for i in SAM_files:
        with open(i, 'r') as f:
            f_dict, r_dict, t_dict = parse_SAM(f, f_dict, r_dict, t_dict)

    # max value to count to
    max_val = max(list(f_dict) + list(f_dict) + list(t_dict))
    # the total number of scores recorded
    f_total, r_total, t_total = sum(f_dict.values()), sum(r_dict.values()), sum(t_dict.values())

    # output
    print('Files:', ', '.join(SAM_files))
    print("Bases SC\t5' end\t3' end\tTotal")
    for i in range(max_val):
        print('\t'.join(map(str, (i, f_dict[i], r_dict[i], t_dict[i]))))
    print('Total\t{}\t{}\t{}'.format(f_total, r_total, t_total))


if __name__ == '__main__':
    main(sys.argv[1:])
