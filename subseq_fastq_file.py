#!/usr/local/bin/python
#last updated: 12/3/2018

from __future__ import print_function
import sys, re
from collections import defaultdict
from Bio import SeqIO

def parse_coords(coord):
    """Parse a set of coordinates in the form "X:1000..2000" into a tuple of
    (chromosome, start position, end position).
    """
    strand = '+'
    if coord[-1] in ('+', '-'):
        strand = coord[-1]
        coord = coord[:-1]
    coord = coord.strip().replace(',', '').replace('..', '-')
    chrom, start, end = re.split(':|-|_', coord)
    return chrom, int(start), int(end), strand


if __name__ == '__main__':
    file_path = sys.argv[1]
    if file_path[-3:].lower() in ('.fastq', '.fq'):
        file_type = 'fastq'
    else:
        file_type = 'fasta'

    coord_list = defaultdict(set)
    for i in sys.argv[2:]:
         chrom, start, end, strand = parse_coords(i)
         coord_list[chrom].add((start, end, strand))
    #with open(sys.argv[2], 'r') as f:
    #    for line in f:
    #        chrom, start, end, strand = parse_coords(line.strip())
    #        coord_list[chrom].add((start, end, strand))

    for record in SeqIO.parse(file_path, file_type):
        chrom, seq = record.id, record.seq
        for coord in coord_list[chrom]:
            start, end, strand = coord
            subseq = seq[start:end]
            if strand == '-':
                subseq = subseq.reverse_complement()
            record.seq = subseq
            SeqIO.write(record, sys.stdout, file_type)
