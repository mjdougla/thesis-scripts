#!/usr/local/bin/python3
#Author: Matt Douglas
#last updated: 3/1/2018

import sys
#from Bio import SeqIO

def parse_range_string(string):
    if string.count(':') > 1:
        print('Invalid range!')
        sys.exit()

    if string[0] == ':':
        string = '0' + string

    string = string.split(':')
    if len(string) > 0:
        beg, end = map(int, string)
    else:
        beg = int(string)
        end = None

    return beg, end


if __name__ == '__main__':
    input_f  = sys.argv[1]
    output_f = sys.argv[2]
    beg, end = parse_range_string(sys.argv[3])  # "20" or "1:20" or "10:20" or "20:" or ":20"

    # with open(output_f, 'w') as out:
    #     for record in SeqIO.parse(input_f, 'fastq'):
    #         record.seq = record.seq[beg:end]
    #         record.qual = record.qual[beg:end]
    #         SeqIO.write(record, out, 'fastq')

    with open(input_f, 'r') as inp, open(output_f, 'w') as out:
        header1 = ''
        seq     = ''
        header2 = ''
        qual    = ''
        count = 0
        for line in inp:
            line = line.strip()
            count += 1
            if count == 1:
                header1 = line
            elif count == 2:
                seq = line[beg:end]
            elif count == 3:
                header2 = line
            elif count == 4:
                qual = line[beg:end]
                print(header1, file=out)
                print(seq, file=out)
                print(header2, file=out)
                print(qual, file=out)
                count = 0
