#!/usr/local/bin/python
# Last updated: 27/11/2017
# Author: Matt Douglas

from __future__ import print_function
import pysam, sys
from collections import defaultdict

#####################
# Utility functions #
#####################
def get_strand(line):
    """Get the orientation of the alignment.
    Indicated by 'XS:i:x' where 'x' is '+' or '-'.
    """
    strand = '.'

    for i in line.get_tags():
        if i[0] == 'XS':
            strand = i[1]
            break

    return strand


def sort_introns(introns):
    """Sort tuples of introns by chromosome, then start position, then end
    position.
    """
    sorted_introns = sorted(introns, key=lambda x: (int(x[0]), int(x[1]), int(x[2])))
    return sorted_introns


######################
# Do the actual work #
######################
def parse_CIGAR(chrom, pos, cigar, strand):
    """Get the pos, end, and size of any introns from the CIGAR string.
    if(  cigar_type == 0): #match
    elif(cigar_type == 1): #insertions
    elif(cigar_type == 2): #deletion
    elif(cigar_type == 3): #skip
    elif(cigar_type == 4): #soft clipping
    elif(cigar_type == 5): #hard clipping
    elif(cigar_type == 6): #padding
    """
    introns = list()

    cigar_type = [i[0] for i in cigar]
    cigar_len = [i[1] for i in cigar]

    for i in [i for i, l in enumerate(cigar_type) if l == 3]:
        size = cigar_len[i]
        start = pos
        for j in range(len(cigar_type[:i])):
            if cigar_type[j] in [0, 2, 3]:
                start += cigar_len[j]
        end = start + size - 1

        introns.append((chrom, start, end, strand))

    return introns


def parse_SAM(bamfile):
    reads1 = defaultdict(set)
    reads2 = defaultdict(set)
    nopair = defaultdict(set)
    ref_indeces = {}

    print('Checking mapped reads...', end='')
    for read in bamfile.fetch():
        # parse the BAM entry
        name   = read.query_name.rsplit('/', 1)[0]
        chrom  = read.reference_id
        pos    = read.pos + 1  # SAM coordinates are 1-based
        cigar  = read.cigartuples
        strand = get_strand(read)
        # keep track of the proper names for each chromsome
        ref_name = bamfile.get_reference_name(chrom)
        ref_indeces[ref_name] = chrom
        # find any/all introns
        for intron in parse_CIGAR(chrom, pos, cigar, strand):
            if read.is_proper_pair:
                if read.is_read1:
                    reads1[name].add(intron)
                elif read.is_read2:
                    reads2[name].add(intron)
            else:
                nopair[name].add(intron)

    print('\rChecking mapped reads... Done! ')
    return reads1, reads2, nopair, ref_indeces


def parse_gff(ref_gff, ref_indeces):
    """Parse a GFF3 file and return all intron coordinates."""
    ref_introns = defaultdict(list)

    print('Parsing GFF file...', end='')
    with open(ref_gff, 'r') as f:
        for line in f:
            if line[0] != '#':
                i = line.split('\t')
                chrom, start, end, strand = i[0], int(i[3]), int(i[4]), i[6]
                try:
                    chrom = ref_indeces[chrom]
                except KeyError:
                    continue
                ref_introns[ (chrom, strand) ].append( (start, end) )

    for region, pos_list in ref_introns.items():
        ref_introns[region] = frozenset(pos_list)

    print('\rParsing GFF file... Done!')
    return ref_introns


def only_listed_introns(linked_introns, ref_introns):
    results = []
    remove  = set()

    print('Checking for included introns...', end='')
    for intron_tuple in linked_introns:
        temp = []
        # check if intron is list of introns
        for i in intron_tuple:
            chrom, start, end, strand = i
            if (start, end) in ref_introns[ (chrom, strand) ]:
                temp.append(i)
            else:
                remove.add(i)
        if len(temp) > 0:
            results.append(temp)

    print('\rChecking for included introns... Done!')
    # for i in sort_introns(remove):
    #     print('  Removed intron {}:{:,}-{:,} ({} strand)'.format(*i))

    return results


def print_as_BAM(linked_introns, bamfile, out_path):
    print('Formatting output as a BAM file...', end='')

    with pysam.AlignmentFile(out_path, 'wb', template=bamfile) as f:
        for n, introns in enumerate(linked_introns):
            # sort the introns by position
            introns = sort_introns(introns)
            # calulate the postion, and distance to the next intron
            if len(introns) > 1:
                tlen = introns[-1][2] - introns[0][1] + 1
            else:
                tlen = 0
            # print out each intron as a seperate SAM entry
            for m, i in enumerate(introns):
                chrom, start, end, strand = i
                length = end - start + 1
                if m < len(introns)-1:
                    next_ref = introns[m+1][1]
                else:
                    next_ref = introns[0][1]
                    tlen     = -tlen
                # format the BAM entry
                a = pysam.AlignedSegment()
                a.query_name           = 'linked_introns'+str(n)
                a.query_sequence       = 'N'*length
                a.flag                 = 0
                a.reference_id         = chrom
                a.reference_start      = start
                a.mapping_quality      = 60
                a.cigartuples          = [(0,length)]
                a.next_reference_id    = chrom
                a.next_reference_start = next_ref
                a.template_length      = tlen
                a.query_qualities      = pysam.qualitystring_to_array('/'*length)
                a.tags                 = [('XN', next_ref+1), ('XI', len(introns))]
                f.write(a)

    print('\rFormatting output as a BAM file... Done!')


#############
# Main loop #
#############
if __name__ == '__main__':
    linked_introns = []
    bamfile = pysam.AlignmentFile(sys.argv[1], 'rb')
    ref_gff = sys.argv[2]

    # determine which introns are joined by reads
    reads1, reads2, nopair, ref_indeces = parse_SAM(bamfile)
    for read in reads1.keys() + reads2.keys():
        introns = tuple(reads1[read] | reads2[read])
        linked_introns.append(introns)
    for introns in nopair.values():
        introns = tuple(introns)
        linked_introns.append(introns)

    # remove redundant intron linkages
    linked_introns = set(linked_introns)

    # convert chromosome names into the integers used by pysam
    ref_introns = parse_gff(ref_gff, ref_indeces)

    # only report introns that are in the list of introns
    linked_introns = only_listed_introns(linked_introns, ref_introns)
    
    # output the results
    # print_as_gff3(linked_introns)
    print_as_BAM(linked_introns, bamfile, 'out.bam')
