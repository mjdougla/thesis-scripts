#!/usr/local/bin/python

# The script can take multiple FASTQ files specified on the command line and output
# the longest sequence out of all of them.

import sys
from itertools import islice

longest = 0
longest_seq = ['', '', '', '']

for fastq_file in sys.argv[1:]:
    f = open(fastq_file)
    
    while True:
        next_fastq = list(islice(f, 4))
        if not next_fastq:
            break
        if len(next_fastq[1]) >= longest:
            longest = len(next_fastq[1])
            longest_seq = [i.strip() for i in next_fastq]

    f.close()

sys.stdout.write('\n'.join(longest_seq))

