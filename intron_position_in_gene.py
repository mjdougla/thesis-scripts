#!/usr/local/bin/python3

import sys
from collections import defaultdict

class Intron(object):
    def __init__(self, line):
        self.line = line
        self.seqid = line[0]
        self.start = int(line[3])
        self.end = int(line[4])
        self.strand = line[6]
        self.pos = self.seqid, self.start, self.end, self.strand
        self.parent = self.get_parent(line[8])

    def __str__(self):
        return '\t'.join(self.line)

    def get_parent(self, attr):
        parent = []

        attr = attr.strip().split(';')
        for i in attr:
            k, v = i.split('=')
            if k == 'Parent':
                for j in v.split(','):
                    p = j.split(':')[-1].strip()
                    parent.append(p)

        return parent
                   

def parse_intron(path):
    transcript = defaultdict(list)
    e_count = 0

    with open(path, 'r') as f:
        for line in f:
            if line[0] != '#':
                line = line.strip().split('\t')
                if line[2] == 'intron':
                    intron = Intron(line)
                    parents = intron.parent
                    if len(parents) < 1:
                        e_count += 1
                        continue
                    for i in parents:
                        transcript[i].append(intron)
    if e_count > 0:
        print('[WARNING] Could not determine the parent transcript of {:,} introns!'.format(e_count))
    
    return transcript


def sort_by_pos(introns):
    numerals = {'I':1, 'II':2, 'III':3, 'IV':4, 'V':5, 'X':10, 'MtDNA':11}
    try:
        return sorted(introns, key=lambda x: (numerals[x[0]], x[1], x[2], x[3]))
    except KeyError:
        return sorted(introns, key=lambda x: (x[0], x[1], x[2], x[3]))


def sort_features(introns):
    numerals = {'I':1, 'II':2, 'III':3, 'IV':4, 'V':5, 'X':10, 'MtDNA':11}
    try:
        return sorted(introns, key=lambda x: (numerals[x.seqid], x.start, x.end, x.strand))
    except KeyError:
        return sorted(introns, key=lambda x: (x.seqid, x.start, x.end, x.strand))


if __name__ == '__main__':
    path = sys.argv[1]
    l_set = {}
    r_set = {}
    i_set = {}
    s_set = {}
    l_out = open('intron_pos.l_term.gff3', 'w')
    i_out = open('intron_pos.intrnl.gff3', 'w')
    r_out = open('intron_pos.r_term.gff3', 'w')
    s_out = open('intron_pos.single.gff3', 'w')

    transcripts = parse_intron(path)

    for introns in transcripts.values():
        introns = sort_features(set(introns))
        strand = introns[0].strand
        if len(introns) == 1:
            s = introns[0]
            s_set[s.pos] = s
            continue
        if strand == '+':
            l = introns[0]
            r = introns[-1]
        else:
            l = introns[-1]
            r = introns[0]
        l_set[l.pos] = l
        r_set[r.pos] = r
        for i in introns[1:-1]:
            i_set[i.pos] = i

    for i in sort_by_pos(l_set):
        print(l_set[i], file=l_out)
    for i in sort_by_pos(i_set):
        print(i_set[i], file=i_out)
    for i in sort_by_pos(r_set):
        print(r_set[i], file=r_out)
    for i in sort_by_pos(s_set):
        print(s_set[i], file=s_out)

    l_out.close()
    i_out.close()
    r_out.close()
    s_out.close()
